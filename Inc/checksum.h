#ifndef CHECKSUM_H_
#define CHECKSUM_H_

#include "inttypes.h"

uint16_t computeChecksum(uint8_t* data, uint16_t length);
uint16_t combineChecksum(uint16_t checksum1, uint16_t checksum2);

#endif /* CHECKSUM_H_ */
