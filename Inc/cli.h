#ifndef CLI_H_
#define CLI_H_

typedef void (*CLI_PrintFunc)(const char* str);
typedef void (*CLI_LineHandlerFunc)(int argc, char* const* argv);

#define SUBMENU_NAME_MAXLEN 20

typedef struct _CLI_Submenu {
	char menuName[SUBMENU_NAME_MAXLEN+1];
	CLI_LineHandlerFunc lineHandlerFunc;
} CLI_Submenu;

void CLI_Init(CLI_PrintFunc printFunc);
void CLI_Loop();
void CLI_Print(const char* str);
void CLI_PrintLn(const char* str);
void CLI_PrintResp(const char* str);
void CLI_PrintRespLn(const char* str);
void CLI_HandleChar(char chr);
void CLI_HandleLine(const char* line);
void CLI_ExitSubmenu();

#endif /* CLI_H_ */
