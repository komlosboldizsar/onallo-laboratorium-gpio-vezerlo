#ifndef CLI_SUBMENU_H_
#define CLI_SUBMENU_H_

#include "cli.h"
#include "string.h"
#include "stdio.h"

#define HANDLE_OPTION(CMD, FUNC)    if (strcmp(argv[0], CMD) == 0) { \
										FUNC(argc-1, argv+1); \
										return; \
									}

#define CLI_PRINT_RESP_FORMAT(FORMAT, ARGS...)			{ \
															char buffer[200]; \
															sprintf(buffer, FORMAT, ARGS); \
															CLI_PrintResp(buffer); \
														}

#define CLI_PRINT_RESP_LN_FORMAT(FORMAT, ARGS...)		{ \
															char buffer[200]; \
															sprintf(buffer, FORMAT, ARGS); \
															CLI_PrintRespLn(buffer); \
														}

#define CLI_INVALID_ARGC(REQ_ARGS)                      if (REQ_ARGS > 1) { \
															CLI_PRINT_RESP_LN_FORMAT("Invalid argument count! %d arguments are required!", REQ_ARGS); \
														} else { \
															CLI_PrintRespLn("Invalid argument count! 1 argument is required!"); \
														}

#endif /* CLI_SUBMENU_H_ */
