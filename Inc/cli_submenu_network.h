#ifndef CLI_SUBMENU_NETWORK_H_
#define CLI_SUBMENU_NETWORK_H_

#include "cli.h"

extern CLI_Submenu CLI_SUBMENU_Network;

void CLI_SUBMENU_Network_LineHandlerFunc(int argc, char* const* argv);

#endif /* CLI_SUBMENU_NETWORK_H_ */
