#ifndef CONFIG_EEPROM_H_
#define CONFIG_EEPROM_H_

#include "stm32f4xx_hal.h"
#include "mem_i2c.h"

#define CES_MAC        0x0000
#define CES_IP         0x0008
#define CES_GW         0x000C
#define CES_MASKLEN    0x0010

extern MEM_I2C configEepromHandle;
#define H_CONFIGEEPROM    configEepromHandle
#define P_CONFIGEEPROM    &configEepromHandle

void ConfigEeprom_Init();

void ConfigEeprom_Read8(uint16_t address, uint16_t count, uint8_t* data);
void ConfigEeprom_Read16(uint16_t address, uint16_t count, uint16_t* data);
void ConfigEeprom_Read32(uint16_t address, uint16_t count, uint32_t* data);

void ConfigEeprom_Write8(uint16_t address, uint16_t count, uint8_t* data);
void ConfigEeprom_Write16(uint16_t address, uint16_t count, uint16_t* data);
void ConfigEeprom_Write32(uint16_t address, uint16_t count, uint32_t* data);

#endif /* CONFIG_EEPROM_H_ */
