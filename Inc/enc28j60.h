#ifndef _INC_ENC28J60_
#define _INC_ENC28J60_

// Opcodes

#define ENC28J60_OPCODE_RCR      0x00
#define ENC28J60_OPCODE_RBM      0x20
#define ENC28J60_OPCODE_WCR      0x40
#define ENC28J60_OPCODE_WBM      0x60
#define ENC28J60_OPCODE_BFS      0x80
#define ENC28J60_OPCODE_BFC      0xA0
#define ENC28J60_OPCODE_SC       0xE0

#define ENC28J60_CONST5_WBM      0x1A
#define ENC28J60_CONST5_RBM      0x1A

#define ENC28J60_MEMORY_SIZE     0x1FFF

// Common registers

#include "enc28j60_commonreg.h"
#include "enc28j60_bank0.h"
#include "enc28j60_bank1.h"
#include "enc28j60_bank2.h"
#include "enc28j60_bank3.h"
#include "enc28j60_phyreg.h"

#include "stm32f4xx_hal.h"

typedef void (*ENC28J60_SpiTxFunc)(uint8_t* pDataTx, uint32_t count, uint8_t continued);
typedef void (*ENC28J60_SpiTxRxFunc)(uint8_t* pDataTx, uint8_t* pDataRx, uint32_t count, uint8_t continued);
typedef void (*ENC28J60_LogFunc)(const char* text);
typedef void (*ENC28J60_ReceiveCallback)(uint8_t* payload, uint16_t payloadLength);

typedef struct _ENC28J60 {
	ENC28J60_SpiTxFunc spiTxFunc;
	ENC28J60_SpiTxRxFunc spiTxRxFunc;
	ENC28J60_LogFunc logFunc;
	ENC28J60_ReceiveCallback receiveCallback;
	uint8_t macAddress[6];
	uint8_t receiveIT;
	uint16_t rxNextPacket;
	uint8_t currentBank;
} ENC28J60;

void ENC28J60_InitStruct(ENC28J60* instance, ENC28J60_SpiTxFunc spiTxFunc, ENC28J60_SpiTxRxFunc spiTxRxFunc, ENC28J60_LogFunc logFunc, ENC28J60_ReceiveCallback receiveCallback);

void ENC28J60_Init(ENC28J60* instance, uint8_t* mac, uint16_t transmitBufferSize);
void ENC28J60_InitMac(ENC28J60* instance);
void ENC28J60_InitPhy(ENC28J60* instance);

void ENC28J60_SwitchBank(ENC28J60* instance, uint8_t bank);
void ENC28J60_WriteControlRegister_Direct(ENC28J60* instance, uint8_t addr, uint8_t data);
void ENC28J60_WriteControlRegister(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t data);
uint8_t ENC28J60_ReadControlRegister_Direct(ENC28J60* instance, uint8_t addr);
uint8_t ENC28J60_ReadControlRegister(ENC28J60* instance, uint8_t bank, uint8_t addr);
void ENC28J60_BitFieldSet_Direct(ENC28J60* instance, uint8_t addr, uint8_t data);
void ENC28J60_BitFieldClear_Direct(ENC28J60* instance, uint8_t addr, uint8_t data);
void ENC28J60_BitFieldSet(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t data);
void ENC28J60_BitFieldClear(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t data);

void ENC28J60_WritePhyRegister(ENC28J60* instance, uint8_t addr, uint16_t data);
uint16_t ENC28J60_ReadPhyRegister(ENC28J60* instance, uint8_t addr);

void ENC28J60_Transmit(ENC28J60* instance, uint8_t* dstMac, uint16_t type, uint8_t* data, uint16_t dataLength);
void ENC28J60_TransmitRaw(ENC28J60* instance, uint8_t* data, uint16_t payloadLength);
void ENC28J60_WriteBufferMemory(ENC28J60* instance, uint8_t* data, uint16_t count);
void ENC28J60_ReadBufferMemory(ENC28J60* instance, uint16_t address, uint16_t count, uint8_t* data);
void ENC28J60_SetMacAddress(ENC28J60* instance, uint8_t* mac);

void ENC28J60_EnableReceive(ENC28J60* instance);
void ENC28J60_ReceiveIT(ENC28J60* instance);
void ENC28J60_Loop(ENC28J60* instance);

#endif
