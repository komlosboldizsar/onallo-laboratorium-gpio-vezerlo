#ifndef _INC_ENC28J60_BANK1_
#define _INC_ENC28J60_BANK1_

// Bank 1

#define ENC28J60_BANK_1           0x01

#define ENC28J60_BANK_ETH0       0x01
#define ENC28J60_REG_ETH0        0x00

#define ENC28J60_BANK_ETH1       0x01
#define ENC28J60_REG_ETH1        0x01

#define ENC28J60_BANK_ETH2       0x01
#define ENC28J60_REG_ETH2        0x02

#define ENC28J60_BANK_ETH3       0x01
#define ENC28J60_REG_ETH3        0x03

#define ENC28J60_BANK_ETH4       0x01
#define ENC28J60_REG_ETH4        0x04

#define ENC28J60_BANK_ETH5       0x01
#define ENC28J60_REG_ETH5        0x05

#define ENC28J60_BANK_ETH6       0x01
#define ENC28J60_REG_ETH6        0x06

#define ENC28J60_BANK_ETH7       0x01
#define ENC28J60_REG_ETH7        0x07

#define ENC28J60_BANK_EPMM0      0x01
#define ENC28J60_REG_EPMM0       0x08

#define ENC28J60_BANK_EPMM1      0x01
#define ENC28J60_REG_EPMM1       0x09

#define ENC28J60_BANK_EPMM2      0x01
#define ENC28J60_REG_EPMM2       0x0A

#define ENC28J60_BANK_EPMM3      0x01
#define ENC28J60_REG_EPMM3       0x0B

#define ENC28J60_BANK_EPMM4      0x01
#define ENC28J60_REG_EPMM4       0x0C

#define ENC28J60_BANK_EPMM5      0x01
#define ENC28J60_REG_EPMM5       0x0D

#define ENC28J60_BANK_EPMM6      0x01
#define ENC28J60_REG_EPMM6       0x0E

#define ENC28J60_BANK_EPMM7      0x01
#define ENC28J60_REG_EPMM7       0x0F

#define ENC28J60_BANK_EPMCSL     0x01
#define ENC28J60_REG_EPMCSL      0x10

#define ENC28J60_BANK_EPMCSH     0x01
#define ENC28J60_REG_EPMCSH      0x11

// register @ 0x12: -

// register @ 0x13: -

#define ENC28J60_BANK_EPMOL      0x01
#define ENC28J60_REG_EPMOL       0x14

#define ENC28J60_BANK_EPMOH      0x01
#define ENC28J60_REG_EPMOH       0x15

#define ENC28J60_BANK_EWOLIE                0x01
#define ENC28J60_REG_EWOLIE                 0x16
#define ENC28J60_BIT_EWOLIE_UCWOLIE         0x80
#define ENC28J60_BIT_EWOLIE_AWOLIE          0x40
#define ENC28J60_BIT_EWOLIE_PMWOLIE         0x10
#define ENC28J60_BIT_EWOLIE_MPWOLIE         0x08
#define ENC28J60_BIT_EWOLIE_HTWOLIE         0x04
#define ENC28J60_BIT_EWOLIE_MCWOLIE         0x02
#define ENC28J60_BIT_EWOLIE_BCWOLIE         0x01

#define ENC28J60_BANK_EWOLIR                0x01
#define ENC28J60_REG_EWOLIR                 0x17
#define ENC28J60_BIT_EWOLIR_UCWOLIF         0x80
#define ENC28J60_BIT_EWOLIR_AWOLIF          0x40
#define ENC28J60_BIT_EWOLIR_PMWOLIF         0x10
#define ENC28J60_BIT_EWOLIR_MPWOLIF         0x08
#define ENC28J60_BIT_EWOLIR_HTWOLIF         0x04
#define ENC28J60_BIT_EWOLIR_MCWOLIF         0x02
#define ENC28J60_BIT_EWOLIR_BCWOLIF         0x01

#define ENC28J60_BANK_ERXFCON               0x01
#define ENC28J60_REG_ERXFCON                0x18
#define ENC28J60_BIT_ERXFCON_UCEN           0x80
#define ENC28J60_BIT_ERXFCON_ANDOR          0x40
#define ENC28J60_BIT_ERXFCON_CRCEN          0x20
#define ENC28J60_BIT_ERXFCON_PMEN           0x10
#define ENC28J60_BIT_ERXFCON_MPEN           0x08
#define ENC28J60_BIT_ERXFCON_HTEN           0x04
#define ENC28J60_BIT_ERXFCON_MCEN           0x02
#define ENC28J60_BIT_ERXFCON_BCEN           0x01

#define ENC28J60_BANK_EPKTCNT    0x01
#define ENC28J60_REG_EPKTCNT     0x19

// register @ 0x1A: reserved (common)

// register @ 0x1B: EIE (common)

// register @ 0x1C: EIR (common)

// register @ 0x1D: ESTAT (common)

// register @ 0x1E: ECON2 (common)

// register @ 0x1F: ECON1 (common)

#endif