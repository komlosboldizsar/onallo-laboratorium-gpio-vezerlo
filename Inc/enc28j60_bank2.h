#ifndef _INC_ENC28J60_BANK2_
#define _INC_ENC28J60_BANK2_

// Bank 2

#define ENC28J60_BANK_2                     0x02

#define ENC28J60_BANK_MACON1                0x02
#define ENC28J60_REG_MACON1                 0x00
#define ENC28J60_BIT_MACON1_LOOPBK          0x10
#define ENC28J60_BIT_MACON1_TXPAUS          0x08
#define ENC28J60_BIT_MACON1_RXPAUS          0x04
#define ENC28J60_BIT_MACON1_PASSALL         0x02
#define ENC28J60_BIT_MACON1_MARXEN          0x01

#define ENC28J60_BANK_MACON2                0x02
#define ENC28J60_REG_MACON2                 0x01
#define ENC28J60_BIT_MACON2_MARST           0x80
#define ENC28J60_BIT_MACON2_RNDRST          0x40
#define ENC28J60_BIT_MACON2_MARXRST         0x08
#define ENC28J60_BIT_MACON2_RFUNRST         0x04
#define ENC28J60_BIT_MACON2_MATXRST         0x02
#define ENC28J60_BIT_MACON2_TFUNRST         0x01

#define ENC28J60_BANK_MACON3                0x02
#define ENC28J60_REG_MACON3                 0x02
#define ENC28J60_BIT_MACON3_PADCFG2         0x80
#define ENC28J60_BIT_MACON3_PADCFG1         0x40
#define ENC28J60_BIT_MACON3_PADCFG0         0x20
#define ENC28J60_BIT_MACON3_TXCRCEN         0x10
#define ENC28J60_BIT_MACON3_PHDRLEN         0x08
#define ENC28J60_BIT_MACON3_HFRMEN          0x04
#define ENC28J60_BIT_MACON3_FRMLNEN         0x02
#define ENC28J60_BIT_MACON3_FULDPX          0x01
#define ENC28J60_BITFIELD_MACON_PADCFG      0xE0

#define ENC28J60_BANK_MACON4                0x02
#define ENC28J60_REG_MACON4                 0x03
#define ENC28J60_BIT_MACON4_DEFER           0x40
#define ENC28J60_BIT_MACON4_BPEN            0x20
#define ENC28J60_BIT_MACON4_NOBKOFF         0x10
#define ENC28J60_BIT_MACON4_LONGPRE         0x02
#define ENC28J60_BIT_MACON4_PUREPRE         0x01

#define ENC28J60_BANK_MABBIPG    0x02
#define ENC28J60_REG_MABBIPG     0x04

// register @ 0x05: -

#define ENC28J60_BANK_MAIPGL     0x02
#define ENC28J60_REG_MAIPGL      0x06

#define ENC28J60_BANK_MAIPGH     0x02
#define ENC28J60_REG_MAIPGH      0x07

#define ENC28J60_BANK_MACLCON1   0x02
#define ENC28J60_REG_MACLCON1    0x08

#define ENC28J60_BANK_MACLCON2   0x02
#define ENC28J60_REG_MACLCON2    0x09

#define ENC28J60_BANK_MAMXFLL    0x02
#define ENC28J60_REG_MAMXFLL     0x0A

#define ENC28J60_BANK_MAMXFLH    0x02
#define ENC28J60_REG_MAMXFLH     0x0B

// register @ 0x0C: reserved

#define ENC28J60_BANK_MAPHSUP                0x02
#define ENC28J60_REG_MAPHSUP                 0x0D
#define ENC28J60_BIT_MAPHSUP_RSTINTFC        0x80
#define ENC28J60_BIT_MAPHSUP_RSTRMII         0x08

// register @ 0x0E: reserved

// register @ 0x0F: -

// register @ 0x10: reserved

#define ENC28J60_BANK_MICON                 0x02
#define ENC28J60_REG_MICON                  0x11
#define ENC28J60_BIT_MICON_RSTMII           0x80

#define ENC28J60_BANK_MICMD                 0x02
#define ENC28J60_REG_MICMD                  0x12
#define ENC28J60_BIT_MICMD_MIISCAN          0x02
#define ENC28J60_BIT_MICMD_MIIRD            0x01

// register @ 0x13: -

#define ENC28J60_BANK_MIREGADR   0x02
#define ENC28J60_REG_MIREGADR    0x14

// register @ 0x15: reserved

#define ENC28J60_BANK_MIWRL      0x02
#define ENC28J60_REG_MIWRL       0x16

#define ENC28J60_BANK_MIWRH      0x02
#define ENC28J60_REG_MIWRH       0x17

#define ENC28J60_BANK_MIRDL      0x02
#define ENC28J60_REG_MIRDL       0x18

#define ENC28J60_BANK_MIRDH      0x02
#define ENC28J60_REG_MIRDH       0x19

// register @ 0x1A: reserved (common)

// register @ 0x1B: EIE (common)

// register @ 0x1C: EIR (common)

// register @ 0x1D: ESTAT (common)

// register @ 0x1E: ECON2 (common)

// register @ 0x1F: ECON1 (common)

#endif