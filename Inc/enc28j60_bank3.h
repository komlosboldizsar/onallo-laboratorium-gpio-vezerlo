#ifndef _INC_ENC28J60_BANK3_
#define _INC_ENC28J60_BANK3_

// Bank 0

#define ENC28J60_BANK_3                     0x03

#define ENC28J60_BANK_MAADR1                0x03
#define ENC28J60_REG_MAADR1                 0x00

#define ENC28J60_BANK_MAADR0                0x03
#define ENC28J60_REG_MAADR0                 0x01

#define ENC28J60_BANK_MAADR3                0x03
#define ENC28J60_REG_MAADR3                 0x02

#define ENC28J60_BANK_MAADR2                0x03
#define ENC28J60_REG_MAADR2                 0x03

#define ENC28J60_BANK_MAADR5                0x03
#define ENC28J60_REG_MAADR5                 0x04

#define ENC28J60_BANK_MAADR4                0x03
#define ENC28J60_REG_MAADR4                 0x05

#define ENC28J60_BANK_EBSTSD                0x03
#define ENC28J60_REG_EBSTSD                 0x06

#define ENC28J60_BANK_EBSTCON               0x03
#define ENC28J60_REG_EBSTCON                0x07
#define ENC28J60_BIT_EBSTCON_PSV2           0x80
#define ENC28J60_BIT_EBSTCON_PSV1           0x40
#define ENC28J60_BIT_EBSTCON_PSV0           0x20
#define ENC28J60_BIT_EBSTCON_PSEL           0x10
#define ENC28J60_BIT_EBSTCON_TMSEL1         0x08
#define ENC28J60_BIT_EBSTCON_TMSEL0         0x04
#define ENC28J60_BIT_EBSTCON_TME            0x02
#define ENC28J60_BIT_EBSTCON_BISTST         0x01
#define ENC28J60_BITFIELD_EBSTCON_PSV       0xE0
#define ENC28J60_BITFIELD_EBSTCON_TMSEL     0x0B

#define ENC28J60_BANK_EBSTCSL               0x03
#define ENC28J60_REG_EBSTCSL                0x08

#define ENC28J60_BANK_EBSTCSH               0x03
#define ENC28J60_REG_EBSTCSH                0x09

#define ENC28J60_BANK_MISTAT                0x03
#define ENC28J60_REG_MISTAT                 0x0A
#define ENC28J60_BIT_MISTAT_NVALID          0x04
#define ENC28J60_BIT_MISTAT_SCAN            0x02
#define ENC28J60_BIT_MISTAT_BUSY            0x01

// register @ 0x0B: -

// register @ 0x0C: -

// register @ 0x0D: -

// register @ 0x0E: -

// register @ 0x0F: -

// register @ 0x10: -

// register @ 0x11: -

#define ENC28J60_BANK_EREVID                0x03
#define ENC28J60_REG_EREVID                 0x12

// register @ 0x13: -

// register @ 0x14: -

#define ENC28J60_BANK_ECOCON                0x03
#define ENC28J60_REG_ECOCON                 0x15
#define ENC28J60_BIT_ECOCON_COCON2          0x04
#define ENC28J60_BIT_ECOCON_COCON1          0x02
#define ENC28J60_BIT_ECOCON_COCON0          0x01
#define ENC28J60_BITFIELD_ECOCON_COCON      0x07

// register @ 0x16: reserved

#define ENC28J60_BANK_EFLOCON               0x03
#define ENC28J60_REG_EFLOCON                0x17
#define ENC28J60_BIT_EFLOCON_FULDPXS        0x04
#define ENC28J60_BIT_EFLOCON_FCEN1          0x02
#define ENC28J60_BIT_EFLOCON_FCEN0          0x01
#define ENC28J60_BITFIELD_EFLOCON_FCEN      0x03

#define ENC28J60_BANK_EPAUSL                0x03
#define ENC28J60_REG_EPAUSL                 0x18

#define ENC28J60_BANK_EPAUSH                0x03
#define ENC28J60_REG_EPAUSH                 0x19

// register @ 0x1A: reserved (common)

// register @ 0x1B: EIE (common)

// register @ 0x1C: EIR (common)

// register @ 0x1D: ESTAT (common)

// register @ 0x1E: ECON2 (common)

// register @ 0x1F: ECON1 (common)

#endif