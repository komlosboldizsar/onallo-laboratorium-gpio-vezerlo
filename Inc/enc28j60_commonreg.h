#ifndef _INC_ENC28J60_COMMONREG_
#define _INC_ENC28J60_COMMONREG_

// Common registers

// register @ 0x1A: reserved (common)

#define ENC28J60_REG_EIE                    0x1B
#define ENC28J60_BIT_EIE_INTIE              0x80
#define ENC28J60_BIT_EIE_PKTIE              0x40
#define ENC28J60_BIT_EIE_DMAIE              0x20
#define ENC28J60_BIT_EIE_LINKIE             0x10
#define ENC28J60_BIT_EIE_TXIE               0x08
#define ENC28J60_BIT_EIE_WOLIE              0x04
#define ENC28J60_BIT_EIE_TXERIE             0x02
#define ENC28J60_BIT_EIE_RXERIE             0x01

#define ENC28J60_REG_EIR                    0x1C
#define ENC28J60_BIT_EIR_PKTIF              0x40
#define ENC28J60_BIT_EIR_DMAIF              0x20
#define ENC28J60_BIT_EIR_LINKIF             0x10
#define ENC28J60_BIT_EIR_TXIF               0x08
#define ENC28J60_BIT_EIR_WOLIF              0x04
#define ENC28J60_BIT_EIR_TXERIF             0x02
#define ENC28J60_BIT_EIR_RXERIF             0x01

#define ENC28J60_REG_ESTAT                  0x1D
#define ENC28J60_BIT_ESTAT_INT              0x80
#define ENC28J60_BIT_ESTAT_LATECOL          0x10
#define ENC28J60_BIT_ESTAT_RXBUSY           0x04
#define ENC28J60_BIT_ESTAT_TXABRT           0x02
#define ENC28J60_BIT_ESTAT_CLKRDY           0x01

#define ENC28J60_REG_ECON2                  0x1E
#define ENC28J60_BIT_ECON2_AUTOINC          0x80
#define ENC28J60_BIT_ECON2_PKTDEC           0x40
#define ENC28J60_BIT_ECON2_PWRSV            0x20
#define ENC28J60_BIT_ECON2_VRPS             0x08

#define ENC28J60_REG_ECON1                  0x1F
#define ENC28J60_BIT_ECON1_TXRST            0x80
#define ENC28J60_BIT_ECON1_RXRST            0x40
#define ENC28J60_BIT_ECON1_DMAST            0x20
#define ENC28J60_BIT_ECON1_CSUMEN           0x10
#define ENC28J60_BIT_ECON1_TXRTS            0x08
#define ENC28J60_BIT_ECON1_RXEN             0x04
#define ENC28J60_BIT_ECON1_BSEL1            0x02
#define ENC28J60_BIT_ECON1_BSEL0            0x01
#define ENC28J60_BITFIELD_ECON1_BSEL        0x03

#endif