#ifndef _INC_ENC28J60_PHYREG_
#define _INC_ENC28J60_PHYREG_

// PHY registers

#define ENC28J60_PHYREG_PHCON1              0x00
#define ENC28J60_PHYREG_PHSTAT1             0x01
#define ENC28J60_PHYREG_PHID1               0x02
#define ENC28J60_PHYREG_PHID2               0x03
#define ENC28J60_PHYREG_PHCON2              0x10
#define ENC28J60_PHYREG_PHSTAT2             0x11
#define ENC28J60_PHYREG_PHIE                0x12
#define ENC28J60_PHYREG_PHIR                0x13
#define ENC28J60_PHYREG_PHLCON              0x14

#endif