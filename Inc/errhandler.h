#ifndef ERRHANDLER_H_
#define ERRHANDLER_H_

#include "stm32f4xx_hal.h"

void Error_Handler(void);
void assert_failed(uint8_t *file, uint32_t line);

#endif /* ERRHANDLER_H_ */
