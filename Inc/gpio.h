#ifndef GPIO_H_
#define GPIO_H_

#include "gpiobus_master.h"

extern GPIOBUS gpioBus;
#define H_GPIOBUS    gpioBus
#define P_GPIOBUS    &gpioBus

typedef void (*GPIO_LOG_FUNCTION)(const char* message);

void GPIO_Init(GPIO_LOG_FUNCTION logFunc);
void GPIO_Loop();
void GPIO_Update();
void GPIO_SetNetworkPort(uint16_t port);
void GPIO_GetNetworkPort(uint16_t* port);

#endif /* GPIO_H_ */
