#ifndef GPIOBUS_MASTER_H_
#define GPIOBUS_MASTER_H_

#include "stm32f4xx_hal.h"

#define NUM_LUTS             1024
#define NUM_INPUT_BLOCKS     7
#define NUM_OUTPUT_BLOCKS    7
#define NUM_FFS              64

#define GPIOBUS_CMD_READ    0x01
#define GPIOBUS_CMD_WRITE   0x02

typedef struct _GPIOBUS GPIOBUS;

typedef void (*GPIOBUS_MASTER_SpiCsFunc)(uint8_t value);

typedef enum _GPIOBUS_MASTER_EventType {
	GPIOBUS_MASTER_EVENT_ConfigChanged,
	__GPIOBUS_MASTER_EVENT_TYPECOUNT
} GPIOBUS_MASTER_EventType;

typedef void (*GPIOBUS_MASTER_CallbackFunc)(GPIOBUS* handle, GPIOBUS_MASTER_EventType eventType, uint8_t* data);

typedef struct __attribute__((__packed__)) _GPIOBUS_LookUpTable {
	uint32_t thruthTable;
	uint16_t inputA;
	uint16_t inputB;
	uint16_t inputC;
	uint16_t inputD;
	uint16_t inputE;
	/* 2+8+8 bytes empty */
} GPIOBUS_LookUpTable;

typedef enum _GPIOBUS_FlipFlop_Type {
	FFTYPE_SR_Set = 0x00,
	FFTYPE_SR_Reset = 0x01,
	FFTYPE_JK = 0x02,
	FFTYPE_D = 0x03
} GPIOBUS_FlipFlop_Type;

typedef struct __attribute__((__packed__)) _GPIOBUS_FlipFlop {
	uint8_t /* GPIO_FlipFlop_Type */ type;
	uint8_t defaultValue;
	uint16_t input1;
	uint16_t input2;
	/* 2+8 bytes empty */
} GPIOBUS_FlipFlop;

typedef struct __attribute__((__packed__)) _GPIOBUS_Output {
	uint16_t driver;
	/* 6 bytes empty */
} GPIOBUS_Output;

typedef struct _GPIOBUS_Config {
	GPIOBUS_LookUpTable lookUpTables[NUM_LUTS];
	GPIOBUS_FlipFlop flipFlops[NUM_FFS];
	GPIOBUS_Output outputs[NUM_OUTPUT_BLOCKS*8];
} GPIOBUS_Config;

struct _GPIOBUS {
	uint8_t initialized;
	// Communication
	I2C_HandleTypeDef* handle_I2C;
	SPI_HandleTypeDef* handle_SPI;
	GPIOBUS_MASTER_SpiCsFunc spiCsFunc;
	// Config
	GPIOBUS_Config* currentConfig;
	GPIOBUS_Config* alternativeConfig;
	GPIOBUS_Config config[2];
	uint8_t currentConfigIndex;
	// Value store
	uint8_t previousInputs[NUM_INPUT_BLOCKS];
	uint8_t flipFlopValues[NUM_FFS];
	// Callbacks
	GPIOBUS_MASTER_CallbackFunc callbacks[__GPIOBUS_MASTER_EVENT_TYPECOUNT];
};

void GPIOBUS_Master_Init(GPIOBUS* handle, I2C_HandleTypeDef* handle_I2C, SPI_HandleTypeDef* handle_SPI, GPIOBUS_MASTER_SpiCsFunc spiCsFunc);

void GPIOBUS_Master_SetCallback(GPIOBUS* handle, GPIOBUS_MASTER_EventType eventType, GPIOBUS_MASTER_CallbackFunc function);

void GPIOBUS_Master_ReadIO(GPIOBUS* handle, uint8_t* buffer);
void GPIOBUS_Master_WriteIO(GPIOBUS* handle, uint8_t* data);
void GPIOBUS_Master_UpdateIO(GPIOBUS* handle, uint8_t* pInputChanges);

void GPIOBUS_Master_ResetModule(GPIOBUS* handle, uint8_t module);
void GPIOBUS_Master_GetModuleType(GPIOBUS* handle, uint8_t module, uint8_t* type);
void GPIOBUS_Master_ReadModuleRegister(GPIOBUS* handle, uint8_t module, uint8_t address, uint8_t* data);
void GPIOBUS_Master_WriteModuleRegister(GPIOBUS* handle, uint8_t module, uint8_t address, uint8_t data);

void GPIOBUS_Master_ConfigUpdated(GPIOBUS* handle);

#endif /* GPIOBUS_MASTER_H_ */
