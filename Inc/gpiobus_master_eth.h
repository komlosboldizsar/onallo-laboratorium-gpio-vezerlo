#ifndef GPIOBUS_MASTER_ETH_H_
#define GPIOBUS_MASTER_ETH_H_

#include "gpiobus_master.h"
#include "stm32f4xx_hal.h"
#include "tcpip.h"
#include "mem_i2c.h"

#define NUM_PREDEF_CLIENTS           4
#define NUM_TEMP_CLIENTS             4
#define TEMP_CLIENT_TTL_SEC          300
#define CONFIG_UPDATE_TIMEOUT_SEC    120

typedef void (*GPIOBUS_Master_Eth_LogFunc)(const char* msg);
typedef uint32_t (*GPIOBUS_Master_Eth_TimeFunc)();

typedef struct __attribute__((__packed__)) _GPIOBUS_Master_Eth_Client {
	uint8_t ipAddr[4];
	uint16_t port;
	uint8_t valid;
	/* padding */ uint8_t __padding1[1];
	uint8_t mask[NUM_INPUT_BLOCKS];
	/* 8+8 bytes empty */
} GPIOBUS_Master_Eth_Client;

typedef struct _GPIOBUS_Master_Eth_PredefClient {
	GPIOBUS_Master_Eth_Client client;
} GPIOBUS_Master_Eth_PredefClient;

typedef struct _GPIOBUS_Master_Eth_TempClient {
	GPIOBUS_Master_Eth_Client client;
	uint32_t validUntil;
} GPIOBUS_Master_Eth_TempClient;

typedef struct _GPIOBUS_Master_Eth_UpdateProcess {
	uint8_t updating;
	uint16_t threadId;
	uint16_t messageCount;
	uint16_t messageChecksum;
	uint32_t timeoutLimit;
	uint8_t formatError;
	uint8_t srcIp[4];
	uint16_t srcPort;
} GPIOBUS_Master_Eth_UpdateProcess;

typedef struct _GPIOBUS_Master_Eth_Handle {
	GPIOBUS* bus;
	MEM_I2C* configMemHandle;
	TcpIpStack* tcpIpStack;
	uint16_t udpPort;
	GPIOBUS_Master_Eth_LogFunc logFunc;
	GPIOBUS_Master_Eth_TimeFunc timeFunc;
	GPIOBUS_Master_Eth_PredefClient predefClients[NUM_PREDEF_CLIENTS];
	GPIOBUS_Master_Eth_TempClient tempClients[NUM_TEMP_CLIENTS];
	GPIOBUS_Master_Eth_UpdateProcess updateProcess;
} GPIOBUS_Master_Eth_Handle;

void GPIOBUS_Master_Eth_Init(GPIOBUS_Master_Eth_Handle* handle, GPIOBUS* bus, MEM_I2C* configMemHandle, TcpIpStack* tcpIpStack, GPIOBUS_Master_Eth_LogFunc logFunc, GPIOBUS_Master_Eth_TimeFunc timeFunc);
void GPIOBUS_Master_Eth_Loop(GPIOBUS_Master_Eth_Handle* handle);
void GPIOBUS_Master_Eth_UdpReceive(uint8_t* sourceIp, uint16_t sourcePort, uint8_t* payload, uint16_t payloadLength, void* tagP /* handle */, uint8_t tagD);
void GPIOBUS_Master_Eth_SetPort(GPIOBUS_Master_Eth_Handle* handle, uint16_t port);
void GPIOBUS_Master_Eth_GetPort(GPIOBUS_Master_Eth_Handle* handle, uint16_t* port);

#endif /* GPIOBUS_MASTER_ETH_H_ */
