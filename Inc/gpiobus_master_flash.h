#ifndef GPIOBUS_MASTER_FLASH_H_
#define GPIOBUS_MASTER_FLASH_H_

#include "gpiobus_master.h"
#include "stm32f4xx_hal.h"
#include "mem_spi.h"

typedef struct _GPIOBUS_Master_Flash {
	GPIOBUS* bus;
	MEM_SPI* configMem;
} GPIOBUS_Master_Flash;

void GPIOBUS_Master_Flash_Init(GPIOBUS_Master_Flash* handle, GPIOBUS* bus, MEM_SPI* configMem);
void GPIOBUS_Master_Flash_LoadConfig(GPIOBUS_Master_Flash* handle, uint8_t toAlternative, uint8_t initial);
void GPIOBUS_Master_Flash_SaveConfig(GPIOBUS_Master_Flash* handle, uint8_t fromAlternative);

#endif /* GPIOBUS_MASTER_FLASH_H_ */
