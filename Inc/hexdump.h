#ifndef HEXDUMP_H_
#define HEXDUMP_H_

#include "stm32f4xx_hal.h"

void hexdump(uint8_t* data, uint16_t length, void (*printLnFunc)(const char* text));

#endif /* HEXDUMP_H_ */
