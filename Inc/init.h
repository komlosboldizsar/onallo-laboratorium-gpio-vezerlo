#ifndef INIT_H_
#define INIT_H_

void TotalSystemInit();
void SystemClock_Config(void);
void MX_GPIO_Init(void);
void MX_USART2_UART_Init(void);
void MX_SPI2_Init(void);
void MX_I2C1_Init(void);
void MX_SPI1_Init(void);
void MX_SPI3_Init(void);
void MX_TIM1_Init(void);

#endif /* INIT_H_ */
