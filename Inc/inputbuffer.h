#ifndef INPUTBUFFER_H_
#define INPUTBUFFER_H_

#include "inttypes.h"

#define INPUTBUFFER_SLOT_SIZE  100
#define INPUTBUFFER_SLOT_COUNT 10

typedef void (*InputBuffer_UnitReceivedHandler)(const char* unit);

typedef struct _InputBufferSlot {
	char content[INPUTBUFFER_SLOT_SIZE+1];
	uint8_t /* bool */ ready;
	uint8_t  /* bool */ overflow;
	uint16_t received;
} InputBufferSlot;

typedef struct _InputBuffer {
	InputBufferSlot slots[INPUTBUFFER_SLOT_COUNT];
	char unitSeparator;
	InputBuffer_UnitReceivedHandler unitReceivedHandler;
	uint16_t slotRdPtr;
	uint16_t slotWrPtr;
} InputBuffer;

void InputBuffer_Init(InputBuffer* handle, char unitSeparator, InputBuffer_UnitReceivedHandler unitReceivedHandler);
void InputBuffer_Clear(InputBuffer* handle);
void InputBuffer_Received_Chr(InputBuffer* handle, char chr);
void InputBuffer_Loop(InputBuffer* handle);

#endif /* INPUTBUFFER_H_ */
