#ifndef MACRO_H_
#define MACRO_H_

#define NOPWAIT(T) for(int wait = 0; wait < T; wait++) \
		               asm("nop");

#define PORT(P)             P##_GPIO_Port
#define PIN(P)              P##_Pin
#define GPIO_WRITE(P, V)    HAL_GPIO_WritePin(PORT(P), PIN(P), V)
#define GPIO_ON(P)          HAL_GPIO_WritePin(PORT(P), PIN(P), 1)
#define GPIO_OFF(P)         HAL_GPIO_WritePin(PORT(P), PIN(P), 0)
#define GPIO_READ(P)        HAL_GPIO_ReadPin(PORT(P), PIN(P))
#define GPIO_TOGGLE(P)      HAL_GPIO_TogglePin(PORT(P), PIN(P))

#define UPPER8(B)		    ((uint8_t)(((B) & 0xFF00) >> 8))
#define LOWER8(B)		    ((uint8_t)((B) & 0xFF))

#endif /* MACRO_H_ */
