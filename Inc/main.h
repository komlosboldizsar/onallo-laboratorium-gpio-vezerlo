/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_ERROR_Pin GPIO_PIN_0
#define LED_ERROR_GPIO_Port GPIOC
#define LED_HEARTBEAT_Pin GPIO_PIN_1
#define LED_HEARTBEAT_GPIO_Port GPIOC
#define LED_STATUS1_Pin GPIO_PIN_2
#define LED_STATUS1_GPIO_Port GPIOC
#define LED_STATUS2_Pin GPIO_PIN_3
#define LED_STATUS2_GPIO_Port GPIOC
#define CONSOLE_UART_TX_Pin GPIO_PIN_2
#define CONSOLE_UART_TX_GPIO_Port GPIOA
#define CONSOLE_UART_RX_Pin GPIO_PIN_3
#define CONSOLE_UART_RX_GPIO_Port GPIOA
#define FLASH_SPI_CS_Pin GPIO_PIN_4
#define FLASH_SPI_CS_GPIO_Port GPIOA
#define FLASH_SPI_SCK_Pin GPIO_PIN_5
#define FLASH_SPI_SCK_GPIO_Port GPIOA
#define FLASH_SPI_MISO_Pin GPIO_PIN_6
#define FLASH_SPI_MISO_GPIO_Port GPIOA
#define FLASH_SPI_MOSI_Pin GPIO_PIN_7
#define FLASH_SPI_MOSI_GPIO_Port GPIOA
#define ETH_RST_Pin GPIO_PIN_2
#define ETH_RST_GPIO_Port GPIOB
#define ETH_SPI_SCK_Pin GPIO_PIN_10
#define ETH_SPI_SCK_GPIO_Port GPIOB
#define ETH_SPI_CS_Pin GPIO_PIN_12
#define ETH_SPI_CS_GPIO_Port GPIOB
#define ENT_INT_Pin GPIO_PIN_13
#define ENT_INT_GPIO_Port GPIOB
#define ENT_INT_EXTI_IRQn EXTI15_10_IRQn
#define ETH_SPI_MISO_Pin GPIO_PIN_14
#define ETH_SPI_MISO_GPIO_Port GPIOB
#define ETH_SPI_MOSI_Pin GPIO_PIN_15
#define ETH_SPI_MOSI_GPIO_Port GPIOB
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define MODULAR_SPI_SCK_Pin GPIO_PIN_10
#define MODULAR_SPI_SCK_GPIO_Port GPIOC
#define MODULAR_SPI_MISO_Pin GPIO_PIN_11
#define MODULAR_SPI_MISO_GPIO_Port GPIOC
#define MODULAR_SPI_MOSI_Pin GPIO_PIN_12
#define MODULAR_SPI_MOSI_GPIO_Port GPIOC
#define MODULAR_SPI_START_Pin GPIO_PIN_2
#define MODULAR_SPI_START_GPIO_Port GPIOD
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define I2C_SCL_Pin GPIO_PIN_8
#define I2C_SCL_GPIO_Port GPIOB
#define I2C_SDA_Pin GPIO_PIN_9
#define I2C_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
