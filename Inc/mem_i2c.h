#ifndef MEM_I2C_H_
#define MEM_I2C_H_

#include "stm32f4xx_hal.h"

#define ADDR_BASE 0x50

typedef enum _MEM_I2C_SIZE {
	MEM_SIZE_32K = 32,
	MEM_SIZE_64K = 64
} MEM_I2C_SIZE;

typedef struct _MEM_I2C {
	I2C_HandleTypeDef* portHandle;
	unsigned int i2c_address;
	MEM_I2C_SIZE size;
} MEM_I2C;

int MEM_I2C_Init(MEM_I2C* handle, I2C_HandleTypeDef* portHandle, unsigned int address, MEM_I2C_SIZE size);

int MEM_I2C_ReadBytes(MEM_I2C* handle, uint16_t address, unsigned int count, unsigned char* buffer);
int MEM_I2C_ReadUint16s(MEM_I2C* handle, uint16_t address, unsigned int count, uint16_t* buffer);
int MEM_I2C_ReadUint32s(MEM_I2C* handle, uint16_t address, unsigned int count, uint32_t* buffer);

int MEM_I2C_WriteByte(MEM_I2C* handle, uint16_t address, unsigned char data);
int MEM_I2C_WritePage(MEM_I2C* handle, uint16_t address, unsigned int count, unsigned char* data);
int MEM_I2C_WriteUint16(MEM_I2C* handle, uint16_t address, uint16_t data);
int MEM_I2C_WriteUint32(MEM_I2C* handle, uint16_t address, uint32_t data);

#endif /* MEM_I2C_H_ */
