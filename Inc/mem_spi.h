#ifndef MEM_SPI_H_
#define MEM_SPI_H_

#include "stm32f4xx_hal.h"

typedef void (*MEM_SPI_CS_FUNC)(uint8_t value);

typedef struct _MEM_SPI {
	SPI_HandleTypeDef* portHandle;
	MEM_SPI_CS_FUNC csFunc;
} MEM_SPI;

void MEM_SPI_Init(MEM_SPI* handle, SPI_HandleTypeDef* portHandle, MEM_SPI_CS_FUNC csFunc);

void MEM_SPI_Read(MEM_SPI* handle, uint32_t address, uint32_t count, uint8_t* buffer);
void MEM_SPI_Write(MEM_SPI* handle, uint32_t address, uint32_t count, uint8_t* data);
void MEM_SPI_WritePages(MEM_SPI* handle, uint32_t address, uint32_t count, uint8_t* data);

void MEM_SPI_Erase4K(MEM_SPI* handle, uint32_t address);
void MEM_SPI_Erase32K(MEM_SPI* handle, uint32_t address);
void MEM_SPI_Erase64K(MEM_SPI* handle, uint32_t address);
void MEM_SPI_EraseFull(MEM_SPI* handle);

void MEM_SPI_WriteEnable(MEM_SPI* handle);
void MEM_SPI_WriteDisable(MEM_SPI* handle);

#endif /* MEM_SPI_H_ */
