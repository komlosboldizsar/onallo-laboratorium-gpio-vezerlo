#ifndef NETWORK_H_
#define NETWORK_H_

#include "enc28j60.h"
#include "tcpip.h"
#include "stm32f4xx_hal.h"
#include "config_eeprom.h"

extern ENC28J60 enc28j60;
extern TcpIpStack tcpIpStack;

#define H_TCPIPSTACK   tcpIpStack
#define P_TCPIPSTACK   &tcpIpStack

typedef void (*NETWORK_LOG_FUNCTION)(const char* message);

void networkInit(NETWORK_LOG_FUNCTION logFunction);
void networkLoop();
void networkReceiveIT();
void convertNetmaskLengthToNetmask(uint8_t length, uint8_t* mask);

void networkGetMac(uint8_t* mac);
void networkGetIp(uint8_t* ip);
void networkGetNetmask(uint8_t* netmask);
void networkGetNetmaskLength(uint8_t* netmaskLength);
void networkGetGateway(uint8_t* gateway);

void networkSetMac(uint8_t* mac);
void networkSetIp(uint8_t* ip);
int networkSetNetmask(uint8_t* netmask);
void networkSetNetmaskLength(uint8_t* netmaskLength);
void networkSetGateway(uint8_t* gateway);

#endif /* NETWORK_H_ */
