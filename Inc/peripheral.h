#ifndef PERIPHERAL_H_
#define PERIPHERAL_H_

#include "stm32f4xx_hal.h"

extern I2C_HandleTypeDef hi2c1;
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;
extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart2;

#define H_I2C                hi2c1
#define H_SPI_FLASH          hspi1
#define H_SPI_ETH            hspi2
#define H_SPI_MODULAR        hspi3
#define H_TIM_GPIOUPDATER    htim1
#define H_UART_CONSOLE       huart2

#define P_I2C                &hi2c1
#define P_SPI_FLASH          &hspi1
#define P_SPI_ETH            &hspi2
#define P_SPI_MODULAR        &hspi3
#define P_TIM_GPIOUPDATER    &htim1
#define P_UART_CONSOLE       &huart2

#endif /* PERIPHERAL_H_ */
