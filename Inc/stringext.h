#ifndef STRINGEXT_H_
#define STRINGEXT_H_

#include <stdio.h>

int strlimcat(char* destination, const char* source, size_t maxlength);
int strlimcpy(char* destination, const char* source, size_t maxlength);
int strnlimcpy(char* destination, const char* source, size_t num, size_t maxlength);

#endif /* STRINGEXT_H_ */
