#ifndef _INC_TCPIP_
#define _INC_TCPIP_

#define TCPIP_ARP_CACHE_SIZE                    16
#define TCPIP_ARP_CACHE_TTL                     15000

#define TCPIP_TX_BUFFER_SIZE                    16
#define TCPIP_TX_TIMEOUT                        5

#define TCPIP_UDP_MAX_APPS                      4
#define TCPIP_UDP_APPREG_ERRCODE_PORTRESERVED   1
#define TCPIP_UDP_APPREG_ERRCODE_NOFREESLOT     1

#include "stm32f4xx_hal.h"

#include "tcpip_eth.h"
#include "tcpip_arp.h"
#include "tcpip_ipv4.h"
#include "tcpip_icmp.h"
#include "tcpip_udp.h"
#include "tcpip_helper.h"
#include "tcpip_debug.h"

typedef void (*TransmitFunc)(uint8_t* pFrameData, uint16_t length);
typedef void (*TcpIpLogFunc)(const char* text);
typedef uint32_t (*TimeFunc)();

typedef struct _TcpIpStack {
	uint8_t ipAddress[4];
	uint8_t subnetMask[4];
	uint8_t defaultGateway[4];
	uint8_t macAddress[TCPIP_ETHFRM_MACADDR_LEN];
	TransmitFunc transmitFunc;
	TcpIpLogFunc logFunc;
	TimeFunc timeFunc;
	ArpCacheEntry arpCache[TCPIP_ARP_CACHE_SIZE];
	Ipv4TxBufferEntry ipv4TxBuffer[TCPIP_TX_BUFFER_SIZE];
	UdpApplication udpApplications[TCPIP_UDP_MAX_APPS];
	uint8_t /* TcpIpStackDebugLevel */ debugOptions[_TCPIP_DBG_OPT_COUNT];
} TcpIpStack;

void TcpIp_InitStruct(TcpIpStack* instance, TransmitFunc transmitFunc, TcpIpLogFunc logFunc, TimeFunc timeFunc);
void TcpIp_Init(TcpIpStack* instance, uint8_t* ipAddress, uint8_t* subnetMask, uint8_t* defaultGateway, uint8_t* macAddress);

#endif
