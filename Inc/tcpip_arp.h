#ifndef TCPIP_ARP_H_
#define TCPIP_ARP_H_

#include "tcpip_stack_decl.h"

typedef struct __attribute__((packed)) _ArpPacket {
	uint16_t hType;
	uint16_t pType;
	uint8_t hLen;
	uint8_t pLen;
	uint16_t oper;
	uint8_t sha[6];
	uint8_t spa[4];
	uint8_t tha[6];
	uint8_t tpa[4];
} ArpPacket;

typedef enum _ArpCacheEntryState {
	ACES_INVALID,
	ACES_PENDING,
	ACES_VALID
} ArpCacheEntryState;

typedef struct _ArpCacheEntry {
	ArpCacheEntryState state;
	uint32_t createTime;
	uint32_t age;
	uint8_t ip[4];
	uint8_t mac[6];
} ArpCacheEntry;

void TcpIp_Arp_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length);
void TcpIp_Arp_SendReply(TcpIpStack* instance, uint8_t* dstMac, uint8_t* dstIp);
void TcpIp_Arp_SendRequest(TcpIpStack* instance, uint8_t* dstIp);
ArpCacheEntry* TcpIp_Arp_GetEntryForIp(TcpIpStack* instance, uint8_t* ipAddress);
ArpCacheEntry* TcpIp_Arp_GetNearestEntryForIp(TcpIpStack* instance, uint8_t* ipAddress);
void TcpIp_Arp_AddIpToCache(TcpIpStack* instance, uint8_t* mac, uint8_t* ip);
void TcpIp_Arp_Process(TcpIpStack* instance);
void TcpIp_Arp_PrintTable(TcpIpStack* instance);

#endif /* TCPIP_ARP_H_ */
