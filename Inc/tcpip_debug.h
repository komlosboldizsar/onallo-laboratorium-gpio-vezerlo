#ifndef TCPIP_DEBUG_H_
#define TCPIP_DEBUG_H_

#include "tcpip_stack_decl.h"
#include "hexdump.h"
#include <stdio.h>

extern char tcpIpDebugBuffer[];

typedef enum _TcpIpStackDebugOption {
	// Receive and Transmit
	TCPIP_DBG_OPT_EthernetReceive,
	TCPIP_DBG_OPT_EthernetTransmit,
	TCPIP_DBG_OPT_ArpReceive,
	TCPIP_DBG_OPT_ArpTransmit,
	TCPIP_DBG_OPT_Ipv4Receive,
	TCPIP_DBG_OPT_Ipv4Transmit,
	TCPIP_DBG_OPT_IcmpReceive,
	TCPIP_DBG_OPT_IcmpTransmit,
	TCPIP_DBG_OPT_UdpReceive,
	TCPIP_DBG_OPT_UdpTransmit,
	// ARP events
	TCPIP_DBG_OPT_ArpCacheEntryAdd,
	TCPIP_DBG_OPT_ArpCacheEntryDrop,
	// END
	_TCPIP_DBG_OPT_COUNT
} TcpIpStackDebugOption;

#define DEBUG_FORMAT(FORMAT_STR, ARGS...)   		sprintf(tcpIpDebugBuffer, FORMAT_STR, ARGS)
#define DEBUG_PRINT_BUFFER()	          			instance->logFunc(tcpIpDebugBuffer)
#define DEBUG_PRINT(STR)	          				instance->logFunc(STR)
#define DEBUG_FORMAT_PRINT(FORMAT_STR, ARGS...)		DEBUG_FORMAT(FORMAT_STR, ARGS); \
													DEBUG_PRINT_BUFFER();

#define DEBUG_SEPARATOR_MINLEVEL_DEFAULT            2

#define DEBUG_START_SEPARATOR(TAG)                  DEBUG_FORMAT_PRINT("[%s] ** >>>>", TAG)
#define DEBUG_START_ML(OPT, TAG, LVL)				DEBUG_BLOCK(OPT, LVL) { \
														DEBUG_START_SEPARATOR(TAG); \
													}
#define DEBUG_START(OPT, TAG)						DEBUG_START_ML(OPT, TAG, DEBUG_SEPARATOR_MINLEVEL_DEFAULT)

#define DEBUG_END_SEPARATOR(TAG)                    DEBUG_FORMAT_PRINT("[%s] ** <<<<", TAG)
#define DEBUG_END_ML(OPT, TAG, LVL)					DEBUG_BLOCK(OPT, LVL) { \
														DEBUG_END_SEPARATOR(TAG); \
													}
#define DEBUG_END(OPT, TAG)							DEBUG_END_ML(OPT, TAG, DEBUG_SEPARATOR_MINLEVEL_DEFAULT)

#define DEBUG_OPT(OPT)								TCPIP_DBG_OPT_##OPT
#define DEBUG_BLOCK(OPT, LVL)						if (instance->debugOptions[DEBUG_OPT(OPT)] >= LVL)

#define DEBUG_HEXDUMP(DATA, LENGTH)		         	hexdump(DATA, LENGTH, instance->logFunc);

void TcpIp_Debug_SetLevel(TcpIpStack* instance, TcpIpStackDebugOption option, uint8_t level);
void TcpIp_Debug_UndebugAll(TcpIpStack* instance);

#endif /* TCPIP_DEBUG_H_ */
