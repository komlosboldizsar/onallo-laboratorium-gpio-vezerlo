#ifndef TCPIP_ETH_H_
#define TCPIP_ETH_H_

#include "tcpip_stack_decl.h"

#define TCPIP_ETHTYPE_IPV4          0x0800
#define TCPIP_ETHTYPE_ARP           0x0806

typedef struct __attribute__((packed)) _EthernetFrame {
	uint8_t dstMac[6];
	uint8_t srcMac[6];
	uint16_t type;
	uint8_t payload[TCPIP_ETHFRM_PAYLOAD_MAXLEN];
	uint16_t payloadLength;
} EthernetFrame;

void TcpIp_ReceiveEthFrame(TcpIpStack* instance, uint8_t* data, uint16_t length);
void TcpIp_ReceiveEthFrameStruct(TcpIpStack* instance, EthernetFrame* frame);
void TcpIp_TransmitEthFrame(TcpIpStack* instance, uint8_t* data, uint16_t length);
void TcpIp_TransmitEthFrameStruct(TcpIpStack* instance, EthernetFrame* frame);
void TcpIp_TransmitEthFrameDetailed(TcpIpStack* instance, uint8_t* dstMac, uint8_t* srcMac, uint16_t type, uint8_t* payload, uint16_t payloadLength);
void TcpIp_TransmitEthFrameDetailedOwnMac(TcpIpStack* instance, uint8_t* dstMac, uint16_t type, uint8_t* payload, uint16_t payloadLength);

#endif /* TCPIP_ETH_H_ */
