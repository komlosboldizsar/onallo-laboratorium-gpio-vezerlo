#ifndef TCPIP_HELPER_H_
#define TCPIP_HELPER_H_

#include "stm32f4xx_hal.h"

int sameSubnet(uint8_t* ip1, uint8_t* ip2, uint8_t* mask);
int arrEq(uint8_t* arr1, uint8_t* arr2, uint16_t length);
void macToStr(uint8_t* macAddress, char* buffer);
void ipToStr(uint8_t* ipAddress, char* buffer);

#define CONV32BYTES(A,B,C,D)            ((uint32_t)(((A)<<24)|((B)<<16)|((C)<<8)|(D)))
#define CONV16BYTES(A,B)                ((uint16_t)(((A)<<8)|(B)))
#define CONV_IPARR8_TO_IP32(ARR)        CONV32BYTES(ARR[3], ARR[2], ARR[1], ARR[0])
#define CPYBYTES(FROM, TO, COUNT)   	for (int i = 0; i < COUNT; i++) \
									        TO[i] = FROM[i];

#define SWP16(UINT16)                   ((uint16_t)(((UINT16)>>8)|((UINT16)<<8)))
#define SWP32(UINT32)                   ((uint32_t)((UINT32<<24)|((UINT32<<8)&0x00FF0000)|((UINT32>>8)&0x0000FF00)|(UINT32>>24)))

#define EQ32(UINT32A, UINT32B)          ((UINT32A[3] == UINT32B[3])|(UINT32A[2] == UINT32B[2])|(UINT32A[1] == UINT32B[1])|(UINT32A[0] == UINT32B[0]))

#endif /* TCPIP_HELPER_H_ */
