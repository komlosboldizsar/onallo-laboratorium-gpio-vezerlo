#ifndef TCPIP_ICMP_H_
#define TCPIP_ICMP_H_

#include "tcpip_stack_decl.h"
#include "tcpip_ipv4.h"

typedef struct __attribute__((packed)) _IcmpPacket {
	uint8_t type;
	uint8_t code;
	uint16_t checksum;
	uint8_t rest[64];
	uint16_t restLength;
} IcmpPacket;

void TcpIp_Icmp_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length, Ipv4Packet* parent);
void TcpIp_Icmp_SendEchoReply(TcpIpStack* instance, IcmpPacket* request, uint8_t* dstIp);


#endif /* TCPIP_ICMP_H_ */
