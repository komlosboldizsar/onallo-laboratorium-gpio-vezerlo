#ifndef TCPIP_IPV4_H_
#define TCPIP_IPV4_H_

#include "tcpip_stack_decl.h"

#define TCPIP_IPV4PROTO_ICMP        0x01
#define TCPIP_IPV4PROTO_UDP         0x11

typedef struct __attribute__((packed)) _Ipv4Packet {
	uint8_t _version_ihl;
	uint8_t _dscp_ecn;
	uint16_t totalLength;
	uint16_t identification;
	uint16_t _flags_fragmentOffset;
	uint8_t ttl;
	uint8_t proto;
	uint16_t headerChecksum;
	uint8_t srcIp[4];
	uint8_t dstIp[4];
	uint8_t payload[1500];
	uint16_t payloadLength;
} Ipv4Packet;

typedef enum _Ipv4TxBufferEntryState {
	IPV4TXBES_EMPTY,
	IPV4TXBES_QUEUED,
	IPv4TXBES_WAITING_ARP
} Ipv4TxBufferEntryState;

typedef struct _Ipv4TxBufferEntry {
	Ipv4Packet packet;
	uint8_t nextIp[4];
	float txTimeoutEnd;
	Ipv4TxBufferEntryState state;
} Ipv4TxBufferEntry;

void TcpIp_Ipv4_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length);
int TcpIp_Ipv4_SendPacket(TcpIpStack* instance, uint8_t* dstIp, uint8_t proto, uint8_t* payload, uint16_t payloadLength);
void TcpIp_Ipv4_SendProcess(TcpIpStack* instance);

#endif /* TCPIP_IPV4_H_ */
