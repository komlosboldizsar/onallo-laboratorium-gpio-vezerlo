#ifndef TCPIP_STACK_DECL_H_
#define TCPIP_STACK_DECL_H_

typedef struct _TcpIpStack TcpIpStack;

#define TCPIP_ETHFRM_PAYLOAD_MAXLEN             1500
#define TCPIP_ETHFRM_MACADDR_LEN                6
#define TCPIP_ETHFRM_TYPEFIELD_LEN              2
#define TCPIP_UDPPKT_PAYLOAD_MAXLEN             1500

#endif /* TCPIP_STACK_DECL_H_ */
