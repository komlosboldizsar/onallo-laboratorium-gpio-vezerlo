#ifndef TCPIP_UDP_H_
#define TCPIP_UDP_H_

#include "tcpip_stack_decl.h"
#include "tcpip_ipv4.h"

typedef struct __attribute__((packed)) _UdpPacket {
	uint16_t srcPort;
	uint16_t dstPort;
	uint16_t length;
	uint16_t checksum;
	uint8_t payload[TCPIP_UDPPKT_PAYLOAD_MAXLEN];
} UdpPacket;

typedef struct __attribute__((packed)) _UdpIpv4PseudoHeader {
	uint8_t srcIp[4];
	uint8_t dstIp[4];
	uint8_t zeroes;
	uint8_t protocol;
	uint16_t udpLength;
} UdpIpv4PseudoHeader;

typedef void (*UdpApplicationReceiveFunc)(uint8_t* sourceIp, uint16_t sourcePort, uint8_t* payload, uint16_t payloadLength, void* tagP, uint8_t tagD);

typedef struct _UdpApplication {
	uint16_t port;
	UdpApplicationReceiveFunc receiveFunc;
	void* tagP;
	uint8_t tagD;
	uint8_t valid;
} UdpApplication;

void TcpIp_Udp_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length, Ipv4Packet* parent);
void TcpIp_Udp_SendPacket(TcpIpStack* instance, uint8_t* dstIp, uint16_t dstPort, uint16_t srcPort, uint8_t* payload, uint16_t payloadLength);
int TcpIp_Udp_RegisterApplication(TcpIpStack* instance, uint16_t port, UdpApplicationReceiveFunc receiveFunc, void* tagP, uint8_t tagD);

#endif /* TCPIP_UDP_H_ */
