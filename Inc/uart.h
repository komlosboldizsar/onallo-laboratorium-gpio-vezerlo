#ifndef UART_H_
#define UART_H_

#include "stm32f4xx_hal.h"

typedef void (*UART_ChrRecvCallback)(char c);

void UART_TX(const char* str);
void UART_RX_Start(UART_ChrRecvCallback chrRecvCallback, uint8_t echo);
void UART_RX_Stop();

#endif /* UART_H_ */
