#include "checksum.h"
#include "inttypes.h"

uint16_t computeChecksum(uint8_t* data, uint16_t length) {
	uint32_t checksum = 0x00000000;
	while (length > 1) {
		checksum += *(uint16_t*)data;
		data += 2;
		length -= 2;
	}
	if (length > 0)
		checksum += *data;
	while (checksum >> 16)
		checksum = (checksum & 0xFFFF) + (checksum >> 16);
	checksum = ~checksum;
	return (uint16_t)(checksum & 0xFFFF);
}

uint16_t combineChecksum(uint16_t checksum1, uint16_t checksum2) {
	uint32_t checksum = ((~checksum1) + (~checksum2)) & 0x0001FFFF;
	while (checksum >> 16)
		checksum = (checksum & 0xFFFF) + (checksum >> 16);
	checksum = ~checksum;
	return (uint16_t)(checksum & 0xFFFF);
}
