#include "cli.h"
#include "cli_submenu_network.h"
#include "string.h"
#include "inputbuffer.h"
#include "stdio.h"
#include "uart.h"
#include "stringext.h"

#define CLI_MAX_ARG_LENGTH  40
#define CLI_MAX_ARG_COUNT 10

CLI_PrintFunc cliPrintFunc;
CLI_Submenu* currentSubmenu;
InputBuffer inputBuffer;

#define ENTERSUBMENU(SUBMENU_ARG, SUBMENU_STRUCT)   	    if (strcmp(SUBMENU_STRUCT.menuName, SUBMENU_ARG) == 0) { \
															    currentSubmenu = &SUBMENU_STRUCT; \
															    return; \
															}

#define MIN(A,B) (((A) < (B)) ? (A) : (B))

static void CLI_PrintPrompt();
static void CLI_EnterSubmenu(const char* submenuName);
static void splitArguments(const char* arguments, char** argv, unsigned int* argc, unsigned int maxArgLength, unsigned int maxArgCount);

void CLI_Init(CLI_PrintFunc printFunc) {
	InputBuffer_Init(&inputBuffer, '\r', &CLI_HandleLine);
	cliPrintFunc = printFunc;
}

void CLI_Loop() {
	InputBuffer_Loop(&inputBuffer);
}

void CLI_Print(const char* str) {
	cliPrintFunc(str);
}

void CLI_PrintLn(const char* str) {
	cliPrintFunc(str);
	cliPrintFunc("\r\n");
}

void CLI_PrintResp(const char* str) {
	cliPrintFunc("> ");
	cliPrintFunc(str);
}

void CLI_PrintRespLn(const char* str) {
	cliPrintFunc("> ");
	cliPrintFunc(str);
	cliPrintFunc("\r\n");
}

void CLI_HandleChar(char chr) {
	InputBuffer_Received_Chr(&inputBuffer, chr);
}

void CLI_HandleLine(const char* line) {

	unsigned int argc;
	char* argv[CLI_MAX_ARG_COUNT];
	char argvstr[CLI_MAX_ARG_COUNT*(CLI_MAX_ARG_LENGTH+1)];
	int k;
	for(k = 0; k < CLI_MAX_ARG_COUNT; k++) {
		argv[k] = &argvstr[(CLI_MAX_ARG_LENGTH+1)*k];
		argv[k][CLI_MAX_ARG_LENGTH] = '\0';
	}
	splitArguments(line, argv, &argc, CLI_MAX_ARG_LENGTH, CLI_MAX_ARG_COUNT);

	if (argc > 0) {
		if (currentSubmenu != NULL) {
			if (strcmp(argv[0], "exit") == 0) {
				CLI_ExitSubmenu();
			} else {
				currentSubmenu->lineHandlerFunc(argc, argv);
			}
		} else {
			CLI_EnterSubmenu(argv[0]);
		}
	}

	CLI_PrintPrompt();

}

void CLI_ExitSubmenu() {
	currentSubmenu = NULL;
}

static void CLI_PrintPrompt() {
	char submenuName[SUBMENU_NAME_MAXLEN+1+1];
	submenuName[0] = '\0';
	if (currentSubmenu != NULL) {
		submenuName[0] = '-';
		strcpy(&submenuName[1], currentSubmenu->menuName);
	}
	char promptBuffer[SUBMENU_NAME_MAXLEN+10];
	sprintf(promptBuffer, "(config%s)# ", submenuName);
	CLI_Print(promptBuffer);
}

static void CLI_EnterSubmenu(const char* submenuName) {
	ENTERSUBMENU(submenuName, CLI_SUBMENU_Network);
	CLI_PrintRespLn("Unknown command");
}

static void splitArguments(const char* arguments, char** argv, unsigned int* argc, unsigned int maxArgLength, unsigned int maxArgCount) {
	*argc = 0;
	const char* argStart = NULL;
	const char* c = arguments;
	while((*argc < maxArgCount) && ((*c != '\0') && (*c != '\r') && (*c != '\n'))) {
		if(((*c != '0') && (*c != ' ')) && (argStart == NULL)) {
			argStart = c;
		}
		if((*c == ' ') && (argStart != NULL)) {
			int argLength = MIN(maxArgLength, (c-argStart));
			strlimcpy(argv[*argc], argStart, argLength);
			(*argc)++;
			argStart = NULL;
		}
		c++;
	}
	if((argStart != NULL) && (*argc < maxArgCount)) {
		int argLength = MIN(maxArgLength, (c-argStart));
        strlimcpy(argv[*argc], argStart, argLength);
        (*argc)++;
	}
}
