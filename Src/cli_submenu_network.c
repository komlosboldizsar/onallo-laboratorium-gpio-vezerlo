#include "cli_submenu_network.h"
#include "cli.h"
#include "cli_submenu.h"
#include "tcpip_helper.h"
#include "network.h"
#include "gpio.h"

CLI_Submenu CLI_SUBMENU_Network = {
	.menuName = "network",
	.lineHandlerFunc = &CLI_SUBMENU_Network_LineHandlerFunc
};

static void CLI_SUBMENU_Network_Show(int argc, char* const* argv);
static void CLI_SUBMENU_Network_Mac(int argc, char* const* argv);
static void CLI_SUBMENU_Network_Ip(int argc, char* const* argv);
static void CLI_SUBMENU_Network_Netmask(int argc, char* const* argv);
static void CLI_SUBMENU_Network_Gateway(int argc, char* const* argv);
static void CLI_SUBMENU_Network_Port(int argc, char* const* argv);

void CLI_SUBMENU_Network_LineHandlerFunc(int argc, char* const* argv) {
	if (argc > 0) {
		HANDLE_OPTION("show", CLI_SUBMENU_Network_Show);
		HANDLE_OPTION("mac", CLI_SUBMENU_Network_Mac);
		HANDLE_OPTION("ip", CLI_SUBMENU_Network_Ip);
		HANDLE_OPTION("netmask", CLI_SUBMENU_Network_Netmask);
		HANDLE_OPTION("gateway", CLI_SUBMENU_Network_Gateway);
		HANDLE_OPTION("port", CLI_SUBMENU_Network_Port);
	}
	CLI_PrintRespLn("Unknown command");
}

static void CLI_SUBMENU_Network_Show(int argc, char* const* argv) {

	CLI_PrintRespLn("Network configuration:");

	uint8_t addressBuffer[10];
	char addressStrBuffer[30];

	networkGetMac(addressBuffer);
	macToStr(addressBuffer, addressStrBuffer);
	CLI_PRINT_RESP_LN_FORMAT("MAC address: %s", addressStrBuffer);

	networkGetIp(addressBuffer);
	ipToStr(addressBuffer, addressStrBuffer);
	CLI_PRINT_RESP_LN_FORMAT("IP address: %s", addressStrBuffer);

	networkGetNetmask(addressBuffer);
	ipToStr(addressBuffer, addressStrBuffer);
	CLI_PRINT_RESP_LN_FORMAT("Netmask: %s", addressStrBuffer);

	networkGetGateway(addressBuffer);
	ipToStr(addressBuffer, addressStrBuffer);
	CLI_PRINT_RESP_LN_FORMAT("Gateway: %s", addressStrBuffer);

	uint16_t port;
	GPIO_GetNetworkPort(&port);
	CLI_PRINT_RESP_LN_FORMAT("Port: %u", port);

}

static void CLI_SUBMENU_Network_Mac(int argc, char* const* argv) {

	if (argc != 1) {
		CLI_INVALID_ARGC(1);
		return;
	}

	unsigned int macInt[6];
	uint8_t mac[6];
	if (sscanf(argv[0], "%02X:%02X:%02X:%02X:%02X:%02X", &macInt[0], &macInt[1], &macInt[2], &macInt[3], &macInt[4], &macInt[5]) != 6) {
		CLI_PrintRespLn("Invalid MAC address format!");
		return;
	}

	for (int i = 0; i < 6; i++) {
		if (macInt[i] > 255) {
			CLI_PrintRespLn("Invalid MAC address format!");
			return;
		}
		mac[i] = (uint8_t)macInt[i];
	}

	networkSetMac(mac);

	char macBuffer[20];
	macToStr(mac, macBuffer);
	CLI_PRINT_RESP_LN_FORMAT("Successfully set MAC address to %s!", macBuffer);

}

static void CLI_SUBMENU_Network_Ip(int argc, char* const* argv) {

	if (argc != 1) {
		CLI_INVALID_ARGC(1);
		return;
	}

	unsigned int ipInt[4];
	uint8_t ip[4];
	if (sscanf(argv[0], "%u.%u.%u.%u", &ipInt[0], &ipInt[1], &ipInt[2], &ipInt[3]) != 4) {
		CLI_PrintRespLn("Invalid IP address format!");
		return;
	}

	for (int i = 0; i < 4; i++) {
		if (ipInt[i] > 255) {
			CLI_PrintRespLn("Invalid IP address format!");
			return;
		}
		ip[i] = (uint8_t)ipInt[i];
	}

	networkSetIp(ip);

	char ipBuffer[20];
	ipToStr(ip, ipBuffer);
	CLI_PRINT_RESP_LN_FORMAT("Successfully set IP address to %s!", ipBuffer);

}

static void CLI_SUBMENU_Network_Netmask(int argc, char* const* argv) {

	if (argc != 1) {
		CLI_INVALID_ARGC(1);
		return;
	}

	unsigned int netmaskInt[4];
	uint8_t netmask[4];
	if (sscanf(argv[0], "%u.%u.%u.%u", &netmaskInt[0], &netmaskInt[1], &netmaskInt[2], &netmaskInt[3]) != 4) {
		CLI_PrintRespLn("Invalid netmask format!");
		return;
	}

	for (int i = 0; i < 4; i++) {
		if (netmaskInt[i] > 255) {
			CLI_PrintRespLn("Invalid netmask format!");
			return;
		}
		netmask[i] = (uint8_t)netmaskInt[i];
	}

	if (!networkSetNetmask(netmask)) {
		CLI_PrintRespLn("Invalid netmask format!");
		return;
	}

	char netmaskBuffer[20];
	ipToStr(netmask, netmaskBuffer);
	CLI_PRINT_RESP_LN_FORMAT("Successfully set netmask to %s!", netmaskBuffer);

}

static void CLI_SUBMENU_Network_Gateway(int argc, char* const* argv) {

	if (argc != 1) {
		CLI_INVALID_ARGC(1);
		return;
	}

	unsigned int gatewayInt[4];
	uint8_t gateway[4];
	if (sscanf(argv[0], "%u.%u.%u.%u", &gatewayInt[0], &gatewayInt[1], &gatewayInt[2], &gatewayInt[3]) != 4) {
		CLI_PrintRespLn("Invalid gateway IP format!");
		return;
	}

	for (int i = 0; i < 4; i++) {
		if (gatewayInt[i] > 255) {
			CLI_PrintRespLn("Invalid gateway IP format!");
			return;
		}
		gateway[i] = (uint8_t)gatewayInt[i];
	}

	networkSetGateway(gateway);

	char gatewayBuffer[20];
	ipToStr(gateway, gatewayBuffer);
	CLI_PRINT_RESP_LN_FORMAT("Successfully set gateway IP to %s!", gatewayBuffer);

}

static void CLI_SUBMENU_Network_Port(int argc, char* const* argv) {

	if (argc != 1) {
		CLI_INVALID_ARGC(1);
		return;
	}

	unsigned int portInt;
	if ((sscanf(argv[0], "%u", &portInt) != 1) || (portInt > 65535)) {
		CLI_PrintRespLn("Invalid port format or range!");
		return;
	}

	GPIO_SetNetworkPort((uint16_t)portInt);

	CLI_PRINT_RESP_LN_FORMAT("Successfully set port to %u!", portInt);

}
