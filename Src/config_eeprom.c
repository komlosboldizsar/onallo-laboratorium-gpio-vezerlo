#include "config_eeprom.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "peripheral.h"
#include "macro.h"
#include "mem_i2c.h"

#define CONFIG_EEPROM_ADDRESS   0x00
MEM_I2C configEepromHandle;

void ConfigEeprom_Init() {
	MEM_I2C_Init(&configEepromHandle, P_I2C, CONFIG_EEPROM_ADDRESS, MEM_SIZE_64K);
}

void ConfigEeprom_Read8(uint16_t address, uint16_t count, uint8_t* data) {
	MEM_I2C_ReadBytes(&configEepromHandle, address, count, data);
}

void ConfigEeprom_Read16(uint16_t address, uint16_t count, uint16_t* data) {
	MEM_I2C_ReadUint16s(&configEepromHandle, address, count, data);
}

void ConfigEeprom_Read32(uint16_t address, uint16_t count, uint32_t* data) {
	MEM_I2C_ReadUint32s(&configEepromHandle, address, count, data);
}

void ConfigEeprom_Write8(uint16_t address, uint16_t count, uint8_t* data) {
	uint16_t pageAddress = address;
	uint16_t afterEndAddress = address + count;
	while(count > 0) {
		uint16_t nextPageAddress = (pageAddress & ~0x001F) + 0x0020;
		uint16_t bytesToWrite = ((nextPageAddress > afterEndAddress) ? afterEndAddress : nextPageAddress) - pageAddress;
		MEM_I2C_WritePage(&configEepromHandle, pageAddress, bytesToWrite, data);
		pageAddress = nextPageAddress;
		data += bytesToWrite;
		count -= bytesToWrite;
	}
}

void ConfigEeprom_Write16(uint16_t address, uint16_t count, uint16_t* data) {
	ConfigEeprom_Write8(address, (count * 2), (uint8_t*)data);
}

void ConfigEeprom_Write32(uint16_t address, uint16_t count, uint32_t* data) {
	ConfigEeprom_Write8(address, (count * 4), (uint8_t*)data);
}
