#include "enc28j60.h"
#include "stdio.h"

/* Helpers */

#define ETH_MAXPAYLOAD		            1500
#define ETH_MACLEN                      6
#define ETH_TYPELEN                     2
#define ETH_MAXFRMLEN	                ETH_MAXPAYLOAD+(2*ETH_MACLEN)+ETH_TYPELEN+ETH_CRCLEN
#define ETH_CRCLEN                      4

#define ENC28J60_RX_STVLEN              6
#define ENC28J60_REG_MABBIPG_DEFVAL     0x15
#define ENC28J60_REG_MAIPGL_DEFVAL      0x12
#define ENC28J60_REG_MAIPGH_DEFVAL      0x0C
#define ENC28J60_MEMREAD_BUFFERLEN      ETH_MAXFRMLEN+ENC28J60_RX_STVLEN+16

#define SPI_TX2(B1, B2)       { \
							      uint8_t bytes[] = { (B1), (B2) }; \
	                              instance->spiTxFunc(bytes, 2, 0); \
                              }
							  
#define SPI_TX1RX1(BTX)       uint8_t bytesTx[] = { (BTX), (0) }; \
						      uint8_t bytesRx[] = { 0x00, 0x00 }; \
	                          instance->spiTxRxFunc(bytesTx, bytesRx, 2, 0); \
							  
#define SPI_TX1RX2(BTX)       uint8_t bytesTx[] = { (BTX), 0x00, 0x00 }; \
						      uint8_t bytesRx[] = { 0x00, 0x00, 0x00 }; \
	                          instance->spiTxRxFunc(bytesTx, bytesRx, 3, 0); \
							 
#define WCREG(ADDR, DATA)     SPI_TX2((ADDR)|ENC28J60_OPCODE_WCR, DATA);

#define RDREG(ADDR)           SPI_TX1RX1((ADDR)|ENC28J60_OPCODE_RCR); \
							  uint8_t regData = bytesRx[1];
							  
#define RDREG2(ADDR)          SPI_TX1RX2((ADDR)|ENC28J60_OPCODE_RCR); \
							  uint8_t regData = bytesRx[2];
							  
#define BFSREG(ADDR, DATA)    SPI_TX2((ADDR)|ENC28J60_OPCODE_BFS, DATA);

#define BFCREG(ADDR, DATA)    SPI_TX2((ADDR)|ENC28J60_OPCODE_BFC, DATA);
							 
#define UPPER8(B)			  ((uint8_t)(((B) & 0xFF00) >> 8))
#define LOWER8(B)			  ((uint8_t)((B) & 0xFF))
#define CONV16BYTES(H, L)     ((uint16_t)(((H)<<8)|(L)))
#define SWP16(UINT16)               ((uint16_t)(((UINT16)>>8)|((UINT16)<<8)))

#define SWBANKIFNEEDED(B)     if((B) != instance->currentBank) \
						          ENC28J60_SwitchBank(instance, (B));

/* Real functions */

void ENC28J60_InitStruct(ENC28J60* instance, ENC28J60_SpiTxFunc spiTxFunc, ENC28J60_SpiTxRxFunc spiTxRxFunc, ENC28J60_LogFunc logFunc, ENC28J60_ReceiveCallback receiveCallback) {
	instance->spiTxFunc = spiTxFunc;
	instance->spiTxRxFunc = spiTxRxFunc;
	instance->logFunc = logFunc;
	instance->receiveCallback = receiveCallback;
	for (int i = 0; i < 6; i++)
		instance->macAddress[i] = 0x00;
	instance->receiveIT = 0;
	instance->rxNextPacket = 0x0000;
	instance->currentBank = 0;
}

// ETH/MAC/MII register commands

// Switch bank

void ENC28J60_SwitchBank(ENC28J60* instance, uint8_t bank) {
	BFCREG(ENC28J60_REG_ECON1, ENC28J60_BITFIELD_ECON1_BSEL);
	BFSREG(ENC28J60_REG_ECON1, ENC28J60_BITFIELD_ECON1_BSEL & bank);
	instance->currentBank = bank;
}

// Write register and register pair

void ENC28J60_WriteControlRegister(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t data) {
	SWBANKIFNEEDED(bank);
	WCREG(addr, data);
}

void ENC28J60_WriteControlRegister_Direct(ENC28J60* instance, uint8_t addr, uint8_t data) {
	WCREG(addr, data);
}

void ENC28J60_WriteControlRegisterPair(ENC28J60* instance, uint8_t bank, uint8_t addrL, uint16_t data) {
	SWBANKIFNEEDED(bank);
	WCREG(addrL, LOWER8(data));
	WCREG(addrL+1, UPPER8(data));
}

void ENC28J60_WriteControlRegisterPair_Direct(ENC28J60* instance, uint8_t addrL, uint16_t data) {
	WCREG(addrL, LOWER8(data));
	WCREG(addrL+1, UPPER8(data));
}

// Read register and register pair

uint8_t ENC28J60_ReadControlRegister(ENC28J60* instance, uint8_t bank, uint8_t addr) {
	SWBANKIFNEEDED(bank);
	RDREG(addr);
	return regData;
}

uint8_t ENC28J60_ReadControlRegister_Direct(ENC28J60* instance, uint8_t addr) {
	RDREG(addr);
	return regData;
}

uint8_t ENC28J60_ReadControlRegisterPair(ENC28J60* instance, uint8_t bank, uint8_t addrL) {
	SWBANKIFNEEDED(bank);
	uint8_t regDataL, regDataH;
	{ RDREG(addrL); regDataL = regData; }
	{ RDREG(addrL|0x01); regDataH = regData; }
	return CONV16BYTES(regDataH, regDataL);
}

uint8_t ENC28J60_ReadControlRegisterPair_Direct(ENC28J60* instance, uint8_t addrL) {
	uint8_t regDataL, regDataH;
	{ RDREG(addrL); regDataL = regData; }
	{ RDREG(addrL|0x01); regDataH = regData; }
	return CONV16BYTES(regDataH, regDataL);
}

uint8_t ENC28J60_ReadControlRegister2(ENC28J60* instance, uint8_t bank, uint8_t addr) {
	SWBANKIFNEEDED(bank);
	RDREG2(addr);
	return regData;
}

uint8_t ENC28J60_ReadControlRegister2_Direct(ENC28J60* instance, uint8_t addr) {
	RDREG2(addr);
	return regData;
}

// Bit field set and clear

void ENC28J60_BitFieldSet(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t data) {
	SWBANKIFNEEDED(bank);
	BFSREG(addr, data);
}

void ENC28J60_BitFieldSet_Direct(ENC28J60* instance, uint8_t addr, uint8_t data) {
	BFSREG(addr, data);
}

void ENC28J60_BitFieldClear(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t data) {
	SWBANKIFNEEDED(bank);
	BFCREG(addr, data);
}

void ENC28J60_BitFieldClear_Direct(ENC28J60* instance, uint8_t addr, uint8_t data) {
	BFCREG(addr, data);
}

// Bit field set for MAC and MII registers

void ENC28J60_BitFieldSet2(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t bitfield) {
	SWBANKIFNEEDED(bank);
	RDREG2(addr);
	regData |= bitfield;
	WCREG(addr, regData);
}

void ENC28J60_BitFieldSet2_Direct(ENC28J60* instance, uint8_t addr, uint8_t bitfield) {
	RDREG2(addr);
	regData |= bitfield;
	WCREG(addr, regData);
}

void ENC28J60_BitFieldClear2(ENC28J60* instance, uint8_t bank, uint8_t addr, uint8_t bitfield) {
	SWBANKIFNEEDED(bank);
	RDREG2(addr);
	regData &= ~bitfield;
	WCREG(addr, regData);
}

void ENC28J60_BitFieldClear2_Direct(ENC28J60* instance, uint8_t addr, uint8_t bitfield) {
	RDREG2(addr);
	regData &= ~bitfield;
	WCREG(addr, regData);
}

// PHY register read and write

void ENC28J60_WritePhyRegister(ENC28J60* instance, uint8_t addr, uint16_t data) {
	ENC28J60_WriteControlRegister(instance, ENC28J60_BANK_MIREGADR, ENC28J60_REG_MIREGADR, addr);
	ENC28J60_WriteControlRegisterPair(instance, ENC28J60_BANK_MIWRL, ENC28J60_REG_MIWRL, data);
}

uint16_t ENC28J60_ReadPhyRegister(ENC28J60* instance, uint8_t addr) {

	ENC28J60_WriteControlRegister(instance, ENC28J60_BANK_MIREGADR, ENC28J60_REG_MIREGADR, addr);
	ENC28J60_WriteControlRegister(instance, ENC28J60_BANK_MISTAT, ENC28J60_REG_MISTAT, ENC28J60_BIT_MICMD_MIIRD);

	ENC28J60_SwitchBank(instance, ENC28J60_BANK_MISTAT);
	uint8_t mistat = 0;
	for(int i = 0; i < 1024; i++);
	do {
		mistat = ENC28J60_ReadControlRegister_Direct(instance, ENC28J60_REG_MISTAT);
	} while(mistat & ENC28J60_BIT_MISTAT_BUSY);

	ENC28J60_SwitchBank(instance, ENC28J60_BANK_MIREGADR);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MICMD, 0);

	uint8_t lower = ENC28J60_ReadControlRegister_Direct(instance, ENC28J60_REG_MIWRL);
	uint8_t upper = ENC28J60_ReadControlRegister_Direct(instance, ENC28J60_REG_MIWRH);

	return (upper<<8)|lower;

}

// Memory commands

//// TODO
void ENC28J60_ReadBufferMemory(ENC28J60* instance, uint16_t address, uint16_t count, uint8_t* data) {
	
	ENC28J60_SwitchBank(instance, ENC28J60_BANK_0);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ERDPTL, LOWER8(address));
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ERDPTH, UPPER8(address));
	
	uint8_t bytesTx1[1] = { ENC28J60_OPCODE_RBM|ENC28J60_CONST5_RBM },
	        bytesRx1[1],
			bytesTx2[ENC28J60_MEMREAD_BUFFERLEN];
	for(int i = 0; i < ENC28J60_MEMREAD_BUFFERLEN; i++)
		bytesTx2[i] = 0x00;
    instance->spiTxRxFunc(bytesTx1, bytesRx1, 1, 1);
    instance->spiTxRxFunc(bytesTx2, data, count, 0);
	
}

void ENC28J60_WriteBufferMemory(ENC28J60* instance, uint8_t* data, uint16_t count) {
	uint8_t bytesTxOpcode[1] = { ENC28J60_CONST5_WBM | ENC28J60_OPCODE_WBM };
    instance->spiTxFunc(bytesTxOpcode, 1, 1);
	instance->spiTxFunc(data, count, 0);
}

// Transmit and receive

void ENC28J60_Transmit(ENC28J60* instance, uint8_t* dstMac, uint16_t type, uint8_t* payload, uint16_t payloadLength) {
	
	uint8_t ctrlByte = 0x00;
	uint8_t mem[1800];
	
	uint16_t i = 0, k;
	mem[i++] = ctrlByte;
	for(k = 0; k < ETH_MACLEN; k++)
		mem[i++] = dstMac[k];
	for(k = 0; k < ETH_MACLEN; k++)
		mem[i++] = instance->macAddress[k];
	mem[i++] = UPPER8(payloadLength);
	mem[i++] = LOWER8(payloadLength);
	for(k = 0; k < payloadLength; k++)
		mem[i++] = payload[k];
	
	ENC28J60_SwitchBank(instance, ENC28J60_BANK_0);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXSTL, 0x00);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXSTH, 0x00);
	
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_EWRPTL, 0x00);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_EWRPTH, 0x00);
	ENC28J60_WriteBufferMemory(instance, mem, i);
	
	i--;
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXNDL, LOWER8(i));
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXNDH, UPPER8(i));
	
	ENC28J60_BitFieldSet_Direct(instance, ENC28J60_REG_ECON1, ENC28J60_BIT_ECON1_TXRTS);
	
}

void ENC28J60_TransmitRaw(ENC28J60* instance, uint8_t* data, uint16_t payloadLength) {
	
	uint8_t ctrlByte = 0x00;
	uint8_t mem[1800];
	
	uint16_t i = 0, k;
	mem[i++] = ctrlByte;
	for(k = 0; k < payloadLength; k++)
		mem[i++] = data[k];
	
	ENC28J60_SwitchBank(instance, ENC28J60_BANK_0);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXSTL, 0x00);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXSTH, 0x00);
	
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_EWRPTL, 0x00);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_EWRPTH, 0x00);
	ENC28J60_WriteBufferMemory(instance, mem, i);
	
	i--;
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXNDL, LOWER8(i));
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_ETXNDH, UPPER8(i));
	
	ENC28J60_BitFieldSet_Direct(instance, ENC28J60_REG_ECON1, ENC28J60_BIT_ECON1_TXRTS);
	
}



// Setters

void ENC28J60_SetMacAddress(ENC28J60* instance, uint8_t* mac) {
	ENC28J60_SwitchBank(instance, ENC28J60_BANK_3);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAADR0, mac[5]);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAADR1, mac[4]);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAADR2, mac[3]);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAADR3, mac[2]);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAADR4, mac[1]);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAADR5, mac[0]);
	for (int i = 0; i < ETH_MACLEN; i++)
		instance->macAddress[i] =  mac[i];
}

void ENC28J60_InitMac(ENC28J60* instance) {
	ENC28J60_SwitchBank(instance, ENC28J60_BANK_2);
	ENC28J60_BitFieldClear2_Direct(instance, ENC28J60_REG_MACON2, ENC28J60_BIT_MACON2_MARST);
	ENC28J60_BitFieldSet2_Direct(instance, ENC28J60_REG_MACON1, ENC28J60_BIT_MACON1_MARXEN);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MACON3,
		ENC28J60_BIT_MACON3_PADCFG0
		|ENC28J60_BIT_MACON3_TXCRCEN
		|ENC28J60_BIT_MACON3_FRMLNEN
	);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAMXFLL, LOWER8(ETH_MAXFRMLEN));
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAMXFLH, UPPER8(ETH_MAXFRMLEN));
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MABBIPG, ENC28J60_REG_MABBIPG_DEFVAL);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAIPGL, ENC28J60_REG_MAIPGL_DEFVAL);
	ENC28J60_WriteControlRegister_Direct(instance, ENC28J60_REG_MAIPGH, ENC28J60_REG_MAIPGH_DEFVAL);
}

void ENC28J60_InitPhy(ENC28J60* instance) {
	ENC28J60_WritePhyRegister(instance, ENC28J60_PHYREG_PHCON2, 0x0100);
	ENC28J60_WritePhyRegister(instance, ENC28J60_PHYREG_PHCON1, 0x0000);
	ENC28J60_WritePhyRegister(instance, ENC28J60_PHYREG_PHLCON, 0x07DA);
}

void ENC28J60_Init(ENC28J60* instance, uint8_t* mac, uint16_t transmitBufferSize) {
	
	ENC28J60_InitMac(instance);
	ENC28J60_SetMacAddress(instance, mac);
	ENC28J60_InitPhy(instance);
	
	// Buffer sizes
	ENC28J60_WriteControlRegisterPair(instance, ENC28J60_BANK_ERXSTL, ENC28J60_REG_ERXSTL, transmitBufferSize);
	ENC28J60_WriteControlRegisterPair(instance, ENC28J60_BANK_ERXNDL, ENC28J60_REG_ERXNDL, ENC28J60_MEMORY_SIZE);
	ENC28J60_WriteControlRegisterPair(instance, ENC28J60_BANK_ERXRDPTL, ENC28J60_REG_ERXRDPTL, transmitBufferSize);
	
	instance->rxNextPacket = transmitBufferSize;
	
}

void ENC28J60_EnableReceive(ENC28J60* instance) {
	ENC28J60_BitFieldSet_Direct(instance, ENC28J60_REG_EIE, ENC28J60_BIT_EIE_PKTIE|ENC28J60_BIT_EIE_INTIE);
	ENC28J60_BitFieldSet_Direct(instance, ENC28J60_REG_ECON1, ENC28J60_BIT_ECON1_RXEN);
	ENC28J60_WriteControlRegister(instance, ENC28J60_BANK_ERXFCON, ENC28J60_REG_ERXFCON,
		ENC28J60_BIT_ERXFCON_UCEN
		|ENC28J60_BIT_ERXFCON_CRCEN
		|ENC28J60_BIT_ERXFCON_BCEN
	);
}

void ENC28J60_ReceiveIT(ENC28J60* instance) {
	instance->receiveIT = 1;
}

void ENC28J60_Loop(ENC28J60* instance) {
	if (instance->receiveIT) {
		while (ENC28J60_ReadControlRegister(instance, ENC28J60_BANK_EPKTCNT, ENC28J60_REG_EPKTCNT) > 0) {
			uint8_t status[ENC28J60_RX_STVLEN], dataRx[ETH_MAXFRMLEN];
			uint16_t ptr = instance->rxNextPacket;
			ENC28J60_ReadBufferMemory(instance, ptr, ENC28J60_RX_STVLEN, status);
			uint16_t dataRxLength = CONV16BYTES(status[3], status[2]);
			ENC28J60_ReadBufferMemory(instance, ptr+ENC28J60_RX_STVLEN, dataRxLength, dataRx);
			instance->rxNextPacket = CONV16BYTES(status[1], status[0]);
			ENC28J60_WriteControlRegisterPair(instance, ENC28J60_BANK_ERXRDPTL, ENC28J60_REG_ERXRDPTL, instance->rxNextPacket);
			ENC28J60_BitFieldSet_Direct(instance, ENC28J60_REG_ECON2, ENC28J60_BIT_ECON2_PKTDEC);
			instance->receiveCallback(dataRx, dataRxLength-ETH_CRCLEN);
		}
		instance->receiveIT = 0;
	}
}
