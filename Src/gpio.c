#include "gpio.h"
#include "stm32f4xx_hal.h"
#include "main.h"
#include "macro.h"
#include "peripheral.h"
#include "config_eeprom.h"
#include "mem_spi.h"
#include "gpiobus_master.h"
#include "gpiobus_master_eth.h"
#include "gpiobus_master_flash.h"
#include "network.h"

MEM_SPI flashMem;
GPIOBUS gpioBus = { 0 };
GPIOBUS_Master_Eth_Handle gpioBusEth = { 0 };
GPIOBUS_Master_Flash gpioBusFlash = { 0 };

static void GPIO_ConfigChangedCallback(GPIOBUS* handle, GPIOBUS_MASTER_EventType eventType, uint8_t* data);

static void flashSpiFunc(uint8_t value);
static void modularSpiFunc(uint8_t value);
static uint32_t timeFunction();

uint8_t atInitLoadingConfigFromFlash = 0;

void GPIO_Init(GPIO_LOG_FUNCTION logFunc) {
	MEM_SPI_Init(&flashMem, P_SPI_FLASH, &flashSpiFunc);
	GPIOBUS_Master_Init(&gpioBus, P_I2C, P_SPI_MODULAR, &modularSpiFunc);
	GPIOBUS_Master_SetCallback(&gpioBus, GPIOBUS_MASTER_EVENT_ConfigChanged, &GPIO_ConfigChangedCallback);
	GPIOBUS_Master_Flash_Init(&gpioBusFlash, &gpioBus, &flashMem);
	atInitLoadingConfigFromFlash = 1;
	GPIOBUS_Master_Flash_LoadConfig(&gpioBusFlash, 1, 1);
	GPIOBUS_Master_ConfigUpdated(&gpioBus);
	atInitLoadingConfigFromFlash = 0;
	GPIOBUS_Master_Eth_Init(&gpioBusEth, &gpioBus, P_CONFIGEEPROM, P_TCPIPSTACK, logFunc, &timeFunction);
}

void GPIO_Loop() {
	GPIOBUS_Master_Eth_Loop(&gpioBusEth);
}

void GPIO_Update() {
	uint8_t changes[8];
	GPIOBUS_Master_UpdateIO(&gpioBus, changes);
}

void GPIO_SetNetworkPort(uint16_t port) {
	GPIOBUS_Master_Eth_SetPort(&gpioBusEth, port);
}

void GPIO_GetNetworkPort(uint16_t* port) {
	GPIOBUS_Master_Eth_GetPort(&gpioBusEth, port);
}

static void GPIO_ConfigChangedCallback(GPIOBUS* handle, GPIOBUS_MASTER_EventType eventType, uint8_t* data) {
	if (atInitLoadingConfigFromFlash)
		return;
	GPIOBUS_Master_Flash_SaveConfig(&gpioBusFlash, 0);
}

static void flashSpiFunc(uint8_t value) {
	GPIO_WRITE(FLASH_SPI_CS, value);
}

static void modularSpiFunc(uint8_t value) {
	GPIO_WRITE(MODULAR_SPI_START, value);
}

static uint32_t timeFunction() {
	return HAL_GetTick();
}
