#include "gpiobus_master.h"
#include "stm32f4xx_hal.h"
#include "macro.h"
#include "hexdump.h"

#define INDEX_LUT_OUTPUT      0
#define INDEX_INPUT_VALUE     INDEX_LUT_OUTPUT+NUM_LUTS
#define INDEX_INPUT_CHANGE    INDEX_INPUT_VALUE+(NUM_INPUT_BLOCKS*8)
#define INDEX_INPUT_POSEDGE   INDEX_INPUT_CHANGE+(NUM_INPUT_BLOCKS*8)
#define INDEX_INPUT_NEGEDGE   INDEX_INPUT_POSEDGE+(NUM_INPUT_BLOCKS*8)
#define INDEX_FF              INDEX_INPUT_NEGEDGE+(NUM_INPUT_BLOCKS*8)
#define INDEX_ZERO            INDEX_FF+NUM_FFS
#define INDEX_ONE             INDEX_ZERO+1
#define VALUE_ARRAY_SIZE      INDEX_ONE+1

void GPIOBUS_Master_Init(GPIOBUS* handle, I2C_HandleTypeDef* handle_I2C, SPI_HandleTypeDef* handle_SPI, GPIOBUS_MASTER_SpiCsFunc spiCsFunc) {

	handle->handle_I2C = handle_I2C;
	handle->handle_SPI = handle_SPI;
	handle->spiCsFunc = spiCsFunc;
	handle->currentConfigIndex = 0;
	handle->currentConfig = &(handle->config[handle->currentConfigIndex]);

	// Initial config
	for (int i = 0; i < NUM_LUTS; i++) {
		GPIOBUS_LookUpTable* lut = &(handle->currentConfig->lookUpTables[i]);
		lut->inputA = INDEX_ZERO;
		lut->inputB = INDEX_ZERO;
		lut->inputC = INDEX_ZERO;
		lut->inputD = INDEX_ZERO;
		lut->inputE = INDEX_ZERO;
		lut->thruthTable = 0x00000000;
	}

	for (int i = 0; i < NUM_FFS; i++) {
		GPIOBUS_FlipFlop* ff = &(handle->currentConfig->flipFlops[i]);
		ff->input1 = INDEX_ZERO;
		ff->input2 = INDEX_ZERO;
		ff->type = FFTYPE_SR_Set;
	}

	for (int i = 0; i < NUM_OUTPUT_BLOCKS*8; i++) {
		GPIOBUS_Output* op = &(handle->currentConfig->outputs[i]);
		op->driver = INDEX_ZERO;
	}

	// Init temporary values
	for (int i = 0; i < NUM_INPUT_BLOCKS; i++)
		handle->previousInputs[i] = 0x00;
	for (int i = 0; i < NUM_FFS; i++)
		handle->flipFlopValues[i] = 0x00;

	// Init callbacks
	for (int i = 0; i < __GPIOBUS_MASTER_EVENT_TYPECOUNT; i++)
		handle->callbacks[i] = NULL;

	spiCsFunc(1);

	handle->initialized = 1;

}

void GPIOBUS_Master_SetCallback(GPIOBUS* handle, GPIOBUS_MASTER_EventType eventType, GPIOBUS_MASTER_CallbackFunc function) {
	if ((eventType < 0) || (eventType >= __GPIOBUS_MASTER_EVENT_TYPECOUNT))
		return;
	handle->callbacks[eventType] = function;
}

#define CALLBACK(TYPE, PDATA)   if (handle->callbacks[TYPE] != NULL) \
                                    handle->callbacks[TYPE](handle, TYPE, PDATA);

void logFunction(const char* text);

void GPIOBUS_Master_ReadIO(GPIOBUS* handle, uint8_t* buffer) {
	handle->spiCsFunc(0);
	NOPWAIT(200);
	uint8_t pTxData[] = { GPIOBUS_CMD_READ };
	HAL_SPI_Transmit(handle->handle_SPI, pTxData, 1, 1);
	NOPWAIT(1500);
	if(HAL_SPI_Receive(handle->handle_SPI, buffer, NUM_INPUT_BLOCKS+1, NUM_INPUT_BLOCKS+1) != HAL_OK)
		while(1);
	NOPWAIT(200);
	handle->spiCsFunc(1);

}

void GPIOBUS_Master_WriteIO(GPIOBUS* handle, uint8_t* data) {
	handle->spiCsFunc(0);
	NOPWAIT(200);
	uint8_t pTxData[] = { GPIOBUS_CMD_WRITE };
	HAL_SPI_Transmit(handle->handle_SPI, pTxData, 1, 1);
	NOPWAIT(1500);
	HAL_SPI_Transmit(handle->handle_SPI, data, NUM_OUTPUT_BLOCKS+1, NUM_OUTPUT_BLOCKS+1);
	NOPWAIT(200);
	handle->spiCsFunc(1);
	hexdump(data, NUM_OUTPUT_BLOCKS+1, &logFunction);
}

void GPIOBUS_Master_UpdateIO(GPIOBUS* handle, uint8_t* pInputChanges) {

	if (handle->initialized != 1)
		return;

	uint8_t newValues[VALUE_ARRAY_SIZE];
	newValues[INDEX_ZERO] = 0x00;
	newValues[INDEX_ONE] = 0x01;

	uint8_t inputs[NUM_INPUT_BLOCKS+1];
	GPIOBUS_Master_ReadIO(handle, inputs);

	// Inputs
	for (int i = 0; i < NUM_INPUT_BLOCKS; i++) {

		uint8_t inputValues = inputs[i];
		uint8_t inputValuesPrev = handle->previousInputs[i];
		uint8_t inputChanges = inputValues ^ inputValuesPrev;
		uint8_t inputPosedges = inputChanges & inputValues;
		uint8_t inputNegedges = inputChanges & ~inputValues;

		for (int j = 0; j < 8; j++) {
			newValues[INDEX_INPUT_VALUE+i*8+j] = (inputValues >> j) & 0x01;
			newValues[INDEX_INPUT_CHANGE+i*8+j] = (inputChanges >> j) & 0x01;
			newValues[INDEX_INPUT_POSEDGE+i*8+j] = (inputPosedges >> j) & 0x01;
			newValues[INDEX_INPUT_NEGEDGE+i*8+j] = (inputNegedges >> j) & 0x01;
		}

		pInputChanges[i] = inputChanges;

	}

	// LUTs
	for (int i = 0; i < NUM_LUTS; i++) {
		GPIOBUS_LookUpTable* lutConfig = &(handle->currentConfig->lookUpTables[i]);
		uint8_t inA = newValues[lutConfig->inputA] & 0x01;
		uint8_t inB = newValues[lutConfig->inputB] & 0x01;
		uint8_t inC = newValues[lutConfig->inputC] & 0x01;
		uint8_t inD = newValues[lutConfig->inputD] & 0x01;
		uint8_t inE = newValues[lutConfig->inputE] & 0x01;
		uint8_t lutAddr = (inE << 4) | (inD << 3) | (inC << 2) | (inB << 1) | (inA);
		uint8_t lutValue = (lutConfig->thruthTable >> lutAddr) & 0x01;
		newValues[INDEX_LUT_OUTPUT+i] = lutValue;
	}

	// FlipFlops
	for (int i = 0; i < NUM_FFS; i++) {
		GPIOBUS_FlipFlop* ffConfig = &(handle->currentConfig->flipFlops[i]);
		uint8_t currentValue = handle->flipFlopValues[i];
		uint8_t in1 = newValues[ffConfig->input1] & 0x01;
		uint8_t in2 = newValues[ffConfig->input2] & 0x01;
		uint8_t newValue = currentValue;
		switch (ffConfig->type) {
		case FFTYPE_SR_Set:
			if (in1)
				newValue = 1;
			else if (in2)
				newValue = 0;
			break;
		case FFTYPE_SR_Reset:
			if (in2)
				newValue = 0;
			else if (in1)
				newValue = 1;
			break;
		case FFTYPE_JK:
			if (in1 && in2)
				newValue = ~currentValue;
			else if (in1)
				newValue = 1;
			else if (in2)
				newValue = 0;
			break;
		case FFTYPE_D:
			if (in2)
				newValue = in1;
			break;
		}
		newValues[INDEX_FF+i] = newValue;
	}

	// Outputs
	uint8_t outputValues[NUM_OUTPUT_BLOCKS+1];
	outputValues[0] = 0x00;
	for (int i = 1; i < NUM_OUTPUT_BLOCKS+1; i++) {
		outputValues[i] = 0x00;
		for (int j = 0; j < 8; j++) {
			uint16_t driver = handle->currentConfig->outputs[(i-1)*8+j].driver;
			uint8_t outputValue = newValues[driver] & 0x01;
			outputValues[i] |= (outputValue << j);
		}
	}

	GPIOBUS_Master_WriteIO(handle, outputValues);

	// Copy values
	for (int i = 0; i < NUM_INPUT_BLOCKS; i++)
		handle->previousInputs[i] = inputs[i+1];
	for (int i = 0; i < NUM_FFS; i++)
		handle->flipFlopValues[i] = newValues[INDEX_FF+i];

}

#define GPIOBUS_MODULE_PREFIX   0xB0
#define GPIOBUS_CMD_RESET       0x00
#define GPIOBUS_CMD_GETTYPE     0x01
#define GPIOBUS_CMD_RREG        0x02
#define GPIOBUS_CMD_WREG        0x03

#define I2C_TX(ADDR, PTX, CTX)                { \
											      uint8_t addressBase = GPIOBUS_MODULE_PREFIX | ((ADDR & 0x07) << 1); \
                                                  HAL_I2C_Master_Transmit(handle->handle_I2C, (addressBase | 0x00), PTX, CTX, CTX); \
                                              }

#define I2C_TXRX(ADDR, PTX, CTX, PRX, CRX)    { \
											      uint8_t addressBase = GPIOBUS_MODULE_PREFIX | ((ADDR & 0x07) << 1); \
                                                  HAL_I2C_Master_Transmit(handle->handle_I2C, (addressBase | 0x00), PTX, CTX, CTX); \
                                                  HAL_I2C_Master_Receive(handle->handle_I2C, (addressBase | 0x01), PRX, CRX, CRX); \
                                              }


void GPIOBUS_Master_ResetModule(GPIOBUS* handle, uint8_t module) {
	uint8_t tx[] = { GPIOBUS_CMD_RESET };
	I2C_TX(module, tx, sizeof(tx));
}

void GPIOBUS_Master_GetModuleType(GPIOBUS* handle, uint8_t module, uint8_t* type) {
	uint8_t tx[] = { GPIOBUS_CMD_GETTYPE };
	I2C_TXRX(module, tx, sizeof(tx), type, 1);
}

void GPIOBUS_Master_ReadModuleRegister(GPIOBUS* handle, uint8_t module, uint8_t address, uint8_t* data) {
	uint8_t tx[] = { GPIOBUS_CMD_READ, address };
	I2C_TXRX(module, tx, sizeof(tx), data, 1);
}

void GPIOBUS_Master_WriteModuleRegister(GPIOBUS* handle, uint8_t module, uint8_t address, uint8_t data) {
	uint8_t tx[] = { GPIOBUS_CMD_WRITE, address, data };
	I2C_TX(module, tx, sizeof(tx));
}

void GPIOBUS_Master_ConfigUpdated(GPIOBUS* handle) {
	handle->currentConfigIndex = 1 - handle->currentConfigIndex;
	handle->currentConfig = &(handle->config[handle->currentConfigIndex]);
	handle->alternativeConfig = &(handle->config[1 - handle->currentConfigIndex]);
	CALLBACK(GPIOBUS_MASTER_EVENT_ConfigChanged, NULL);
}


