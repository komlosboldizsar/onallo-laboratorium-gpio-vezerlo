#include "gpiobus_master.h"
#include "gpiobus_master_eth.h"
#include "stm32f4xx_hal.h"
#include "tcpip.h"
#include "mem_i2c.h"
#include "macro.h"
#include "checksum.h"

typedef enum _GPIOBUS_Master_Eth_MsgType_ToMaster {
	MSGTYPE_TOMASTER_ResetMaster = 0x00,
	MSGTYPE_TOMASTER_ResetModule = 0x01,
	MSGTYPE_TOMASTER_GetModuleRegister = 0x10,
	MSGTYPE_TOMASTER_SetModuleRegister = 0x11,
	MSGTYPE_TOMASTER_GetInputs = 0x20,
	MSGTYPE_TOMASTER_SetFlipFlop = 0x21,
	MSGTYPE_TOMASTER_RegisterClient = 0x30,
	MSGTYPE_TOMASTER_UnregisterClient = 0x31,
	MSGTYPE_TOMASTER_StartUpdateConfig = 0x40,
	MSGTYPE_TOMASTER_EndUpdateConfig = 0x41,
	MSGTYPE_TOMASTER_UpdateConfigContent = 0x42
} GPIOBUS_Master_Eth_MsgType_ToMaster;

typedef enum _GPIOBUS_Master_Eth_MsgType_FromMaster {
	MSGTYPE_FROMMASTER_Started = 0x80,
	MSGTYPE_FROMMASTER_ModuleRegisterValue = 0x90,
	MSGTYPE_FROMMASTER_InputValues = 0xA0,
	MSGTYPE_FROMMASTER_ClientRegistered = 0xB0,
	MSGTYPE_FROMMASTER_StartUpdateConfig = 0xC0,
	MSGTYPE_FROMMASTER_EndUpdateConfig = 0xC1
} GPIOBUS_Master_Eth_MsgType_FromMaster;

typedef enum _GPIOBUS_Master_Eth_SetFlipFlop_Mode {
	SETFLIPFLOP_MODE_Set = 0,
	SETFLIPFLOP_MODE_Reset = 1,
	SETFLIPFLOP_MODE_Toggle = 2
} GPIOBUS_Master_Eth_SetFlipFlop_Mode;

typedef enum _GPIOBUS_Master_Eth_UpdateConfigContent_UnitType {
	UPDATECONFIGCONTENT_UNITTYPE_LUT = 0,
	UPDATECONFIGCONTENT_UNITTYPE_FF = 1,
	UPDATECONFIGCONTENT_UNITTYPE_Output = 2
} GPIOBUS_Master_Eth_UpdateConfigContent_UnitType;

typedef enum _GPIOBUS_Master_Eth_StartUpdateConfigContent_Status {
	STARTUPDATECONFIGCONTENT_STATUS_Started = 0,
	STARTUPDATECONFIGCONTENT_STATUS_AnotherInProgress = 1
} GPIOBUS_Master_Eth_StartUpdateConfigContent_Status;

typedef enum _GPIOBUS_Master_Eth_EndUpdateConfigContent_Status {
	ENDUPDATECONFIGCONTENT_STATUS_Success = 0,
	ENDUPDATECONFIGCONTENT_STATUS_Timeout = 1,
	ENDUPDATECONFIGCONTENT_STATUS_MessageCountMismatch = 2,
	ENDUPDATECONFIGCONTENT_STATUS_MessageFormatError = 3,
	ENDUPDATECONFIGCONTENT_STATUS_ChecksumMismatch = 4,
	ENDUPDATECONFIGCONTENT_STATUS_UNKNOWN = 0xFF
} GPIOBUS_Master_Eth_UpdateConfigContent_Status;

const uint8_t GPIOBUS_Master_Eth_Preamble[] = { 'G', 'P', 'I', 'O' };
#define PREAMBLE_LENGTH    sizeof(GPIOBUS_Master_Eth_Preamble)
#define OFFSET_SEQNUMBER   PREAMBLE_LENGTH
#define OFFSET_MSGTYPE     OFFSET_SEQNUMBER+2
#define OFFSET_MSGBODY     OFFSET_MSGTYPE+1

#define MEM_I2C_ADDRESS_UDP_PORT                    0x0018
#define MEM_I2C_ADDRESS_START_PREDEF_CLIENTS        0x0100
#define MEM_I2C_OFFSET_PREDEF_CLIENTS               0x0020
#define MEM_I2C_ADDRESS_PREDEF_CLIENT(INDEX)        (MEM_I2C_ADDRESS_START_PREDEF_CLIENTS + INDEX * MEM_I2C_OFFSET_PREDEF_CLIENTS)

static void GPIOBUS_Master_Eth_RX_ResetMaster(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_ResetModule(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_GetModuleRegister(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_SetModuleRegister(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_GetInputs(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_SetFlipFlop(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_RegisterClient(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_UnregisterClient(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_StartUpdateConfig(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_EndUpdateConfig(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);
static void GPIOBUS_Master_Eth_RX_UpdateConfigContent(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort);

static void GPIOBUS_Master_Eth_UdpTransmit(GPIOBUS_Master_Eth_Handle* handle, uint8_t* dstIp, uint16_t dstPort, uint8_t type, uint8_t* body, uint16_t bodyLength);
static void GPIOBUS_Master_Eth_TX_Started(GPIOBUS_Master_Eth_Handle* handle, uint8_t* dstIp, uint16_t dstPort);

void GPIOBUS_Master_Eth_Init(GPIOBUS_Master_Eth_Handle* handle, GPIOBUS* bus, MEM_I2C* configMemHandle, TcpIpStack* tcpIpStack, GPIOBUS_Master_Eth_LogFunc logFunc, GPIOBUS_Master_Eth_TimeFunc timeFunc) {

	handle->bus = bus;
	handle->configMemHandle = configMemHandle;
	handle->tcpIpStack = tcpIpStack;
	handle->logFunc = logFunc;
	handle->timeFunc = timeFunc;

	handle->updateProcess.updating = 0;

	// Read UDP port
	GPIOBUS_Master_Eth_GetPort(handle, &(handle->udpPort));

	// Load predefined clients
	for (int i = 0; i < NUM_PREDEF_CLIENTS; i++)
		//MEM_I2C_ReadBytes(handle->configMemHandle, MEM_I2C_ADDRESS_PREDEF_CLIENT(i), sizeof(GPIOBUS_Master_Eth_PredefClient), (uint8_t*)(&(handle->predefClients[i])));
		handle->predefClients[i].client.valid = 0;

	TcpIp_Udp_RegisterApplication(tcpIpStack, handle->udpPort, &GPIOBUS_Master_Eth_UdpReceive, handle, 0);

	for (int i = 0; i < NUM_PREDEF_CLIENTS; i++) {
		GPIOBUS_Master_Eth_Client* predefClient = &(handle->predefClients[i].client);
		if (predefClient->valid == 1)
			GPIOBUS_Master_Eth_TX_Started(handle, predefClient->ipAddr, predefClient->port);
	}

}

void GPIOBUS_Master_Eth_Loop(GPIOBUS_Master_Eth_Handle* handle) {

	uint32_t time = handle->timeFunc();
	if ((handle->updateProcess.updating) && (time > handle->updateProcess.timeoutLimit)) {
		handle->updateProcess.updating = 0;
		uint8_t msg[] = { UPPER8(handle->updateProcess.threadId), LOWER8(handle->updateProcess.threadId), (uint8_t)ENDUPDATECONFIGCONTENT_STATUS_Timeout };
		GPIOBUS_Master_Eth_UdpTransmit(handle, handle->updateProcess.srcIp, handle->updateProcess.srcPort, MSGTYPE_FROMMASTER_EndUpdateConfig, msg, sizeof(msg));
	}

	for (int i = 0; i < NUM_TEMP_CLIENTS; i++) {
		GPIOBUS_Master_Eth_TempClient* tmpClient = &(handle->tempClients[i]);
		if (tmpClient->client.valid && (tmpClient->validUntil > time))
			tmpClient->client.valid = 0;
	}

}

void GPIOBUS_Master_Eth_UdpReceive(uint8_t* srcIp, uint16_t srcPort, uint8_t* payload, uint16_t payloadLength, void* tagP /* handle */, uint8_t tagD /* 0x00 */) {

	// Check preamble + if contains sequence number and type
	if (payloadLength < PREAMBLE_LENGTH+2+1)
		return;
	for (int i = 0; i < PREAMBLE_LENGTH; i++)
		if (payload[i] != GPIOBUS_Master_Eth_Preamble[i])
			return;

	// Get handle, sequence number, message type and body pointer
	GPIOBUS_Master_Eth_Handle* handle = (GPIOBUS_Master_Eth_Handle*)tagP;
	uint16_t msgSeq = SWP16(*((uint16_t*)(&payload[OFFSET_SEQNUMBER])));
	GPIOBUS_Master_Eth_MsgType_ToMaster msgType = payload[OFFSET_MSGTYPE];
	uint8_t* msgBody = &payload[OFFSET_MSGBODY];
	uint16_t msgBodyLength = payloadLength - OFFSET_MSGBODY;

	switch(msgType) {
	case MSGTYPE_TOMASTER_ResetMaster:
		GPIOBUS_Master_Eth_RX_ResetMaster(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_ResetModule:
		GPIOBUS_Master_Eth_RX_ResetModule(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_GetModuleRegister:
		GPIOBUS_Master_Eth_RX_GetModuleRegister(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_SetModuleRegister:
		GPIOBUS_Master_Eth_RX_SetModuleRegister(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_GetInputs:
		GPIOBUS_Master_Eth_RX_GetInputs(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_SetFlipFlop:
		GPIOBUS_Master_Eth_RX_SetFlipFlop(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_RegisterClient:
		GPIOBUS_Master_Eth_RX_RegisterClient(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_UnregisterClient:
		GPIOBUS_Master_Eth_RX_UnregisterClient(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_StartUpdateConfig:
		GPIOBUS_Master_Eth_RX_StartUpdateConfig(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_EndUpdateConfig:
		GPIOBUS_Master_Eth_RX_EndUpdateConfig(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	case MSGTYPE_TOMASTER_UpdateConfigContent:
		GPIOBUS_Master_Eth_RX_UpdateConfigContent(handle, msgBody, msgBodyLength, msgSeq, srcIp, srcPort);
		break;
	}

}

#define REQSEQ      UPPER8(msgSeq), LOWER8(msgSeq)

static void GPIOBUS_Master_Eth_RX_ResetMaster(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {
	NVIC_SystemReset();
}

static void GPIOBUS_Master_Eth_RX_ResetModule(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {
	uint8_t moduleId = msgBody[0];
	if ((moduleId < 0) || (moduleId >= 8))
		return;
	GPIOBUS_Master_ResetModule(handle->bus, moduleId);
}

static void GPIOBUS_Master_Eth_RX_GetModuleRegister(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	uint8_t moduleId = msgBody[0];
	if ((moduleId < 0) || (moduleId >= 8))
		return;

	uint8_t registerAddress = msgBody[1];
	uint8_t registerData = 0x00;

	GPIOBUS_Master_ReadModuleRegister(handle->bus, moduleId, registerAddress, &registerData);

	uint8_t response[] = { REQSEQ, moduleId, registerAddress, registerData };
	GPIOBUS_Master_Eth_UdpTransmit(handle, srcIp, srcPort, MSGTYPE_FROMMASTER_ModuleRegisterValue, response, sizeof(response));

}

static void GPIOBUS_Master_Eth_RX_SetModuleRegister(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	uint8_t moduleId = msgBody[0];
	if ((moduleId < 0) || (moduleId >= 8))
		return;

	uint8_t registerAddress = msgBody[1];
	uint8_t registerData = msgBody[2];

	GPIOBUS_Master_WriteModuleRegister(handle->bus, moduleId, registerAddress, registerData);

}

static void GPIOBUS_Master_Eth_RX_GetInputs(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {
	uint8_t response[2+NUM_INPUT_BLOCKS];
	response[0] = UPPER8(msgSeq);
	response[1] = LOWER8(msgSeq);
	uint8_t* responseInputValuesPtr = &response[2];
	CPYBYTES(handle->bus->previousInputs, responseInputValuesPtr, NUM_INPUT_BLOCKS);
	GPIOBUS_Master_Eth_UdpTransmit(handle, srcIp, srcPort, MSGTYPE_FROMMASTER_InputValues, response, sizeof(response));
}

static void GPIOBUS_Master_Eth_RX_SetFlipFlop(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	uint16_t ffIndex = SWP16(*((uint16_t*)msgBody));
	if ((ffIndex < 0 )|| (ffIndex >= NUM_FFS))
		return;
	uint8_t* ffValue = &(handle->bus->flipFlopValues[ffIndex]);

	GPIOBUS_Master_Eth_SetFlipFlop_Mode mode = msgBody[2];
	switch (mode) {
	case SETFLIPFLOP_MODE_Set:
		*ffValue = 1;
		break;
	case SETFLIPFLOP_MODE_Reset:
		*ffValue = 0;
		break;
	case SETFLIPFLOP_MODE_Toggle:
		*ffValue = 1 - *ffValue;
		break;
	}

}

static void GPIOBUS_Master_Eth_RX_RegisterClient(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	uint16_t portToRegister = SWP16(*((uint16_t*)msgBody));

	GPIOBUS_Master_Eth_TempClient* foundClientSlot = NULL;
	for (int i = 0; i < NUM_TEMP_CLIENTS; i++) {

		GPIOBUS_Master_Eth_TempClient* clientSlot = &(handle->tempClients[i]);

		uint8_t sameClient = (clientSlot->client.valid == 1) && arrEq(clientSlot->client.ipAddr, srcIp, 4) && (clientSlot->client.port == portToRegister);
		if (sameClient) {
			foundClientSlot = clientSlot;
			break;
		}

		if (clientSlot->client.valid == 0) {
			foundClientSlot = clientSlot;
		}

	}

	if (foundClientSlot != NULL) {
		CPYBYTES(srcIp, foundClientSlot->client.ipAddr, 4);
		foundClientSlot->client.port = portToRegister;
		uint8_t* mask = &msgBody[2];
		CPYBYTES(mask, foundClientSlot->client.mask, NUM_INPUT_BLOCKS);
		foundClientSlot->client.valid = 1;
		foundClientSlot->validUntil = handle->timeFunc() + TEMP_CLIENT_TTL_SEC * 1000;
	}

	uint8_t response[11];
	response[0] = UPPER8(msgSeq);
	response[1] = LOWER8(msgSeq);
	uint8_t* responseIp = &response[2];
	CPYBYTES(srcIp, responseIp, 4);
	response[6] = UPPER8(portToRegister);
	response[7] = LOWER8(portToRegister);
	response[8] = (foundClientSlot != NULL) ? 1 : 0;
	response[9] = UPPER8(TEMP_CLIENT_TTL_SEC);
	response[10] = LOWER8(TEMP_CLIENT_TTL_SEC);

	GPIOBUS_Master_Eth_UdpTransmit(handle, srcIp, srcPort, MSGTYPE_FROMMASTER_ClientRegistered, response, sizeof(response));

}

static void GPIOBUS_Master_Eth_RX_UnregisterClient(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {
	uint16_t portToUnregister = SWP16(*((uint16_t*)msgBody));
	for (int i = 0; i < NUM_TEMP_CLIENTS; i++) {
		GPIOBUS_Master_Eth_TempClient* clientSlot = &(handle->tempClients[i]);
		if (arrEq(clientSlot->client.ipAddr, srcIp, 4) && (clientSlot->client.port == portToUnregister))
			clientSlot->client.valid = 0;
	}
}

static void GPIOBUS_Master_Eth_RX_StartUpdateConfig(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	uint8_t canStart = 1;

	uint16_t updateThreadId = SWP16(*((uint16_t*)msgBody));
	if (handle->updateProcess.updating && (updateThreadId != handle->updateProcess.threadId))
		canStart = 0;

	if (canStart) {
		handle->updateProcess.updating = 1;
		handle->updateProcess.threadId = updateThreadId;
		handle->updateProcess.messageCount = 0;
		handle->updateProcess.messageChecksum = 0x0000;
		handle->updateProcess.timeoutLimit = handle->timeFunc() + CONFIG_UPDATE_TIMEOUT_SEC * 1000;
		handle->updateProcess.formatError = 0;
		CPYBYTES(srcIp, handle->updateProcess.srcIp, 4);
		handle->updateProcess.srcPort = srcPort;
	}

	uint8_t response[] = { REQSEQ, msgBody[0], msgBody[1], (canStart ? STARTUPDATECONFIGCONTENT_STATUS_Started : STARTUPDATECONFIGCONTENT_STATUS_AnotherInProgress) };
	GPIOBUS_Master_Eth_UdpTransmit(handle, srcIp, srcPort, MSGTYPE_FROMMASTER_StartUpdateConfig, response, sizeof(response));

}

static void GPIOBUS_Master_Eth_RX_EndUpdateConfig(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	GPIOBUS_Master_Eth_UpdateConfigContent_Status status = ENDUPDATECONFIGCONTENT_STATUS_UNKNOWN;

	uint16_t updateThreadId = SWP16(*((uint16_t*)msgBody));
	uint16_t messageCount = SWP16(*((uint16_t*)(msgBody+2)));
	uint16_t messageChecksum = SWP16(*((uint16_t*)(msgBody+4)));

	if (handle->updateProcess.threadId != updateThreadId)
		return;

	if (handle->updateProcess.messageCount != messageCount)
		status = ENDUPDATECONFIGCONTENT_STATUS_MessageCountMismatch;
	else if (handle->updateProcess.messageChecksum != messageChecksum)
		status = ENDUPDATECONFIGCONTENT_STATUS_ChecksumMismatch;
	else if (handle->updateProcess.formatError != 0)
		status = ENDUPDATECONFIGCONTENT_STATUS_MessageFormatError;
	else
		status = ENDUPDATECONFIGCONTENT_STATUS_Success;

	uint8_t response[] = { REQSEQ, UPPER8(handle->updateProcess.threadId), LOWER8(handle->updateProcess.threadId), (uint8_t)status };
	GPIOBUS_Master_Eth_UdpTransmit(handle, srcIp, srcPort, MSGTYPE_FROMMASTER_EndUpdateConfig, response, sizeof(response));

	handle->updateProcess.updating = 0;

	GPIOBUS_Master_ConfigUpdated(handle->bus);

}

static void GPIOBUS_Master_Eth_RX_UpdateConfigContent(GPIOBUS_Master_Eth_Handle* handle, uint8_t* msgBody, uint16_t msgBodyLength, uint16_t msgSeq, uint8_t* srcIp, uint16_t srcPort) {

	if ((handle->updateProcess.updating != 1) || (handle->updateProcess.formatError == 1))
		return;

	uint16_t updateThreadId = SWP16(*((uint16_t*)msgBody));
	if (updateThreadId != handle->updateProcess.threadId)
		return;

	uint8_t blockCount = msgBody[2];
	uint8_t* pData = &msgBody[3];

	for (int b = 0; b < blockCount; b++) {

		uint8_t dataLength = pData[0];
		GPIOBUS_Master_Eth_UpdateConfigContent_UnitType unitType = (GPIOBUS_Master_Eth_UpdateConfigContent_UnitType)pData[1];
		uint16_t unitIndex = SWP16(*(uint16_t*)(&pData[2]));
		uint8_t* binData = &pData[4];

		uint8_t* dstPtr = NULL;
		switch (unitType) {
		case UPDATECONFIGCONTENT_UNITTYPE_LUT:
			if ((unitIndex > NUM_LUTS) || (dataLength != sizeof(GPIOBUS_LookUpTable))) {
				handle->updateProcess.formatError = 1;
			} else {
				GPIOBUS_LookUpTable* lutPtr = &(handle->bus->alternativeConfig->lookUpTables[unitIndex]);
				uint8_t* dstPtr = (uint8_t*)lutPtr;
				CPYBYTES(binData, dstPtr, dataLength);
				lutPtr->inputA = SWP16(lutPtr->inputA);
				lutPtr->inputB = SWP16(lutPtr->inputB);
				lutPtr->inputC = SWP16(lutPtr->inputC);
				lutPtr->inputD = SWP16(lutPtr->inputD);
				lutPtr->inputE = SWP16(lutPtr->inputE);
				lutPtr->thruthTable = SWP32(lutPtr->thruthTable);
			}
			break;
		case UPDATECONFIGCONTENT_UNITTYPE_FF:
			if ((unitIndex > NUM_FFS) || (dataLength != sizeof(GPIOBUS_FlipFlop))) {
				handle->updateProcess.formatError = 1;
			} else {
				GPIOBUS_FlipFlop* ffPtr = &(handle->bus->alternativeConfig->flipFlops[unitIndex]);
				dstPtr = (uint8_t*)ffPtr;
				CPYBYTES(binData, dstPtr, dataLength);
				ffPtr->input1 = SWP16(ffPtr->input1);
				ffPtr->input2 = SWP16(ffPtr->input2);
			}
			break;
		case UPDATECONFIGCONTENT_UNITTYPE_Output:
			if ((unitIndex > NUM_OUTPUT_BLOCKS*8) || (dataLength != sizeof(GPIOBUS_Output))) {
				handle->updateProcess.formatError = 1;
			} else {
				GPIOBUS_Output* opPtr = &(handle->bus->alternativeConfig->outputs[unitIndex]);
				dstPtr = (uint8_t*)opPtr;
				CPYBYTES(binData, dstPtr, dataLength);
				opPtr->driver = SWP16(opPtr->driver);
			}
			break;
		default:
			handle->updateProcess.formatError = 1;
			break;
		}

		if (dstPtr != NULL)


		pData += (dataLength + 4);

	}

	handle->updateProcess.messageCount++;

	uint16_t msgChecksum = computeChecksum(msgBody, msgBodyLength);
	uint16_t oldChecksum = handle->updateProcess.messageChecksum;
	uint16_t newChecksum = combineChecksum(oldChecksum, msgChecksum);
	handle->updateProcess.messageChecksum = newChecksum;

}

static void GPIOBUS_Master_Eth_UdpTransmit(GPIOBUS_Master_Eth_Handle* handle, uint8_t* dstIp, uint16_t dstPort, uint8_t type, uint8_t* body, uint16_t bodyLength) {
	uint8_t payload[200];
	CPYBYTES(GPIOBUS_Master_Eth_Preamble, payload, PREAMBLE_LENGTH);
	payload[PREAMBLE_LENGTH] = type;
	uint8_t* bodyStart = &payload[PREAMBLE_LENGTH+1];
	CPYBYTES(body, bodyStart, bodyLength);
	uint16_t payloadLength = PREAMBLE_LENGTH + 1 + bodyLength;
	TcpIp_Udp_SendPacket(handle->tcpIpStack, dstIp, dstPort, handle->udpPort, payload, payloadLength);
}

static void GPIOBUS_Master_Eth_TX_Started(GPIOBUS_Master_Eth_Handle* handle, uint8_t* dstIp, uint16_t dstPort) {
	uint8_t msg[2];
	msg[0] = UPPER8(handle->udpPort);
	msg[1] = LOWER8(handle->udpPort);
	GPIOBUS_Master_Eth_UdpTransmit(handle, dstIp, dstPort, MSGTYPE_FROMMASTER_Started, msg, sizeof(msg));
}

void GPIOBUS_Master_Eth_SetPort(GPIOBUS_Master_Eth_Handle* handle, uint16_t port) {
	MEM_I2C_WriteUint16(handle->configMemHandle, MEM_I2C_ADDRESS_UDP_PORT, port);
}

void GPIOBUS_Master_Eth_GetPort(GPIOBUS_Master_Eth_Handle* handle, uint16_t* port) {
	MEM_I2C_ReadUint16s(handle->configMemHandle, MEM_I2C_ADDRESS_UDP_PORT, 1, port);
}

