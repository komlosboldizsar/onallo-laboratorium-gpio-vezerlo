#include "gpiobus_master_flash.h"
#include "gpiobus_master.h"
#include "stm32f4xx_hal.h"
#include "mem_spi.h"

#define ADDRESS_START_LUTS        0x00000000
#define ADDRESS_START_FFS         0x00010000
#define ADDRESS_START_OUTPUTS     0x00020000

#define ADDRESS_OFFSET_LUTS       0x00000020
#define ADDRESS_OFFSET_FFS        0x00000010
#define ADDRESS_OFFSET_OUTPUTS    0x00000008

#define ADDRESS_LUT(INDEX)        (ADDRESS_START_LUTS + INDEX * ADDRESS_OFFSET_LUTS)
#define ADDRESS_FF(INDEX)         (ADDRESS_START_FFS + INDEX * ADDRESS_OFFSET_FFS)
#define ADDRESS_OUTPUT(INDEX)     (ADDRESS_START_OUTPUTS + INDEX * ADDRESS_OFFSET_OUTPUTS)

void GPIOBUS_Master_Flash_Init(GPIOBUS_Master_Flash* handle, GPIOBUS* bus, MEM_SPI* configMem) {
	handle->bus = bus;
	handle->configMem = configMem;
}

void GPIOBUS_Master_Flash_LoadConfig(GPIOBUS_Master_Flash* handle, uint8_t toAlternative, uint8_t initial) {

	GPIOBUS_Config* config = toAlternative ? handle->bus->alternativeConfig : handle->bus->currentConfig;

	// LUTs
	for (int i = 0; i < NUM_LUTS; i++)
		MEM_SPI_Read(handle->configMem, ADDRESS_LUT(i), sizeof(GPIOBUS_LookUpTable), (uint8_t*)(&(config->lookUpTables[i])));

	// FFs
	for (int i = 0; i < NUM_FFS; i++) {
		MEM_SPI_Read(handle->configMem, ADDRESS_FF(i), sizeof(GPIOBUS_FlipFlop), (uint8_t*)(&(config->flipFlops[i])));
		if (initial)
			handle->bus->flipFlopValues[i] = config->flipFlops[i].defaultValue;
	}

	// Outputs
	for (int i = 0; i < NUM_OUTPUT_BLOCKS*8; i++)
		MEM_SPI_Read(handle->configMem, ADDRESS_OUTPUT(i), sizeof(GPIOBUS_Output), (uint8_t*)(&(config->outputs[i])));

}

void GPIOBUS_Master_Flash_SaveConfig(GPIOBUS_Master_Flash* handle, uint8_t fromAlternative) {

	GPIOBUS_Config* config = fromAlternative ? handle->bus->alternativeConfig : handle->bus->currentConfig;

	// LUTs
	MEM_SPI_Erase64K(handle->configMem, ADDRESS_START_LUTS);
	MEM_SPI_WriteEnable(handle->configMem);
	for (int i = 0; i < NUM_LUTS; i++)
		MEM_SPI_Write(handle->configMem, ADDRESS_LUT(i), sizeof(GPIOBUS_LookUpTable), (uint8_t*)(&(config->lookUpTables[i])));

	// FFs
	MEM_SPI_Erase64K(handle->configMem, ADDRESS_START_FFS);
	MEM_SPI_WriteEnable(handle->configMem);
	for (int i = 0; i < NUM_FFS; i++)
		MEM_SPI_Write(handle->configMem, ADDRESS_FF(i), sizeof(GPIOBUS_FlipFlop), (uint8_t*)(&(config->flipFlops[i])));

	// Outputs
	MEM_SPI_Erase64K(handle->configMem, ADDRESS_START_OUTPUTS);
	MEM_SPI_WriteEnable(handle->configMem);
	for (int i = 0; i < NUM_OUTPUT_BLOCKS*8; i++)
		MEM_SPI_Write(handle->configMem, ADDRESS_OUTPUT(i), sizeof(GPIOBUS_Output), (uint8_t*)(&(config->outputs[i])));

	MEM_SPI_WriteDisable(handle->configMem);

}
