#include "hexdump.h"
#include "stm32f4xx_hal.h"
#include "stdio.h"

void hexdump(uint8_t* data, uint16_t length, void (*printLnFunc)(const char* text)) {
	char bufferArr[256];
	char* bufferStart = bufferArr;
	char* buffer = bufferArr;
	buffer[0] = 0;
	uint16_t i;
	for (i = 0; i < length; i++) {
		sprintf(buffer, "0x%02X ", data[i]);
		buffer += 5;
		if (i%16 == 15)  {
			printLnFunc(bufferStart);
			buffer = bufferStart;
			buffer[0] = 0;
		} else if (i%8 == 7) {
			*buffer = ' ';
			buffer++;
			*buffer = ' ';
			buffer++;
			*buffer = '\0';
		}
	}
	if (length%16 != 0)
		printLnFunc(bufferStart);
}
