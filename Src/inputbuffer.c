#include "inputbuffer.h"

#define CHR_BACKSPACE '\177'

static void InputBuffer_ClearSlot(InputBuffer* handle, uint16_t slotIndex);

void InputBuffer_Init(InputBuffer* handle, char unitSeparator, InputBuffer_UnitReceivedHandler unitReceivedHandler) {
	InputBuffer_Clear(handle);
	handle->unitReceivedHandler = unitReceivedHandler;
	handle->unitSeparator = unitSeparator;
}

void InputBuffer_Clear(InputBuffer* handle) {
	for (int i = 0; i < INPUTBUFFER_SLOT_COUNT; i++)
		InputBuffer_ClearSlot(handle, i);
	handle->slotRdPtr = 0;
	handle->slotWrPtr = 0;
}

void InputBuffer_Received_Chr(InputBuffer* handle, char chr) {

	InputBufferSlot* slotWr = &(handle->slots[handle->slotWrPtr]);
	if (slotWr->ready)
		return;

	if((chr == CHR_BACKSPACE) && (slotWr->received > 0)) {
		slotWr->received--;
		return;
	}

	if(slotWr->received >= INPUTBUFFER_SLOT_SIZE)
		slotWr->overflow = 1;
	else if (chr != handle->unitSeparator)
		slotWr->content[slotWr->received] = chr;

	slotWr->received++;

	if(chr == handle->unitSeparator) {
		slotWr->ready = 1;
		if (((handle->slotWrPtr + 1) % INPUTBUFFER_SLOT_COUNT) != handle->slotRdPtr)
			handle->slotWrPtr = (handle->slotWrPtr + 1) % INPUTBUFFER_SLOT_COUNT;
	}

}

void InputBuffer_Loop(InputBuffer* handle) {
	while(handle->slotRdPtr != handle->slotWrPtr) {
		InputBufferSlot* slotRd = &(handle->slots[handle->slotRdPtr]);
		if (!slotRd->overflow)
			if(handle->unitReceivedHandler)
				handle->unitReceivedHandler(slotRd->content);
		InputBuffer_ClearSlot(handle, handle->slotRdPtr);
		handle->slotRdPtr = (handle->slotRdPtr + 1) % INPUTBUFFER_SLOT_COUNT;
	}
}

static void InputBuffer_ClearSlot(InputBuffer* handle, uint16_t slotIndex) {
	InputBufferSlot* slot = &(handle->slots[slotIndex]);
	slot->content[0] = '\0';
	slot->overflow = 0;
	slot->received = 0;
	slot->ready = 0;
}
