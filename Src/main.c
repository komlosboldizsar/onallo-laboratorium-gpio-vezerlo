#include "main.h"
#include "peripheral.h"
#include "init.h"
#include "macro.h"
#include "network.h"
#include "string.h"
#include "config_eeprom.h"
#include "stdio.h"
#include "mem_spi.h"
#include "hexdump.h"
#include "gpio.h"
#include "uart.h"
#include "cli.h"

static void networkLogFunction(const char* text);
static void gpioLogFunction(const char* text);
static void initCli();
static void loopCli();

int main(void) {

	TotalSystemInit();
	ConfigEeprom_Init();
	networkInit(&networkLogFunction);
	GPIO_Init(&gpioLogFunction);
	HAL_TIM_Base_Start_IT(&htim1);
	initCli();

	//uint8_t values[] = { 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00 };
	while (1)
	{
		/*GPIOBUS_Master_WriteIO(&gpioBus, values);
		values[0]++;
		values[7]--;*/
		networkLoop();
		GPIO_Loop();
		loopCli();
		//GPIO_TOGGLE(LED_HEARTBEAT);
		HAL_Delay(10);
	}

}

void logFunction(const char* text) {
	HAL_UART_Transmit(P_UART_CONSOLE, (uint8_t*)text, strlen(text)+1, 2000);
	char newline[] = { '\r', '\n' };
	HAL_UART_Transmit(P_UART_CONSOLE, (uint8_t*)newline, 2, 50);
}

static void networkLogFunction(const char* text) {
	logFunction(text);
}

static void gpioLogFunction(const char* text) {
	logFunction(text);
}

static void initCli() {
	CLI_Init(&UART_TX);
	UART_RX_Start(&CLI_HandleChar, 1);
}

static void loopCli() {
	CLI_Loop();
}
