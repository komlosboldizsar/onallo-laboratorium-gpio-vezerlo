#include "stm32f4xx_hal.h"
#include "mem_i2c.h"

/*
 * @source http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/ka15414.html
 * @source https://electronics.stackexchange.com/questions/183021/stm32f103c8xx-big-or-small-endian
 */

#define PAGE_SIZE 32

#define ADDR_MSB			((address & 0xFF00) >> 8)
#define ADDR_LSB			(address & 0xFF)
#define ADDR_BYTES			ADDR_MSB, ADDR_LSB

#define ADDR_PREAMBLE	    0xA0

#define CHECK_ADDRESS()		if(ADDR_MSB >= handle->size) return 0;
#define WAIT_A_BIT()		HAL_Delay(5);

int MEM_I2C_Init(MEM_I2C* handle, I2C_HandleTypeDef* portHandle, unsigned int address, MEM_I2C_SIZE size) {

	if(address < 0 || address >= 8)
		return 0;

	if(size != MEM_SIZE_32K && size != MEM_SIZE_64K)
		return 0;

	handle->portHandle = portHandle;
	handle->i2c_address = (address<<1) | ADDR_PREAMBLE;
	handle->size = size;

	return 1;

}

int MEM_I2C_ReadBytes(MEM_I2C* handle, uint16_t address, unsigned int count, unsigned char* buffer) {
	CHECK_ADDRESS();
	WAIT_A_BIT();
	unsigned char wordAddress[] = { ADDR_BYTES };
	if(HAL_I2C_Master_Transmit(handle->portHandle, (handle->i2c_address | 0x00), wordAddress, sizeof(wordAddress), 5) != HAL_OK)
		return 0;
	if(HAL_I2C_Master_Receive(handle->portHandle, (handle->i2c_address | 0x01), buffer, count, count) != HAL_OK)
		return 0;
	return 1;
}

int MEM_I2C_ReadUint16s(MEM_I2C* handle, uint16_t address, unsigned int count, uint16_t* buffer) {
	return MEM_I2C_ReadBytes(handle, address, (count*2), (unsigned char*)buffer);
}

int MEM_I2C_ReadUint32s(MEM_I2C* handle, uint16_t address, unsigned int count, uint32_t* buffer) {
	return MEM_I2C_ReadBytes(handle, address, (count*4), (unsigned char*)buffer);
}

int MEM_I2C_WriteByte(MEM_I2C* handle, uint16_t address, unsigned char data) {
	CHECK_ADDRESS();
	WAIT_A_BIT();
	unsigned char i2c_data[] = { ADDR_BYTES, data };
	if(HAL_I2C_Master_Transmit(handle->portHandle, (handle->i2c_address | 0x00), i2c_data, sizeof(i2c_data), 5) != HAL_OK)
		return 0;
	return 1;
}

int MEM_I2C_WritePage(MEM_I2C* handle, uint16_t address, unsigned int count, unsigned char* data) {

	CHECK_ADDRESS();
	WAIT_A_BIT();
	if((ADDR_LSB & 0x1F) + count > PAGE_SIZE)
		return 0;

	unsigned char i2c_data[PAGE_SIZE+2];
	i2c_data[0] = ADDR_MSB;
	i2c_data[1] = ADDR_LSB;
	int i;
	for(i = 0; i < count; i++)
		i2c_data[i+2] = data[i];

	if(HAL_I2C_Master_Transmit(handle->portHandle, (handle->i2c_address | 0x00), i2c_data, count+2, 500) != HAL_OK)
		return 0;

	return 1;

}

int MEM_I2C_WriteUint16(MEM_I2C* handle, uint16_t address, uint16_t data) {
	return MEM_I2C_WritePage(handle, address, 2, (unsigned char*)&data);
}

int MEM_I2C_WriteUint32(MEM_I2C* handle, uint16_t address, uint32_t data) {
	return MEM_I2C_WritePage(handle, address, 4, (unsigned char*)&data);
}
