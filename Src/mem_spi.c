#include "mem_spi.h"
#include "stm32f4xx_hal.h"
#include "macro.h"

#define AT25SF041_OPCODE_READARRAY_1       0x0B
#define AT25SF041_OPCODE_READARRAY_2       0x03
#define AT25SF041_OPCODE_ERASE_4K          0x20
#define AT25SF041_OPCODE_ERASE_32K         0x52
#define AT25SF041_OPCODE_ERASE_64K         0xD8
#define AT25SF041_OPCODE_ERASE_FULL_1      0x60
#define AT25SF041_OPCODE_ERASE_FULL_2      0xC7
#define AT25SF041_OPCODE_PROGRAM           0x02
#define AT25SF041_OPCODE_WRITE_ENABLE      0x06
#define AT25SF041_OPCODE_WRITE_DISABLE     0x04
#define AT25SF041_OPCODE_READ_STATUS_1     0x05
#define AT25SF041_OPCODE_READ_STATUS_2     0x35

#define ADDRESS_BYTES(ADDR)   (uint8_t)((ADDR >> 16) & 0xFF), \
			                  (uint8_t)((ADDR >> 8) & 0xFF), \
			                  (uint8_t)((ADDR) & 0xFF)

#define MEM_SPI_START()                         handle->csFunc(0)
#define MEM_SPI_END()                           handle->csFunc(1)

#define MEM_SPI_TX(COUNT, DATA)                 HAL_SPI_Transmit(handle->portHandle, DATA, COUNT, COUNT)
#define MEM_SPI_RX(COUNT, DATA)                 HAL_SPI_Receive(handle->portHandle, DATA, COUNT, COUNT)
#define MEM_SPI_TXRX(COUNT, DATATX, DATARX)     HAL_SPI_TransmitReceive(handle->portHandle, DATATX, DATARX, COUNT, COUNT)

#define MEM_SPI_WAITBUSY()                      MEM_SPI_WaitBusy(handle)

#define MEM_SPI_SINGLEOP(OPCODE)                { \
												    uint8_t singleOpTx = OPCODE; \
	                                                MEM_SPI_START(); \
	                                                MEM_SPI_TX(1, &singleOpTx); \
                                                    MEM_SPI_END(); \
                                                }

#define MEM_SPI_SINGLEOP_ADDR(OPCODE, ADDR)     { \
												    uint8_t singleOpAddrTx[] = { OPCODE, ADDRESS_BYTES(ADDR) }; \
	                                                MEM_SPI_START(); \
	                                                MEM_SPI_TX(4, singleOpAddrTx); \
                                                    MEM_SPI_END(); \
                                                }

static void MEM_SPI_WaitBusy(MEM_SPI* handle);

void MEM_SPI_Init(MEM_SPI* handle, SPI_HandleTypeDef* portHandle, MEM_SPI_CS_FUNC csFunc) {
	handle->portHandle = portHandle;
	handle->csFunc = csFunc;
	csFunc(1);
}

void MEM_SPI_Read(MEM_SPI* handle, uint32_t address, uint32_t count, uint8_t* buffer) {
	MEM_SPI_START();
	uint8_t commandBytes[] = {
			AT25SF041_OPCODE_READARRAY_2,
			ADDRESS_BYTES(address)
	};
	MEM_SPI_TX(4, commandBytes);
	MEM_SPI_RX(count, buffer);
	MEM_SPI_END();
	MEM_SPI_WAITBUSY();
}


void MEM_SPI_Write(MEM_SPI* handle, uint32_t address, uint32_t count, uint8_t* data) {
	MEM_SPI_WriteEnable(handle);
	MEM_SPI_START();
	uint8_t commandBytes[] = {
			AT25SF041_OPCODE_PROGRAM,
			ADDRESS_BYTES(address)
	};
	MEM_SPI_TX(4, commandBytes);
	MEM_SPI_TX(count, data);
	MEM_SPI_END();
	MEM_SPI_WAITBUSY();
}


void MEM_SPI_WritePages(MEM_SPI* handle, uint32_t address, uint32_t count, uint8_t* data) {
	uint32_t pageAddress = address;
	uint32_t afterEndAddress = address + count;
	while(count > 0) {
		uint32_t nextPageAddress = (pageAddress & ~0x000000FF) + 0x00000100;
		uint32_t bytesToWrite = ((nextPageAddress > afterEndAddress) ? afterEndAddress : nextPageAddress) - pageAddress;
		MEM_SPI_Write(handle, pageAddress, bytesToWrite, data);
		pageAddress = nextPageAddress;
		data += bytesToWrite;
		count -= bytesToWrite;
	}
}

void MEM_SPI_Erase4K(MEM_SPI* handle, uint32_t address) {
	MEM_SPI_WriteEnable(handle);
	MEM_SPI_SINGLEOP_ADDR(AT25SF041_OPCODE_ERASE_4K, address);
	MEM_SPI_WAITBUSY();
}

void MEM_SPI_Erase32K(MEM_SPI* handle, uint32_t address) {
	MEM_SPI_WriteEnable(handle);
	MEM_SPI_SINGLEOP_ADDR(AT25SF041_OPCODE_ERASE_32K, address);
	MEM_SPI_WAITBUSY();
}

void MEM_SPI_Erase64K(MEM_SPI* handle, uint32_t address) {
	MEM_SPI_WriteEnable(handle);
	MEM_SPI_SINGLEOP_ADDR(AT25SF041_OPCODE_ERASE_64K, address);
	MEM_SPI_WAITBUSY();
}

void MEM_SPI_EraseFull(MEM_SPI* handle) {
	MEM_SPI_WriteEnable(handle);
	MEM_SPI_SINGLEOP(AT25SF041_OPCODE_ERASE_FULL_1);
	MEM_SPI_WAITBUSY();
}

void MEM_SPI_WriteEnable(MEM_SPI* handle) {
	MEM_SPI_SINGLEOP(AT25SF041_OPCODE_WRITE_ENABLE);
	MEM_SPI_WAITBUSY();
}

void MEM_SPI_WriteDisable(MEM_SPI* handle) {
	MEM_SPI_SINGLEOP(AT25SF041_OPCODE_WRITE_DISABLE);
	MEM_SPI_WAITBUSY();
	MEM_SPI_WaitBusy(handle);
}

static void MEM_SPI_WaitBusy(MEM_SPI* handle) {
	uint8_t statusTx = AT25SF041_OPCODE_READ_STATUS_1;
	uint8_t statusRx = 0x00;
	MEM_SPI_START();
	MEM_SPI_TX(1, &statusTx);
	do {
		MEM_SPI_RX(1, &statusRx);
	} while(statusRx & 0x01);
	MEM_SPI_END();
}

