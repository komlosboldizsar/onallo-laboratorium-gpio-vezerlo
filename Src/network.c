#include "network.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "enc28j60.h"
#include "tcpip.h"
#include "tcpip_helper.h"
#include "peripheral.h"
#include "macro.h"
#include "stdio.h"

ENC28J60 enc28j60;
TcpIpStack tcpIpStack;

static void ethRxCallback(uint8_t* payload, uint16_t payloadLength);
static void ethTxFunc(uint8_t* pFrameData, uint16_t length);
static void ethSpiTxFunc(uint8_t* pDataTx, uint32_t count, uint8_t continued);
static void ethSpiTxRxFunc(uint8_t* pDataTx, uint8_t* pDataRx, uint32_t count, uint8_t continued);
static uint32_t timeFunction();

void networkInit(NETWORK_LOG_FUNCTION logFunction) {

	uint8_t mac[6];
	uint8_t ipaddr[4];
	uint8_t netmask[4];
	uint8_t gateway[4];
	networkGetMac(mac);
	networkGetIp(ipaddr);
	networkGetNetmask(netmask);
	networkGetGateway(gateway);

	GPIO_ON(ETH_SPI_CS);
	GPIO_OFF(ETH_RST);
	HAL_Delay(10);
	GPIO_ON(ETH_RST);
	HAL_Delay(2000);
	ENC28J60_InitStruct(&enc28j60, &ethSpiTxFunc, &ethSpiTxRxFunc, logFunction, &ethRxCallback);
	ENC28J60_Init(&enc28j60, mac, 1024);
	ENC28J60_EnableReceive(&enc28j60);
	TcpIp_InitStruct(&tcpIpStack, &ethTxFunc, logFunction, &timeFunction);
	TcpIp_Init(&tcpIpStack, ipaddr, netmask, gateway, mac);

	logFunction("Initialized network.");
	char buffer[100], addrbuffer[30];
	macToStr(mac, addrbuffer);
	sprintf(buffer, "MAC: %s", addrbuffer);
	logFunction(buffer);
	ipToStr(ipaddr, addrbuffer);
	sprintf(buffer, "IP: %s", addrbuffer);
	logFunction(buffer);
	ipToStr(netmask, addrbuffer);
	sprintf(buffer, "Netmask: %s", addrbuffer);
	logFunction(buffer);
	ipToStr(gateway, addrbuffer);
	sprintf(buffer, "Gateway: %s", addrbuffer);
	logFunction(buffer);

}

void networkLoop() {
	ENC28J60_Loop(&enc28j60);
	TcpIp_Arp_Process(&tcpIpStack);
	TcpIp_Ipv4_SendProcess(&tcpIpStack);
}

void networkReceiveIT() {
	ENC28J60_ReceiveIT(&enc28j60);
}

void convertNetmaskLengthToNetmask(uint8_t length, uint8_t* mask) {
	for (int i = 0; i < 4; i++) {
		uint8_t ones = ((length > 8) ? 8 : length);
		mask[i] = ((0xFF << (8 - ones)) & 0xFF);
		length -= ones;
	}
}

int convertNetmaskToNetmaskLength(uint8_t* length, uint8_t* mask) {
	*length = 0;
	uint8_t zeros = 0;
	for (int i = 0; i < 4; i++) {
		for (int j = 7; j >= 0; j--) {
			if ((mask[i] >> j) & 0x01) {
				if (zeros) {
					return 0;
				} else {
					(*length)++;
				}
			} else {
				zeros = 1;
			}
		}
	}
	return 1;
}

static void ethRxCallback(uint8_t* payload, uint16_t payloadLength) {
	TcpIp_ReceiveEthFrame(&tcpIpStack, payload, payloadLength);
}

static void ethTxFunc(uint8_t* pFrameData, uint16_t length) {
	ENC28J60_TransmitRaw(&enc28j60, pFrameData, length);
}

static void ethSpiTxFunc(uint8_t* pDataTx, uint32_t count, uint8_t continued) {
	GPIO_OFF(ETH_SPI_CS);
	HAL_SPI_Transmit(P_SPI_ETH, pDataTx, count, 5000);
	NOPWAIT(1000);
	if (!continued)
		GPIO_ON(ETH_SPI_CS);
}

static void ethSpiTxRxFunc(uint8_t* pDataTx, uint8_t* pDataRx, uint32_t count, uint8_t continued) {
	GPIO_OFF(ETH_SPI_CS);
	HAL_SPI_TransmitReceive(P_SPI_ETH, pDataTx, pDataRx, count, 5000);
	NOPWAIT(1000);
	if (!continued)
		GPIO_ON(ETH_SPI_CS);
}

static uint32_t timeFunction() {
	return HAL_GetTick();
}

void networkGetMac(uint8_t* mac) {
	ConfigEeprom_Read8(CES_MAC, 6, mac);
}

void networkGetIp(uint8_t* ip) {
	ConfigEeprom_Read8(CES_IP, 4, ip);
}

void networkGetNetmask(uint8_t* netmask) {
	uint8_t masklen = 0;
	ConfigEeprom_Read8(CES_MASKLEN, 1, &masklen);
	convertNetmaskLengthToNetmask(masklen, netmask);
}

void networkGetNetmaskLength(uint8_t* netmaskLength) {
	ConfigEeprom_Read8(CES_MASKLEN, 1, netmaskLength);
}

void networkGetGateway(uint8_t* gateway) {
	ConfigEeprom_Read8(CES_GW, 4, gateway);
}

void networkSetMac(uint8_t* mac) {
	ConfigEeprom_Write8(CES_MAC, 6, mac);
}

void networkSetIp(uint8_t* ip) {
	ConfigEeprom_Write8(CES_IP, 4, ip);
}

int networkSetNetmask(uint8_t* netmask) {
	uint8_t masklen;
	if (convertNetmaskToNetmaskLength(&masklen, netmask) != 1)
		return 0;
	ConfigEeprom_Write8(CES_MASKLEN, 1, &masklen);
	return 1;
}

void networkSetNetmaskLength(uint8_t* netmaskLength) {
	ConfigEeprom_Write8(CES_MASKLEN, 1, netmaskLength);
}
void networkSetGateway(uint8_t* gateway) {
	ConfigEeprom_Write8(CES_GW, 4, gateway);
}
