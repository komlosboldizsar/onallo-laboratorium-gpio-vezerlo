#include "main.h"
#include "stm32f4xx_it.h"
#include "peripheral.h"
#include "network.h"
#include "macro.h"
#include "gpio.h"

void NMI_Handler(void)
{ }

void HardFault_Handler(void) {
	while (1)
	{ }
}

void MemManage_Handler(void) {
	while (1)
	{ }
}

void BusFault_Handler(void) {
	while (1)
	{ }
}

void UsageFault_Handler(void) {
	while (1)
	{ }
}

void SVC_Handler(void)
{ }

void DebugMon_Handler(void)
{ }

void PendSV_Handler(void)
{ }

void SysTick_Handler(void) {
	HAL_IncTick();
}

void TIM1_UP_TIM10_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&htim1);
	GPIO_Update();
	GPIO_TOGGLE(LED_HEARTBEAT);
}

void EXTI15_10_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
	networkReceiveIT();
}

void USART2_IRQHandler(void)
{
	HAL_UART_IRQHandler(&huart2);
}
