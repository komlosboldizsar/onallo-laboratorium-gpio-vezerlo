#include "stringext.h"

int strlimcat(char* destination, const char* source, size_t maxlength) {
    int len = 0;
    for (; *destination != '\0'; destination++, len++);
    for (; *source != '\0'; destination++, source++, len++) {
        *destination = *source;
        if (len >= maxlength) {
            *destination = '\0';
            return 0;
        }
    }
    *destination = '\0';
    return 1;
}

int strlimcpy(char* destination, const char* source, size_t maxlength) {
    int len;
    for (len = 0; *source != '\0'; destination++, source++, len++) {
        *destination = *source;
        if (len >= maxlength) {
            *destination = '\0';
            return 0;
        }
    }
    *destination = '\0';
    return 1;
}

int strnlimcpy(char* destination, const char* source, size_t num, size_t maxlength) {
    int len;
    for (len = 0; ((*source != '\0') && (len < num)); destination++, source++, len++) {
        *destination = *source;
        if (len >= maxlength) {
            *destination = '\0';
            return 0;
        }
    }
    *destination = '\0';
    return 1;
}
