#include "stm32f4xx_hal.h"
#include "tcpip.h"
#include "tcpip_eth.h"
#include "tcpip_arp.h"
#include "tcpip_ipv4.h"
#include "tcpip_icmp.h"
#include "tcpip_udp.h"
#include "tcpip_helper.h"
#include <stdio.h>

#define _DEBUG_PRINT_IPV4_DETAILS
#define DEBUG_PRINT_ICMP_DETAILS

void TcpIp_InitStruct(TcpIpStack* instance, TransmitFunc transmitFunc, TcpIpLogFunc logFunc, TimeFunc timeFunc) {
	instance->transmitFunc = transmitFunc;
	instance->logFunc = logFunc;
	instance->timeFunc = timeFunc;
	for (int i = 0; i < TCPIP_ARP_CACHE_SIZE; i++)
		instance->arpCache[i].state = ACES_INVALID;
	for (int i = 0; i < TCPIP_TX_BUFFER_SIZE; i++)
		instance->ipv4TxBuffer[i].state = IPV4TXBES_EMPTY;
	for (int i = 0; i < _TCPIP_DBG_OPT_COUNT; i++)
		instance->debugOptions[i] = 0;
}

void TcpIp_Init(TcpIpStack* instance, uint8_t* ipAddress, uint8_t* subnetMask, uint8_t* defaultGateway, uint8_t* macAddress) {
	CPYBYTES(ipAddress, instance->ipAddress, 4);
	CPYBYTES(subnetMask, instance->subnetMask, 4);
	CPYBYTES(defaultGateway, instance->defaultGateway, 4);
	for (int i = 0; i < TCPIP_ETHFRM_MACADDR_LEN; i++)
		instance->macAddress[i] = macAddress[i];
}
