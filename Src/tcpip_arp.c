#include "stm32f4xx_hal.h"
#include "tcpip_arp.h"
#include "tcpip_eth.h"
#include "tcpip.h"
#include <stdio.h>

const static char* TCPIP_DEBUG_TAG_ARP = "TCPIP/ARP";

static void TcpIp_Arp_Debug_Header(TcpIpStack* instance, ArpPacket* packet);
static void TcpIp_Arp_AddPendingIpToCache(TcpIpStack* instance, uint8_t* ip);
static void TcpIp_Arp_RemoveEntry(TcpIpStack* instance, ArpCacheEntry* entry);

const static uint8_t EMPTY_MAC[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

void TcpIp_Arp_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length) {

	ArpPacket arpPacket;
	for (int i = 0; i < 28; i++)
		((uint8_t*)(&arpPacket))[i] = data[i];

	/* START DEBUG - ArpReceive */

	DEBUG_START(ArpReceive, TCPIP_DEBUG_TAG_ARP);

	DEBUG_BLOCK(ArpReceive, 1) {
		DEBUG_FORMAT_PRINT("[%s] Received ARP packet.", TCPIP_DEBUG_TAG_ARP);
	}

	DEBUG_BLOCK(ArpReceive, 2) {
		TcpIp_Arp_Debug_Header(instance, &arpPacket);
	}

	DEBUG_END(ArpReceive, TCPIP_DEBUG_TAG_ARP);

	/* END DEBUG - ArpReceive */

	if (SWP16(arpPacket.hType) == 1) {

		if (SWP16(arpPacket.pType) == 0x0800) {

			uint16_t oper = SWP16(arpPacket.oper);

			if (oper == 1) {
				if (EQ32(arpPacket.tpa, instance->ipAddress))
					TcpIp_Arp_SendReply(instance, arpPacket.sha, arpPacket.spa);
			} else if (oper == 2) {
				TcpIp_Arp_AddIpToCache(instance, arpPacket.sha, arpPacket.spa);
			}

		}

	}

}

void TcpIp_Arp_SendReply(TcpIpStack* instance, uint8_t* dstMac, uint8_t* dstIp) {
	ArpPacket arpPacket;
	arpPacket.hType = SWP16(1);
	arpPacket.pType = SWP16(0x0800);
	arpPacket.hLen = 6;
	arpPacket.pLen = 4;
	arpPacket.oper = SWP16(2);
	CPYBYTES(instance->macAddress, arpPacket.sha, 6);
	CPYBYTES(instance->ipAddress, arpPacket.spa, 4);
	CPYBYTES(dstMac, arpPacket.tha, 6);
	CPYBYTES(dstIp, arpPacket.tpa, 4);

	/* START DEBUG - ArpTransmit */

	DEBUG_START(ArpTransmit, TCPIP_DEBUG_TAG_ARP);

	DEBUG_BLOCK(ArpTransmit, 1) {
		DEBUG_FORMAT_PRINT("[%s] Transmitting ARP packet.", TCPIP_DEBUG_TAG_ARP);
	}

	DEBUG_BLOCK(ArpTransmit, 2) {
		TcpIp_Arp_Debug_Header(instance, &arpPacket);
	}

	DEBUG_END(ArpTransmit, TCPIP_DEBUG_TAG_ARP);

	/* END DEBUG - ArpTransmit */

	TcpIp_TransmitEthFrameDetailedOwnMac(instance, dstMac, 0x0806, (uint8_t*)&arpPacket, sizeof(ArpPacket));
}

void TcpIp_Arp_SendRequest(TcpIpStack* instance, uint8_t* dstIp) {

	uint8_t broadcastMac[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	ArpPacket arpPacket;
	arpPacket.hType = SWP16(1);
	arpPacket.pType = SWP16(0x0800);
	arpPacket.hLen = 6;
	arpPacket.pLen = 4;
	arpPacket.oper = SWP16(1);
	CPYBYTES(instance->macAddress, arpPacket.sha, 6);
	CPYBYTES(instance->ipAddress, arpPacket.spa, 4);
	CPYBYTES(EMPTY_MAC, arpPacket.tha, 6);
	CPYBYTES(dstIp, arpPacket.tpa, 4);

	/* START DEBUG - ArpTransmit */

	DEBUG_START(ArpTransmit, TCPIP_DEBUG_TAG_ARP);

	DEBUG_BLOCK(ArpTransmit, 1) {
		DEBUG_FORMAT_PRINT("[%s] Transmitting ARP packet.", TCPIP_DEBUG_TAG_ARP);
	}

	DEBUG_BLOCK(ArpTransmit, 2) {
		TcpIp_Arp_Debug_Header(instance, &arpPacket);
	}

	DEBUG_END(ArpTransmit, TCPIP_DEBUG_TAG_ARP);

	/* END DEBUG - ArpTransmit */

	TcpIp_TransmitEthFrameDetailedOwnMac(instance, broadcastMac, 0x0806, (uint8_t*)&arpPacket, sizeof(ArpPacket));
	TcpIp_Arp_AddPendingIpToCache(instance, dstIp);

}

ArpCacheEntry* TcpIp_Arp_GetEntryForIp(TcpIpStack* instance, uint8_t* ipAddress) {
	for (int i = 0; i < TCPIP_ARP_CACHE_SIZE; i++) {
		ArpCacheEntry* entry = &(instance->arpCache[i]);
		if ((entry->state == ACES_VALID) && arrEq(entry->ip, ipAddress, 4))
			return entry;
	}
	return NULL;
}

ArpCacheEntry* TcpIp_Arp_GetNearestEntryForIp(TcpIpStack* instance, uint8_t* ipAddress) {

	ArpCacheEntry* firstFree = NULL;
	ArpCacheEntry* oldest = NULL;
	ArpCacheEntry* sameIp = NULL;
	int oldestAge = -1;
	for (int i = 0; i < TCPIP_ARP_CACHE_SIZE; i++) {
		ArpCacheEntry* entry = &(instance->arpCache[i]);
		if ((firstFree == NULL) && (entry->state == ACES_INVALID)) {
			firstFree = entry;
		}
		if ((entry->state == ACES_VALID) && (entry->age > oldestAge)) {
			oldest = entry;
			oldestAge = entry->age;
		}
		if (((entry->state == ACES_VALID) || (entry->state == ACES_PENDING)) && arrEq(entry->ip, ipAddress, 4)) {
			sameIp = entry;
		}
	}

	if (sameIp != NULL)
		return sameIp;
	else if (firstFree != NULL)
		return firstFree;
	else if (oldest != NULL)
		return oldest;
	return NULL;

}

void TcpIp_Arp_AddIpToCache(TcpIpStack* instance, uint8_t* mac, uint8_t* ip) {

	ArpCacheEntry* usedEntry = TcpIp_Arp_GetNearestEntryForIp(instance, ip);
	if (usedEntry != NULL) {
		usedEntry->state = ACES_VALID;
		usedEntry->age = 0;
		CPYBYTES(ip, usedEntry->ip, 4);
		CPYBYTES(mac, usedEntry->mac, 6);
	}

	/* START DEBUG - ArpCacheEntryAdd */

	DEBUG_START(ArpCacheEntryAdd, TCPIP_DEBUG_TAG_ARP);

	DEBUG_BLOCK(ArpCacheEntryAdd, 1) {
		DEBUG_FORMAT_PRINT("[%s] Add ARP cache entry.", TCPIP_DEBUG_TAG_ARP);
	}

	DEBUG_BLOCK(ArpCacheEntryAdd, 2) {
		char addressBuffer[20];
		macToStr(mac, addressBuffer);
		DEBUG_FORMAT_PRINT("[%s] MAC: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
		ipToStr(ip, addressBuffer);
		DEBUG_FORMAT_PRINT("[%s] IP: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
	}

	DEBUG_BLOCK(ArpCacheEntryAdd, 3) {
		TcpIp_Arp_PrintTable(instance);
	}

	DEBUG_END(ArpCacheEntryAdd, TCPIP_DEBUG_TAG_ARP);

	/* END DEBUG - ArpCacheEntryAdd */

}

static void TcpIp_Arp_AddPendingIpToCache(TcpIpStack* instance, uint8_t* ip) {
	ArpCacheEntry* usedEntry = TcpIp_Arp_GetNearestEntryForIp(instance, ip);
	if (usedEntry != NULL) {
		usedEntry->state = ACES_PENDING;
		usedEntry->age = 0;
		usedEntry->createTime = instance->timeFunc();
		CPYBYTES(ip, usedEntry->ip, 4);
		CPYBYTES(EMPTY_MAC, usedEntry->mac, 6);
	}
}

static void TcpIp_Arp_RemoveEntry(TcpIpStack* instance, ArpCacheEntry* entry) {

	entry->state = ACES_INVALID;

	/* START DEBUG - ArpCacheEntryDrop */

	DEBUG_START(ArpCacheEntryDrop, TCPIP_DEBUG_TAG_ARP);

	DEBUG_BLOCK(ArpCacheEntryDrop, 1) {
		DEBUG_FORMAT_PRINT("[%s] Remove ARP cache entry.", TCPIP_DEBUG_TAG_ARP);
	}

	DEBUG_BLOCK(ArpCacheEntryDrop, 2) {
		char addressBuffer[20];
		macToStr(entry->mac, addressBuffer);
		DEBUG_FORMAT_PRINT("[%s] MAC: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
		ipToStr(entry->ip, addressBuffer);
		DEBUG_FORMAT_PRINT("[%s] IP: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
	}

	DEBUG_BLOCK(ArpCacheEntryDrop, 3) {
		TcpIp_Arp_PrintTable(instance);
	}

	DEBUG_END(ArpCacheEntryDrop, TCPIP_DEBUG_TAG_ARP);

	/* END DEBUG - ArpCacheEntryDrop */

}

void TcpIp_Arp_Process(TcpIpStack* instance) {
	float time = instance->timeFunc();
	for (int i = 0; i < TCPIP_ARP_CACHE_SIZE; i++) {
		ArpCacheEntry* entry = &(instance->arpCache[i]);
		if ((entry->state == ACES_VALID) || (entry->state == ACES_PENDING)) {
			entry->age = (time - entry->createTime);
			if (entry->age > TCPIP_ARP_CACHE_TTL) {
				TcpIp_Arp_RemoveEntry(instance, entry);
			}
		}
	}
}

void TcpIp_Arp_PrintTable(TcpIpStack* instance) {
	int entries = 0;
	DEBUG_PRINT("+----- ARP TABLE ---------+-------+-------------------+-----------------+");
	for (int i = 0; i < TCPIP_ARP_CACHE_SIZE; i++) {
		ArpCacheEntry* entry = &(instance->arpCache[i]);
		if (entry->state != ACES_INVALID) {
			char macStr[20], ipStr[20];
			macToStr(entry->mac, macStr);
			ipToStr(entry->ip, ipStr);
			DEBUG_FORMAT_PRINT("| %-23s | %-5lu | %s | %-15s |", (entry->state == ACES_PENDING ? "pending" : "valid"), (TCPIP_ARP_CACHE_TTL - entry->age)/1000, macStr, ipStr);
			entries++;
		}
	}
	if (entries > 0)
		DEBUG_PRINT("+-------------------------+-------+-------------------+-----------------+");
	DEBUG_FORMAT_PRINT("| Total: %d entries                                                      |", entries);
	DEBUG_PRINT("+-----------------------------------------------------------------------+");
}

static void TcpIp_Arp_Debug_Header(TcpIpStack* instance, ArpPacket* packet) {
	uint16_t hType = SWP16(packet->hType);
	DEBUG_FORMAT_PRINT("[%s] Hardware type: 0x%04X (%u)", TCPIP_DEBUG_TAG_ARP, hType, hType);
	uint16_t pType = SWP16(packet->pType);
	DEBUG_FORMAT_PRINT("[%s] Protocol type: 0x%04X (%u)", TCPIP_DEBUG_TAG_ARP, pType, pType);
	DEBUG_FORMAT_PRINT("[%s] Hardware address length: %u", TCPIP_DEBUG_TAG_ARP, packet->hLen);
	DEBUG_FORMAT_PRINT("[%s] Protocol address length: %u", TCPIP_DEBUG_TAG_ARP, packet->pLen);
	uint16_t oper = SWP16(packet->oper);
	DEBUG_FORMAT_PRINT("[%s] Operation: 0x%04X (%u)", TCPIP_DEBUG_TAG_ARP, oper, oper);
	char addressBuffer[20];
	macToStr(packet->sha, addressBuffer);
	DEBUG_FORMAT_PRINT("[%s] Sender MAC: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
	ipToStr(packet->spa, addressBuffer);
	DEBUG_FORMAT_PRINT("[%s] Sender IP: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
	macToStr(packet->tha, addressBuffer);
	DEBUG_FORMAT_PRINT("[%s] Target MAC: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
	ipToStr(packet->tpa, addressBuffer);
	DEBUG_FORMAT_PRINT("[%s] Target IP: %s", TCPIP_DEBUG_TAG_ARP, addressBuffer);
}
