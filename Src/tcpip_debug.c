#include "tcpip_debug.h"
#include "tcpip.h"

const static char* TCPIP_DEBUG_TAG_DEBUG = "TCPIP/Debug";

char tcpIpDebugBuffer[255];

const static char* TCPIP_DBG_OPT_STR_EthernetReceive = "ethernet receive";
const static char* TCPIP_DBG_OPT_STR_EthernetTransmit = "ethernet transmit";
const static char* TCPIP_DBG_OPT_STR_ArpReceive = "ARP receive";
const static char* TCPIP_DBG_OPT_STR_ArpTransmit = "ARP transmit";
const static char* TCPIP_DBG_OPT_STR_Ipv4Receive = "IPv4 receive";
const static char* TCPIP_DBG_OPT_STR_Ipv4Transmit = "IPv4 transmit";
const static char* TCPIP_DBG_OPT_STR_IcmpReceive = "ICMP receive";
const static char* TCPIP_DBG_OPT_STR_IcmpTransmit = "ICMP transmit";
const static char* TCPIP_DBG_OPT_STR_UdpReceive = "UDP receive";
const static char* TCPIP_DBG_OPT_STR_UdpTransmit = "UDP transmit";
const static char* TCPIP_DBG_OPT_STR_ArpCacheEntryAdd = "ARP cache entry add";
const static char* TCPIP_DBG_OPT_STR_ArpCacheEntryDrop = "ARP cache entry drop";
const static char* TCPIP_DBG_OPT_STR_unknown = "??";

static const char* TcpIp_Debug_OptionToStr(TcpIpStackDebugOption opt) {
	switch (opt) {
	case TCPIP_DBG_OPT_EthernetReceive:
		return TCPIP_DBG_OPT_STR_EthernetReceive;
	case TCPIP_DBG_OPT_EthernetTransmit:
		return TCPIP_DBG_OPT_STR_EthernetTransmit;
	case TCPIP_DBG_OPT_ArpReceive:
		return TCPIP_DBG_OPT_STR_ArpReceive;
	case TCPIP_DBG_OPT_ArpTransmit:
		return TCPIP_DBG_OPT_STR_ArpTransmit;
	case TCPIP_DBG_OPT_Ipv4Receive:
		return TCPIP_DBG_OPT_STR_Ipv4Receive;
	case TCPIP_DBG_OPT_Ipv4Transmit:
		return TCPIP_DBG_OPT_STR_Ipv4Transmit;
	case TCPIP_DBG_OPT_IcmpReceive:
		return TCPIP_DBG_OPT_STR_IcmpReceive;
	case TCPIP_DBG_OPT_IcmpTransmit:
		return TCPIP_DBG_OPT_STR_IcmpTransmit;
	case TCPIP_DBG_OPT_UdpReceive:
		return TCPIP_DBG_OPT_STR_UdpReceive;
	case TCPIP_DBG_OPT_UdpTransmit:
		return TCPIP_DBG_OPT_STR_UdpTransmit;
	case TCPIP_DBG_OPT_ArpCacheEntryAdd:
		return TCPIP_DBG_OPT_STR_ArpCacheEntryAdd;
	case TCPIP_DBG_OPT_ArpCacheEntryDrop:
		return TCPIP_DBG_OPT_STR_ArpCacheEntryDrop;
	default:
		return TCPIP_DBG_OPT_STR_unknown;
	}
}

void TcpIp_Debug_SetLevel(TcpIpStack* instance, TcpIpStackDebugOption option, uint8_t level) {
	instance->debugOptions[option] = level;
	DEBUG_FORMAT_PRINT("[%s] Debug level for option '%s' is %u.", TCPIP_DEBUG_TAG_DEBUG, TcpIp_Debug_OptionToStr(option), level);
}

void TcpIp_Debug_UndebugAll(TcpIpStack* instance) {
	for (int i = 0; i < _TCPIP_DBG_OPT_COUNT; i++)
		instance->debugOptions[i] = 0;
	DEBUG_FORMAT_PRINT("[%s] All debug options set to off.", TCPIP_DEBUG_TAG_DEBUG);
}
