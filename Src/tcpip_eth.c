#include "stm32f4xx_hal.h"
#include "tcpip_eth.h"
#include "tcpip_ipv4.h"
#include "tcpip_arp.h"
#include "tcpip.h"
#include "tcpip_debug.h"
#include <stdio.h>

const static char* TCPIP_DEBUG_TAG_ETH = "TCPIP/Ethernet";

static void TcpIp_Eth_Debug_Header(TcpIpStack* instance, EthernetFrame* frame);
static void TcpIp_Eth_Debug_Payload(TcpIpStack* instance, EthernetFrame* frame);

void TcpIp_ReceiveEthFrame(TcpIpStack* instance, uint8_t* data, uint16_t length) {
	EthernetFrame frame;
	frame.payloadLength = length-14;
	for (int i = 0; i < length; i++)
		((uint8_t*)(&frame))[i] = data[i];
	TcpIp_ReceiveEthFrameStruct(instance, &frame);
}

void TcpIp_ReceiveEthFrameStruct(TcpIpStack* instance, EthernetFrame* frame) {
#ifdef CHECK_RECVFRAME_MAC
	for (int i = 0; i < TCPIP_ETHFRM_MACADDR_LEN; i++)
		if (frame->dstMac[i] != instance->macAddress[i])
			return;
#endif

	/* START DEBUG - EthernetReceive */

	DEBUG_START(EthernetReceive, TCPIP_DEBUG_TAG_ETH);

	DEBUG_BLOCK(EthernetReceive, 1) {
		DEBUG_FORMAT_PRINT("[%s] Received Ethernet frame.", TCPIP_DEBUG_TAG_ETH);
	}

	DEBUG_BLOCK(EthernetReceive, 2) {
		TcpIp_Eth_Debug_Header(instance, frame);
	}

	DEBUG_BLOCK(EthernetReceive, 3) {
		TcpIp_Eth_Debug_Payload(instance, frame);
	}

	DEBUG_END(EthernetReceive, TCPIP_DEBUG_TAG_ETH);

	/* END DEBUG - EthernetReceive */

	uint16_t type = SWP16(frame->type);

	if (type == TCPIP_ETHTYPE_IPV4) {
		TcpIp_Ipv4_ReceivePacket(instance, frame->payload, frame->payloadLength);
		return;
	}

	if (type == TCPIP_ETHTYPE_ARP) {
		TcpIp_Arp_ReceivePacket(instance, frame->payload, frame->payloadLength);
		return;
	}

}

void TcpIp_TransmitEthFrame(TcpIpStack* instance, uint8_t* data, uint16_t length) {
	instance->transmitFunc(data, length);
}

void TcpIp_TransmitEthFrameStruct(TcpIpStack* instance, EthernetFrame* frame) {

	uint16_t length = (2*TCPIP_ETHFRM_MACADDR_LEN)+TCPIP_ETHFRM_TYPEFIELD_LEN+frame->payloadLength;

	/* START DEBUG - EthernetTransmit */

	DEBUG_START(EthernetTransmit, TCPIP_DEBUG_TAG_ETH);

	DEBUG_BLOCK(EthernetTransmit, 1) {
		DEBUG_FORMAT_PRINT("[%s] Transmitting Ethernet frame.", TCPIP_DEBUG_TAG_ETH);
	}

	DEBUG_BLOCK(EthernetTransmit, 2) {
		TcpIp_Eth_Debug_Header(instance, frame);
	}

	DEBUG_BLOCK(EthernetTransmit, 3) {
		TcpIp_Eth_Debug_Payload(instance, frame);
	}

	DEBUG_END(EthernetTransmit, TCPIP_DEBUG_TAG_ETH);

	/* END DEBUG - EthernetTransmit */

	instance->transmitFunc((uint8_t*)frame, length);

}

void TcpIp_TransmitEthFrameDetailed(TcpIpStack* instance, uint8_t* dstMac, uint8_t* srcMac, uint16_t type, uint8_t* payload, uint16_t payloadLength) {
	EthernetFrame frame;
	for (int i = 0; i < TCPIP_ETHFRM_MACADDR_LEN; i++)
		frame.dstMac[i] = dstMac[i];
	for (int i = 0; i < TCPIP_ETHFRM_MACADDR_LEN; i++)
		frame.srcMac[i] = srcMac[i];
	frame.type = SWP16(type);
	for (int i = 0; i < payloadLength; i++)
		frame.payload[i] = payload[i];
	frame.payloadLength = payloadLength;
	TcpIp_TransmitEthFrameStruct(instance, &frame);
}

void TcpIp_TransmitEthFrameDetailedOwnMac(TcpIpStack* instance, uint8_t* dstMac, uint16_t type, uint8_t* payload, uint16_t payloadLength) {
	TcpIp_TransmitEthFrameDetailed(instance, dstMac, instance->macAddress, type, payload, payloadLength);
}

static void TcpIp_Eth_Debug_Header(TcpIpStack* instance, EthernetFrame* frame) {
	char macAddress[20];
	macToStr(frame->srcMac, macAddress);
	DEBUG_FORMAT_PRINT("[%s] Source MAC: %s", TCPIP_DEBUG_TAG_ETH, macAddress);
	macToStr(frame->dstMac, macAddress);
	DEBUG_FORMAT_PRINT("[%s] Destination MAC: %s", TCPIP_DEBUG_TAG_ETH, macAddress);
	DEBUG_FORMAT_PRINT("[%s] Type: 0x%04X", TCPIP_DEBUG_TAG_ETH, SWP16(frame->type));
	DEBUG_FORMAT_PRINT("[%s] Payload length: %u bytes", TCPIP_DEBUG_TAG_ETH, frame->payloadLength);
}

static void TcpIp_Eth_Debug_Payload(TcpIpStack* instance, EthernetFrame* frame) {
	DEBUG_FORMAT_PRINT("[%s] Payload:", TCPIP_DEBUG_TAG_ETH);
	DEBUG_HEXDUMP(frame->payload, frame->payloadLength);
}

