#include "stm32f4xx_hal.h"
#include "tcpip_helper.h"
#include "hexdump.h"
#include <stdio.h>

int sameSubnet(uint8_t* ip1, uint8_t* ip2, uint8_t* mask) {
	for (int i = 0; i < 4; i++)
		if((ip1[i]&mask[i]) != (ip2[i]&mask[i]))
			return 0;
	return 1;
}

int arrEq(uint8_t* arr1, uint8_t* arr2, uint16_t length) {
	for (int i = 0; i < length; i++)
		if(arr1[i] != arr2[i])
			return 0;
	return 1;
}

void macToStr(uint8_t* macAddress, char* buffer) {
	sprintf(buffer, "%02X:%02X:%02X:%02X:%02X:%02X", macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5]);
}

void ipToStr(uint8_t* ipAddress, char* buffer) {
	sprintf(buffer, "%u.%u.%u.%u", ipAddress[0], ipAddress[1], ipAddress[2], ipAddress[3]);
}
