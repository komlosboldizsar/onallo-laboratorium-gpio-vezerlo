#include "stm32f4xx_hal.h"
#include "tcpip_icmp.h"
#include "tcpip_ipv4.h"
#include "tcpip.h"
#include "checksum.h"

const static char* TCPIP_DEBUG_TAG_ICMP = "TCPIP/ICMP";

static void TcpIp_Icmp_Debug_Header(TcpIpStack* instance, IcmpPacket* packet);
static void TcpIp_Icmp_Debug_Payload(TcpIpStack* instance, IcmpPacket* packet);

void TcpIp_Icmp_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length, Ipv4Packet* parent) {

	IcmpPacket packet;
	for (int i = 0; i < length; i++)
		((uint8_t*)(&packet))[i] = data[i];
	packet.restLength = length - 4;

	/* START DEBUG - IcmpReceive */

	DEBUG_START(IcmpReceive, TCPIP_DEBUG_TAG_ICMP);

	DEBUG_BLOCK(IcmpReceive, 1) {
		DEBUG_FORMAT_PRINT("[%s] Received ICMP packet.", TCPIP_DEBUG_TAG_ICMP);
	}

	DEBUG_BLOCK(IcmpReceive, 2) {
		TcpIp_Icmp_Debug_Header(instance, &packet);
	}

	DEBUG_BLOCK(IcmpReceive, 3) {
		TcpIp_Icmp_Debug_Payload(instance, &packet);
	}

	DEBUG_END(IcmpReceive, TCPIP_DEBUG_TAG_ICMP);

	/* END DEBUG - IcmpReceive */

	if (packet.type == 8) {
		TcpIp_Icmp_SendEchoReply(instance, &packet, parent->srcIp);
	}

}

void TcpIp_Icmp_SendEchoReply(TcpIpStack* instance, IcmpPacket* request, uint8_t* dstIp) {

	IcmpPacket packet;
	packet.type = 0;
	packet.code = 0;
	packet.checksum = 0;
	packet.restLength = request->restLength;
	for (int i = 0; i < packet.restLength; i++)
		packet.rest[i] = request->rest[i];
	uint8_t* p = (uint8_t*)(&packet);
	uint16_t length = packet.restLength + 4;
	packet.checksum = computeChecksum(p, length);

	/* START DEBUG - IcmpTransmit */

	DEBUG_START(IcmpTransmit, TCPIP_DEBUG_TAG_ICMP);

	DEBUG_BLOCK(IcmpTransmit, 1) {
		DEBUG_FORMAT_PRINT("[%s] Transmitting ICMP packet.", TCPIP_DEBUG_TAG_ICMP);
	}

	DEBUG_BLOCK(IcmpTransmit, 2) {
		TcpIp_Icmp_Debug_Header(instance, &packet);
	}

	DEBUG_BLOCK(IcmpTransmit, 3) {
		TcpIp_Icmp_Debug_Payload(instance, &packet);
	}

	DEBUG_END(IcmpTransmit, TCPIP_DEBUG_TAG_ICMP);

	/* END DEBUG - IcmpReceive */

	TcpIp_Ipv4_SendPacket(instance, dstIp, TCPIP_IPV4PROTO_ICMP, p, length);

}

static void TcpIp_Icmp_Debug_Header(TcpIpStack* instance, IcmpPacket* packet) {
	DEBUG_FORMAT_PRINT("[%s] Type: 0x%02X (%u)", TCPIP_DEBUG_TAG_ICMP, packet->type, packet->type);
	DEBUG_FORMAT_PRINT("[%s] Code: 0x%02X (%u)", TCPIP_DEBUG_TAG_ICMP, packet->code, packet->code);
	DEBUG_FORMAT_PRINT("[%s] Checksum: 0x%04X", TCPIP_DEBUG_TAG_ICMP, SWP16(packet->checksum));
	DEBUG_FORMAT_PRINT("[%s] Data length: %u bytes", TCPIP_DEBUG_TAG_ICMP, packet->restLength);
}

static void TcpIp_Icmp_Debug_Payload(TcpIpStack* instance, IcmpPacket* packet) {
	DEBUG_FORMAT_PRINT("[%s] Data:", TCPIP_DEBUG_TAG_ICMP);
	DEBUG_HEXDUMP(packet->rest, packet->restLength);
}
