#include "stm32f4xx_hal.h"
#include "tcpip_eth.h"
#include "tcpip_ipv4.h"
#include "tcpip_icmp.h"
#include "tcpip_udp.h"
#include "tcpip.h"
#include "checksum.h"
#include <stdio.h>

#define IPV4_GET_VERSION(PACKET)         ((uint8_t)((((PACKET)._version_ihl)&0xF0)>>4))
#define IPV4_GET_IHL(PACKET)             ((uint8_t)(((PACKET)._version_ihl)&0x0F))
#define IPV4_GET_DSCP(PACKET)            ((uint8_t)((((PACKET)._dscp_ecn)&0xFC)>>6))
#define IPV4_GET_ECN(PACKET)             ((uint8_t)(((PACKET)._dscp_ecn)&0x03))
#define IPV4_GET_FLAGS(PACKET)           ((uint8_t)((SWP16((PACKET)._flags_fragmentOffset)&0xE000)>>13))
#define IPV4_GET_FRAGMENTOFFSET(PACKET)  ((uint16_t)(SWP16((PACKET)._flags_fragmentOffset)&0x1FFF))

#define IPV4_SET_VERSION(PACKET, VALUE)         (PACKET)._version_ihl = (((PACKET)._version_ihl)&0x0F)|(((VALUE)<<4)&0xF0);
#define IPV4_SET_IHL(PACKET, VALUE)             (PACKET)._version_ihl = (((PACKET)._version_ihl)&0xF0)|((VALUE)&0x0F);
#define IPV4_SET_DSCP(PACKET, VALUE)            (PACKET)._dscp_ecn = (((PACKET)._dscp_ecn)&0x03)|(((VALUE)<<6)&0xFC);
#define IPV4_SET_ECN(PACKET, VALUE)             (PACKET)._dscp_ecn = (((PACKET)._dscp_ecn)&0xFC)|((VALUE)&0x03);
/*
#define IPV4_SET_FLAGS(PACKET, VALUE)           PACKET._flags_fragmentOffset = (((PACKET)._flags_fragmentOffset)&0xFC)|((VALUE)&0x03);
#define IPV4_SET_FRAGMENTOFFSET(PACKET, VALUE)  ((uint8_t)(SWP16((PACKET)._flags_fragmentOffset)&0x1FFF))
*/

const static char* TCPIP_DEBUG_TAG_IPV4 = "TCPIP/IPv4";

static void TcpIp_Ipv4_Debug_Header(TcpIpStack* instance, Ipv4Packet* packet);
static void TcpIp_Ipv4_Debug_Payload(TcpIpStack* instance, Ipv4Packet* packet);

void TcpIp_Ipv4_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length) {

	Ipv4Packet packet;
	for (int i = 0; i < length; i++)
		((uint8_t*)(&packet))[i] = data[i];
	packet.payloadLength = length - 20;

	/* START DEBUG - Ipv4Receive */

	DEBUG_START(Ipv4Receive, TCPIP_DEBUG_TAG_IPV4);

	DEBUG_BLOCK(Ipv4Receive, 1) {
		DEBUG_FORMAT_PRINT("[%s] Received IPv4 packet.", TCPIP_DEBUG_TAG_IPV4);
	}

	DEBUG_BLOCK(Ipv4Receive, 2) {
		TcpIp_Ipv4_Debug_Header(instance, &packet);
	}

	DEBUG_BLOCK(Ipv4Receive, 3) {
		TcpIp_Ipv4_Debug_Payload(instance, &packet);
	}

	DEBUG_END(Ipv4Receive, TCPIP_DEBUG_TAG_IPV4);

	/* END DEBUG - Ipv4Receive */

	// Header checksum test

	if (IPV4_GET_VERSION(packet) != 4)
		return;

	if (IPV4_GET_IHL(packet) != 5)
		return;

	if (IPV4_GET_FRAGMENTOFFSET(packet) != 0)
		return;

	if (IPV4_GET_FLAGS(packet) != 0)
		return;

	switch (packet.proto) {
	case TCPIP_IPV4PROTO_ICMP:
		TcpIp_Icmp_ReceivePacket(instance, packet.payload, packet.payloadLength, &packet);
		break;
	case TCPIP_IPV4PROTO_UDP:
		TcpIp_Udp_ReceivePacket(instance, packet.payload, packet.payloadLength, &packet);
		break;
	}

}

int TcpIp_Ipv4_SendPacket(TcpIpStack* instance, uint8_t* dstIp, uint8_t proto, uint8_t* payload, uint16_t payloadLength) {

	int txBufferEntryIndex = -1;
	for (int i = 0; i < TCPIP_TX_BUFFER_SIZE; i++) {
		if (instance->ipv4TxBuffer[i].state == IPV4TXBES_EMPTY) {
			txBufferEntryIndex = i;
			break;
		}
	}

	if (txBufferEntryIndex == -1)
		return 1;

	Ipv4TxBufferEntry* bufferEntry = &(instance->ipv4TxBuffer[txBufferEntryIndex]);
	bufferEntry->state = IPV4TXBES_QUEUED;
	bufferEntry->txTimeoutEnd = instance->timeFunc() + TCPIP_TX_TIMEOUT;

	Ipv4Packet* packet = &bufferEntry->packet;
	/*IPV4_SET_VERSION(*packet, 4);
	IPV4_SET_IHL(*packet, 5);*/
	packet->_version_ihl = 0x45;
	packet->_dscp_ecn = 0x00;
	/*IPV4_SET_DSCP(*packet, 0);
	IPV4_SET_ECN(*packet, 0);*/
	packet->totalLength = SWP16(payloadLength + 20);
	packet->identification = 0x0000;
	packet->_flags_fragmentOffset = 0x0000;
	packet->ttl = 255;
	packet->proto = proto;
	packet->headerChecksum = 0x0000;
	CPYBYTES(instance->ipAddress, packet->srcIp, 4);
	CPYBYTES(dstIp, packet->dstIp, 4);
	CPYBYTES(payload, packet->payload, payloadLength);
	packet->payloadLength = payloadLength;

	packet->headerChecksum = computeChecksum((uint8_t*)packet, 20);

	/* START DEBUG - Ipv4Transmit */

	DEBUG_START(Ipv4Transmit, TCPIP_DEBUG_TAG_IPV4);

	DEBUG_BLOCK(Ipv4Transmit, 1) {
		DEBUG_FORMAT_PRINT("[%s] Transmitting IPv4 packet.", TCPIP_DEBUG_TAG_IPV4);
	}

	DEBUG_BLOCK(Ipv4Transmit, 2) {
		TcpIp_Ipv4_Debug_Header(instance, packet);
	}

	DEBUG_BLOCK(Ipv4Transmit, 3) {
		TcpIp_Ipv4_Debug_Payload(instance, packet);
	}

	DEBUG_END(Ipv4Transmit, TCPIP_DEBUG_TAG_IPV4);

	/* END DEBUG - Ipv4Transmit */

	uint8_t* nextIp = sameSubnet(dstIp, instance->ipAddress, instance->subnetMask) ? dstIp : instance->defaultGateway;
	CPYBYTES(nextIp, bufferEntry->nextIp, 4);

	return 0;

}

void TcpIp_Ipv4_SendProcess(TcpIpStack* instance) {

	for (int i = 0; i < TCPIP_TX_BUFFER_SIZE; i++) {

		Ipv4TxBufferEntry* bufferEntry = &(instance->ipv4TxBuffer[i]);
		if (bufferEntry->state == IPV4TXBES_EMPTY)
			continue;

		int send = 0;
		uint8_t* mac;

		if (bufferEntry->state == IPV4TXBES_QUEUED) {
			ArpCacheEntry* cacheEntry = TcpIp_Arp_GetEntryForIp(instance, bufferEntry->nextIp);
			if (cacheEntry != NULL) {
				send = 1;
				mac = cacheEntry->mac;
			} else {
				TcpIp_Arp_SendRequest(instance, bufferEntry->nextIp);
				bufferEntry->state = IPv4TXBES_WAITING_ARP;
			}
		}

		if (bufferEntry->state == IPv4TXBES_WAITING_ARP) {
			ArpCacheEntry* cacheEntry = TcpIp_Arp_GetEntryForIp(instance, bufferEntry->nextIp);
			if (cacheEntry != NULL) {
				send = 1;
				mac = cacheEntry->mac;
			}
		}

		if (send) {
			TcpIp_TransmitEthFrameDetailedOwnMac(instance, mac, 0x0800, (uint8_t*)(&(bufferEntry->packet)), SWP16(bufferEntry->packet.totalLength));
			bufferEntry->state = IPV4TXBES_EMPTY;
		}

	}

}

static void TcpIp_Ipv4_Debug_Header(TcpIpStack* instance, Ipv4Packet* packet) {
	DEBUG_FORMAT_PRINT("[%s] Version: %u", TCPIP_DEBUG_TAG_IPV4, IPV4_GET_VERSION(*packet));
	DEBUG_FORMAT_PRINT("[%s] IHL: %u", TCPIP_DEBUG_TAG_IPV4, IPV4_GET_IHL(*packet));
	uint8_t dscp = IPV4_GET_DSCP(*packet);
	DEBUG_FORMAT_PRINT("[%s] DSCP: 0x%02X (%u)", TCPIP_DEBUG_TAG_IPV4, dscp, dscp);
	uint8_t ecn = IPV4_GET_ECN(*packet);
	DEBUG_FORMAT_PRINT("[%s] ECN: 0x%01X (%u)", TCPIP_DEBUG_TAG_IPV4, ecn, ecn);
	DEBUG_FORMAT_PRINT("[%s] Total length: %u bytes", TCPIP_DEBUG_TAG_IPV4, SWP16(packet->totalLength));
	uint16_t identification = SWP16(packet->identification);
	DEBUG_FORMAT_PRINT("[%s] Identification: 0x%04X (%u)", TCPIP_DEBUG_TAG_IPV4, identification, identification);
	DEBUG_FORMAT_PRINT("[%s] Flags: 0x%01X", TCPIP_DEBUG_TAG_IPV4, IPV4_GET_FLAGS(*packet));
	DEBUG_FORMAT_PRINT("[%s] TTL: %u", TCPIP_DEBUG_TAG_IPV4, packet->ttl);
	DEBUG_FORMAT_PRINT("[%s] Protocol: 0x%02X (%u)", TCPIP_DEBUG_TAG_IPV4, packet->proto, packet->proto);
	DEBUG_FORMAT_PRINT("[%s] Header checksum: 0x%04X", TCPIP_DEBUG_TAG_IPV4, SWP16(packet->headerChecksum));
	char ipAddress[20];
	ipToStr(packet->srcIp, ipAddress);
	DEBUG_FORMAT_PRINT("[%s] Source IP: %s", TCPIP_DEBUG_TAG_IPV4, ipAddress);
	ipToStr(packet->dstIp, ipAddress);
	DEBUG_FORMAT_PRINT("[%s] Destination IP: %s", TCPIP_DEBUG_TAG_IPV4, ipAddress);
}

static void TcpIp_Ipv4_Debug_Payload(TcpIpStack* instance, Ipv4Packet* packet) {
	DEBUG_FORMAT_PRINT("[%s] Payload:", TCPIP_DEBUG_TAG_IPV4);
	DEBUG_HEXDUMP(packet->payload, packet->payloadLength);
}
