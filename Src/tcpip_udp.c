#include "stm32f4xx_hal.h"
#include "tcpip_udp.h"
#include "tcpip_ipv4.h"
#include "checksum.h"
#include "tcpip.h"

const static char* TCPIP_DEBUG_TAG_UDP = "TCPIP/UDP";

static void TcpIp_Udp_Debug_Header(TcpIpStack* instance, UdpPacket* packet);
static void TcpIp_Udp_Debug_Payload(TcpIpStack* instance, UdpPacket* packet);

void TcpIp_Udp_ReceivePacket(TcpIpStack* instance, uint8_t* data, uint16_t length, Ipv4Packet* parent) {

	UdpPacket packet;
	for (int i = 0; i < length; i++)
		((uint8_t*)(&packet))[i] = data[i];

	/* START DEBUG - UdpReceive */

	DEBUG_START(UdpReceive, TCPIP_DEBUG_TAG_UDP);

	DEBUG_BLOCK(UdpReceive, 1) {
		DEBUG_FORMAT_PRINT("[%s] Received UDP packet.", TCPIP_DEBUG_TAG_UDP);
	}

	DEBUG_BLOCK(UdpReceive, 2) {
		TcpIp_Udp_Debug_Header(instance, &packet);
	}

	DEBUG_BLOCK(UdpReceive, 3) {
		TcpIp_Udp_Debug_Payload(instance, &packet);
	}

	DEBUG_END(UdpReceive, TCPIP_DEBUG_TAG_UDP);

	/* END DEBUG - UdpReceive */

	uint16_t dstPort = SWP16(packet.dstPort);

	for (int i = 0; i < TCPIP_UDP_MAX_APPS; i++) {
		UdpApplication* app = &(instance->udpApplications[i]);
		if ((app->valid == 1) && (app->port == dstPort)) {
			app->receiveFunc(parent->srcIp, SWP16(packet.srcPort), packet.payload, SWP16(packet.length)-8, app->tagP, app->tagD);
			break;
		}
	}

}

void TcpIp_Udp_SendPacket(TcpIpStack* instance, uint8_t* dstIp, uint16_t dstPort, uint16_t srcPort, uint8_t* payload, uint16_t payloadLength) {

	// Create packet without checksum
	UdpPacket packet;
	uint16_t totalLength = payloadLength + 8;
	packet.srcPort = SWP16(srcPort);
	packet.dstPort = SWP16(dstPort);
	packet.length = SWP16(totalLength);
	packet.checksum = 0x0000;
	for (int i = 0; i < payloadLength; i++)
		packet.payload[i] = payload[i];
	uint16_t udpChecksum = computeChecksum((uint8_t*)(&packet), totalLength);

	// Create IPv4 pseudo header
	UdpIpv4PseudoHeader pseudoHeader;
	CPYBYTES(instance->ipAddress, pseudoHeader.srcIp, 4);
	CPYBYTES(dstIp, pseudoHeader.dstIp, 4);
	pseudoHeader.zeroes = 0x00;
	pseudoHeader.protocol = TCPIP_IPV4PROTO_UDP;
	pseudoHeader.udpLength = SWP16(totalLength);
	uint16_t pseudoHeaderChecksum = computeChecksum((uint8_t*)(&pseudoHeader), sizeof(UdpIpv4PseudoHeader));

	packet.checksum = combineChecksum(udpChecksum, pseudoHeaderChecksum);

	/* START DEBUG - UdpTransmit */

	DEBUG_START(UdpTransmit, TCPIP_DEBUG_TAG_UDP);

	DEBUG_BLOCK(UdpTransmit, 1) {
		DEBUG_FORMAT_PRINT("[%s] Transmitting UDP packet.", TCPIP_DEBUG_TAG_UDP);
	}

	DEBUG_BLOCK(UdpTransmit, 2) {
		TcpIp_Udp_Debug_Header(instance, &packet);
	}

	DEBUG_BLOCK(UdpTransmit, 3) {
		TcpIp_Udp_Debug_Payload(instance, &packet);
	}

	DEBUG_END(UdpTransmit, TCPIP_DEBUG_TAG_UDP);

	/* END DEBUG - UdpTransmit */

	TcpIp_Ipv4_SendPacket(instance, dstIp, TCPIP_IPV4PROTO_UDP, (uint8_t*)(&packet), totalLength);

}

int TcpIp_Udp_RegisterApplication(TcpIpStack* instance, uint16_t port, UdpApplicationReceiveFunc receiveFunc, void* tagP, uint8_t tagD) {

	UdpApplication* freeAppSlot = NULL;
	for (int i = 0; i < TCPIP_UDP_MAX_APPS; i++) {
		UdpApplication* app = &(instance->udpApplications[i]);
		if ((app->valid == 1) && (app->port == port))
			return TCPIP_UDP_APPREG_ERRCODE_PORTRESERVED;
		if ((freeAppSlot == NULL) && (app->valid == 0))
			freeAppSlot = app;
	}

	if (freeAppSlot == NULL)
		return TCPIP_UDP_APPREG_ERRCODE_NOFREESLOT;

	freeAppSlot->port = port;
	freeAppSlot->receiveFunc = receiveFunc;
	freeAppSlot->tagP = tagP;
	freeAppSlot->tagD = tagD;
	freeAppSlot->valid = 1;

	return 0;

}

static void TcpIp_Udp_Debug_Header(TcpIpStack* instance, UdpPacket* packet) {
	DEBUG_FORMAT_PRINT("[%s] Source port: %u", TCPIP_DEBUG_TAG_UDP, SWP16(packet->srcPort));
	DEBUG_FORMAT_PRINT("[%s] Destination port: %u", TCPIP_DEBUG_TAG_UDP, SWP16(packet->dstPort));
	DEBUG_FORMAT_PRINT("[%s] Length: %u bytes", TCPIP_DEBUG_TAG_UDP, SWP16(packet->length));
	DEBUG_FORMAT_PRINT("[%s] Checksum: 0x%04X", TCPIP_DEBUG_TAG_UDP, SWP16(packet->checksum));
}

static void TcpIp_Udp_Debug_Payload(TcpIpStack* instance, UdpPacket* packet) {
	DEBUG_FORMAT_PRINT("[%s] Payload:", TCPIP_DEBUG_TAG_UDP);
	DEBUG_HEXDUMP(packet->payload, SWP16(packet->length)-8);
}

