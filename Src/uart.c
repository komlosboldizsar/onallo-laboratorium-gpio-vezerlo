#include "uart.h"
#include "stm32f4xx_hal.h"
#include "peripheral.h"
#include "macro.h"
#include "string.h"

UART_ChrRecvCallback uartChrRecvCallback = NULL;
uint8_t uartEcho = 0;

uint8_t uartRecvData;

#define P_UART				P_UART_CONSOLE
#define UART_RX_Chr()		HAL_UART_Receive_IT(P_UART, &uartRecvData, 1)
#define UART_TX_Chr(C)		{ \
								uint8_t chr = (uint8_t)(C); \
								HAL_UART_Transmit(P_UART, &chr, 1, 5); \
							}


void UART_TX(const char* str) {
	NOPWAIT(1000);
	HAL_UART_Transmit(P_UART, (uint8_t*)str, strlen(str), 100);
	UART_RX_Chr();
}

void UART_RX_Start(UART_ChrRecvCallback chrRecvCallback, uint8_t echo) {
	uartChrRecvCallback = chrRecvCallback;
	uartEcho = echo;
	UART_RX_Chr();
}

void UART_RX_Stop() {
	uartChrRecvCallback = NULL;
	uartEcho = 0;
	HAL_UART_AbortReceive_IT(P_UART);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart) {
	if (huart == P_UART) {
		if (uartChrRecvCallback != NULL) {
			if (uartEcho) {
				UART_TX_Chr(uartRecvData);
				if(uartRecvData == '\r')
					UART_TX_Chr('\n');
			}
			uartChrRecvCallback((char)uartRecvData);
			UART_RX_Chr();
		}
	}
}
